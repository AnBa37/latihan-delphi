unit LaporanGaji;

interface

uses
  Uglobal,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, BaseGrid, AdvGrid, ExtCtrls;

type
  TfrmLaporanGaji = class(TForm)
    asgLaporan: TAdvStringGrid;
    btnPrint: TButton;
    btnClose: TButton;
    Shape1: TShape;
    Label1: TLabel;
    Shape2: TShape;
    Label2: TLabel;
    procedure asgLaporanGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgLaporanGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure btnPrintClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillGrid;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmLaporanGaji: TfrmLaporanGaji;

implementation

uses UData, Koneksi, asgPrintPrev;

const
  ColDept = 1;
  ColNama = 2;
  ColPosisi = 3;
  ColGaji = 4;
  ColTunjangan = 5;
  ColBonus = 6;
  ColTotal = 7;

{$R *.dfm}

{ TfrmLaporanGaji }

procedure TfrmLaporanGaji.Execute;
begin
  FillGrid;
  Showmodal;
end;

procedure TfrmLaporanGaji.FillGrid;
var i, j, r1, r2 : integer;
    ADepartemen : ARDepartemen;
    AKaryawan : ARKaryawan;
    ABonus : ARBonus;
    total, TGaji, TTunj, TBonus, GTGaji, GTTunj, GTBonus : currency;
begin
  asgLaporan.BeginUpdate;
  asgLaporan.BringToFront;
  asgLaporan.ExpandAll;
  asgLaporan.ClearNormalCells;
  asgLaporan.RowCount := 3;
  UData.GetDepartemen(0,ADepartemen);
  total := 0;
  GTGaji := 0;
  GTTunj := 0;
  GTBonus := 0;
  for i := 0 to length(ADepartemen)-1 do begin
    r1 := asgLaporan.RowCount-2;
    asgLaporan.Cells[ColDept,r1] := ADepartemen[i].NamaDepartemen;
    r2 := r1;
    UData.GetDaftarKaryawan('','',ADepartemen[i].NoDepartemen,AKaryawan);
    TGaji := 0;
    TTunj := 0;
    TBonus := 0;
    for j := 0 to length(AKaryawan)-1 do begin
      asgLaporan.AddRow;
      r2 := asgLaporan.RowCount-2;
      asgLaporan.Cells[ColNama,r2] := NumberToString(Akaryawan[j].NIK,7)+' - '+AKaryawan[j].Nama;
      asgLaporan.Cells[ColPosisi,r2] := AKaryawan[j].Posisi;
      asgLaporan.Floats[ColGaji,r2] := AKaryawan[j].GajiPokok;
      TGaji := TGaji + AKaryawan[j].GajiPokok;
      asgLaporan.Floats[ColTunjangan,r2] := AKaryawan[j].Tunjangan;
      TTunj := TTunj + AKaryawan[j].Tunjangan;
      UData.GetBonus(AKaryawan[j].NIK,ABonus);
      if ABonus <> nil then begin
        asgLaporan.Floats[ColBonus,r2] := ABonus[0].Bonus;
        TBonus := TBonus + ABonus[0].Bonus;
        asgLaporan.Floats[ColTotal,r2] := AKaryawan[j].GajiPokok + AKaryawan[j].Tunjangan
                                          + ABonus[0].Bonus;
      end else begin
        asgLaporan.Floats[ColTotal,r2] := AKaryawan[j].GajiPokok + AKaryawan[j].Tunjangan;
      end;
    end;
    asgLaporan.Floats[ColGaji,r1] := TGaji;
    GTGaji := GTGaji + TGaji;
    asgLaporan.Floats[ColTunjangan,r1] := TTunj;
    GTTunj := GTTunj + TTunj;
    asgLaporan.Floats[ColBonus,r1] := TBonus;
    GTBonus := GTBonus + TBonus;
    asgLaporan.Floats[ColTotal,r1] := TGaji+TTunj+TBonus;
    Total := Total + (TGaji+TTunj+TBonus);
    asgLaporan.Cells[15,r1] := 'H';
    asgLaporan.AddNode(r1,r2-r1+1);
    asgLaporan.AddRow;
  end;
  asgLaporan.Floats[ColGaji,asgLaporan.RowCount-1] := GTGaji;
  asgLaporan.Floats[ColTunjangan,asgLaporan.RowCount-1] := GTTunj;
  asgLaporan.Floats[ColBonus,asgLaporan.RowCount-1] := GTBonus;
  asgLaporan.Floats[ColTotal,asgLaporan.RowCount-1] := total;
  asgLaporan.AutoSizeColumns(true,2);
  asgLaporan.ContractAll;
  asgLaporan.EndUpdate;
end;

procedure TfrmLaporanGaji.asgLaporanGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ACol in [ColGaji..ColTotal] then HAlign := TARightJustify;
  if ARow = 0 then HAlign := TACenter;
end;

procedure TfrmLaporanGaji.asgLaporanGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (asgLaporan.Cells[ColNama,ARow] = '') and (ARow <> asgLaporan.RowCount-1) then begin
    AFont.Style := [fsBold];
    if asgLaporan.Floats[ColTotal,ARow] > 5000000 then AFont.Color := clBlue;
  end else begin
    if (asgLaporan.Floats[ColGaji,ARow] > 1500000) and (ARow <> asgLaporan.RowCount-1) then begin
      AFont.Color := clRed;
    end;
  end;
  if ARow = asgLaporan.RowCount -1 then AFont.Style := [fsBold];

end;

procedure TfrmLaporanGaji.btnPrintClick(Sender: TObject);
var p:tASGPrintPreview;
begin
  showmessage('Print report ini menggunakan advance string grid print. '+#13+
              'Pelajari pembuatan report menggunakan Quick Report untuk test.');
  p:=tASGPrintPreview.CreatePreview(self,asgLaporan);
  p.ShowModal;
  p.Destroy;
end;

end.
