unit AsgPrintPrev;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, AdvGrid, Grids, StdCtrls, asgprint, Buttons;

type
  TASGPrintPreview = class(TForm)
    Panel1: TPanel;
    lblPage: TLabel;
    Panel2: TPanel;
    apsPrintSetting: TAdvGridPrintSettingsDialog;
    psPrinterSetup: TPrinterSetupDialog;
    ScrollBox1: TScrollBox;
    PaintBox1: TPaintBox;
    btnPrint: TBitBtn;
    btnPrintSetup: TBitBtn;
    btnPrev: TBitBtn;
    btnNext: TBitBtn;
    btnClose: TBitBtn;
    procedure PaintBox1Paint(Sender: TObject);
    procedure btnPrevClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnPrintSetupClick(Sender: TObject);
  private
    { Private declarations }
    ASG: TAdvStringGrid;
  public
    { Public declarations }
    constructor CreatePreview(AOwner:TComponent; AAdvStringGrid:TAdvStringGrid);
  end;

var
  ASGPrintPreview: TASGPrintPreview;

implementation

{$R *.DFM}

constructor TASGPrintPreview.CreatePreview(AOwner: TComponent; AAdvStringGrid:TAdvStringGrid);
begin
  inherited Create(aOwner);
  ASG := AAdvStringGrid;
//  apsPrintSetting.Grid := ASG;
end;

procedure TASGPrintPreview.PaintBox1Paint(Sender: TObject);
var R: TRect;
begin
  ASG.printsettings.headersize:=150;

  r:=PaintBox1.clientrect;
  r.bottom:=r.bottom+PaintBox1.top;
  ASG.printpreview(PaintBox1.canvas,r);
end;

procedure TASGPrintPreview.btnPrevClick(Sender: TObject);
begin
  if (ASG.PreviewPage>1) then begin
    ASG.PreviewPage:=ASG.PreviewPage-1;
    PaintBox1.Repaint;
    lblPage.caption:='Page : '+IntToStr(ASG.PreviewPage);
  end;
end;

procedure TASGPrintPreview.btnNextClick(Sender: TObject);
begin
  ASG.PreviewPage:=ASG.PreviewPage+1;
  lblPage.caption:='Page : '+IntToStr(ASG.PreviewPage);
  PaintBox1.Repaint;
end;

procedure TASGPrintPreview.btnPrintSetupClick(Sender: TObject);
begin
  apsPrintSetting.Grid := ASG;
  if apsPrintSetting.Execute then
    PaintBox1.Repaint;
end;

procedure TASGPrintPreview.btnPrintClick(Sender: TObject);
begin
  if psPrinterSetup.Execute then
    ASG.Print;
end;

procedure TASGPrintPreview.btnCloseClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

end.
