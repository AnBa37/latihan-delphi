object frmMenu: TfrmMenu
  Left = 423
  Top = 247
  BorderStyle = bsDialog
  Caption = 'Menu Utama'
  ClientHeight = 249
  ClientWidth = 161
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnDaftarKaryawan: TButton
    Left = 32
    Top = 8
    Width = 97
    Height = 25
    Caption = '&Daftar Karyawan'
    TabOrder = 0
    OnClick = btnDaftarKaryawanClick
  end
  object BtnInputBonus: TButton
    Left = 32
    Top = 40
    Width = 97
    Height = 25
    Caption = '&Input Bonus'
    TabOrder = 1
    OnClick = BtnInputBonusClick
  end
  object btnLaporan: TButton
    Left = 32
    Top = 72
    Width = 97
    Height = 25
    Caption = '&Laporan Gaji'
    TabOrder = 2
    OnClick = btnLaporanClick
  end
  object btnClose: TButton
    Left = 32
    Top = 216
    Width = 97
    Height = 25
    Cancel = True
    Caption = '&Close'
    TabOrder = 3
    OnClick = btnCloseClick
  end
  object btnLaporanTransaksiBiaya: TButton
    Left = 32
    Top = 104
    Width = 97
    Height = 25
    Caption = 'Transaksi Biaya'
    TabOrder = 4
  end
end
