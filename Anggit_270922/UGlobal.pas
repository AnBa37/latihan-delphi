unit UGlobal;

interface

uses
  Classes, Controls, StrUtils, SysUtils, ADODB, Koneksi;

type
{Record Karyawan}
  TRKaryawan = Record
    NIK            :    integer;
    Nama           :    String[30];
    Alamat         :    String[50];
    TglLahir       :    TDateTime;
    TglMasuk       :    TDateTime;
    GajiPokok      :    currency;
    Tunjangan      :    currency;
    Departemen     :    byte;
    Posisi         :    String[20];
    Keterangan     :    String[50];
  end;

  TRDepartemen = Record
    NoDepartemen        : byte;
    NamaDepartemen      : string[30];
    Keterangan          : string[50];
  end;

  TRBonus = Record
    NIK                 : integer;
    Bonus               : currency;
    Keterangan          : string[50];
  end;

  TRSystem_Users = Record
    User_Name           : string[20];
    NIK                 : integer;
    Passwd              : string[20];
    Valid               : boolean;
  end;
{Array record karyawan}
  ARKaryawan = Array of TRKaryawan;
  ARDepartemen = Array of TRDepartemen;
  ARBonus = Array of TRBonus;
  ARSystem_Users = Array of TRSystem_Users;

{Fungsi umum}
  function CreateNewNumber(Field,Table : string) : integer;
  function NumberToString(Number,Panjang : integer) : string;

implementation

  function CreateNewNumber(Field,Table : string) : integer;
  var sql : string;
      buffer : _RecordSet;
  begin
    sql := 'SELECT MAX(' + Field + ') FROM ' + Table;
    buffer := dmKoneksi.OpenSQL(sql);
    Result := BufToInt(buffer.Fields[0].Value) + 1;
    buffer.Close;
  end;

  function NumberToString(Number,Panjang : integer) : string;
  var temp : string;
  begin
    temp := IntToStr(Number);
    while length(temp) < Panjang do temp := '0' + temp;
    Result := temp;
  end;

end.
