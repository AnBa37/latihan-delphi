unit TambahKaryawan;

interface

uses
  UGlobal,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit, ExtCtrls;

type
  TfrmTambahKaryawan = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    mmoKeterangan: TMemo;
    lblNIK: TLabel;
    txtNama: TAdvEdit;
    txtAlamat: TAdvEdit;
    txtPosisi: TAdvEdit;
    txtTunjangan: TAdvEdit;
    txtGajiPokok: TAdvEdit;
    cmbNoDept: TComboBox;
    dtpTglLahir: TDateTimePicker;
    dtpTglMasuk: TDateTimePicker;
    cmbNamaDept: TComboBox;
    btnSave: TButton;
    btnCancel: TButton;
    Bevel1: TBevel;
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure cmbNamaDeptSelect(Sender: TObject);
  private
    { Private declarations }
    idx : Integer;
    EditMode : boolean;
    procedure ResetForm;
    function CekInput : boolean;
    function IsValid : Boolean;

  public
    { Public declarations }
    procedure InitForm;
    procedure Execute;
  end;

var
  frmTambahKaryawan: TfrmTambahKaryawan;

implementation

uses Koneksi, UData, DateUtils;

{$R *.dfm}

procedure TfrmTambahKaryawan.btnCancelClick(Sender: TObject);
begin
  MOdalResult := MrCancel;
end;

procedure TfrmTambahKaryawan.btnSaveClick(Sender: TObject);
var RKaryawan : TRKaryawan;
begin
  if (cekinput = true) and (MessageDlg('Data yang akan disimpan telah benar ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
    dmkoneksi.BeginSQL;
    try
      RKaryawan.NIK             := strtoint(lblNIK.Caption);
      RKaryawan.Nama            := txtNama.Text;
      RKaryawan.Alamat          := txtAlamat.Text;
      RKaryawan.TglLahir        := dtpTglLahir.DateTime;
      RKaryawan.TglMasuk        := dtpTglMasuk.DateTime;
      RKaryawan.GajiPokok       := txtGajiPokok.IntValue;
      RKaryawan.Tunjangan       := txtTunjangan.IntValue;
      RKaryawan.Departemen      := strtoint(cmbNoDept.Text);
      RKaryawan.Posisi          := txtPosisi.Text;
      RKaryawan.Keterangan      := mmoKeterangan.Text;
      SaveKaryawan(RKaryawan);
      dmkoneksi.EndSQL;
      ResetForm;
    except
      dmkoneksi.UndoSQL;
      showmessage('Penyimpanan datat tidak berhasil.');
    end;
  end;
end;

function TfrmTambahKaryawan.CekInput: boolean;
begin
  result := true;
  if txtNama.Text = '' then begin
    Showmessage('Nama karyawan belum terisi.');
    Result := false;
    txtNama.SetFocus;
  end else begin
    //pengecekan input sesuai kebutuhan.
  end;
end;

procedure TfrmTambahKaryawan.Execute;
begin
  ResetForm;
  Showmodal
end;

procedure TfrmTambahKaryawan.ResetForm;
var i : integer;
    ADepartemen : ARDepartemen;
begin
  lblNIK.Caption := NumberToString(CreateNewNumber('NIK','Karyawan'),7);
  txtNama.Clear;
  txtAlamat.Clear;
  dtpTglLahir.DateTime := EncodeDate(1980,01,01);
  dtpTglMasuk.DateTime := now;
  txtGajiPokok.IntValue := 0;
  txtTunjangan.IntValue := 0;
  UData.GetDepartemen(0,ADepartemen);
  cmbNoDept.Clear;
  cmbNamaDept.Clear;
  for i := 0 to length(ADepartemen)-1 do begin
    cmbNoDept.Items.Append(inttostr(ADepartemen[i].NoDepartemen));
    cmbNamaDept.Items.Append(ADepartemen[i].NamaDepartemen);
  end;
  cmbNoDept.ItemIndex := 0;
  cmbNamaDept.ItemIndex := 0;
  txtPosisi.Clear;
  mmoKeterangan.Clear;
end;

procedure TfrmTambahKaryawan.cmbNamaDeptSelect(Sender: TObject);
begin
  cmbNoDept.ItemIndex := cmbNamaDept.ItemIndex;
end;

procedure TfrmTambahKaryawan.InitForm;
begin
  lblNIK.Caption := '';
  txtNama.Text := '';
  dtpTglLahir.DateTime := now;
  dtpTglMasuk.DateTime:= now;
  txtGajiPokok.Text := CurrencyString;
  txtTunjangan.Text := CurrencyString;
  txtPosisi.Text := '';
  mmoKeterangan.Lines.Text := '';
  cmbNamaDept.ItemIndex := -1;

 end;

//function TfrmTambahKaryawan.IsSaved: Boolean;
//begin

//end;

function TfrmTambahKaryawan.IsValid: Boolean;
begin
  Result := True;
    if txtNama.Text = '' then begin
    ShowMessage('Nama kosong.');
    txtNama.SetFocus;
    Result := False; exit;
  //end else if UData.GetDaftarKaryawan(lblNIK), idx) then begin
    //ShowMessage('NIK sudah ada, silahkan diganti.');
    //lblNIK.SetFocus;
    //Result := False; exit;
  end else if trim(txtNama.Text) = '' then begin
    ShowMessage('Nama kosong.');
    txtNama.SetFocus;
    Result := False; exit;
  end else if txtGajiPokok.FloatValue = 0 then begin
    ShowMessage('Gaji Pokok = 0.');
    txtGajiPokok.SetFocus;
    Result := False; exit;
  end else if txtTunjangan.FloatValue = 0 then begin
    ShowMessage('Tunjangan = 0.');
    txtTunjangan.SetFocus;
    Result := False; exit;
  end else if cmbNamaDept.ItemIndex = -1 then begin
    ShowMessage('Nama Departemen belum dipilih.');
    cmbNamaDept.SetFocus;
    Result := False; exit;
  end
end;

//procedure TfrmTambahKaryawan.Load_Data;
//var ARKaryawan : TRKaryawan;
//begin
   //Data := Select_brg(idx);
  //txtNama.Text:= ARKaryawan.Nama;
  //txtAlamat.Text := ARKaryawan.Alamat;
  //dtpTglLahir.DateTime := ARKaryawan.TglLahir;
  //dtpTglMasuk.DateTime := ARKaryawan.TglMasuk;
  //txtGajiPokok.Text := ARKaryawan.GajiPokok;
  //txtTunjangan.Text := ARKaryawan.Tunjangan;
  //txtPosisi.Text := ARKaryawan.Posisi;
  //mmoKeterangan.Lines.Text := ARKaryawan.Keterangan;

  //if ARKaryawan.Departemen = 'Procurement' then cmbDepartemen.ItemIndex := 0
  //else if ARKaryawan.Departemen = 'Marketing' then cmbDepartemen.ItemIndex := 1
  //else if ARKaryawan.Departemen = 'Finance' then cmbDepartemen.ItemIndex := 2;
  //else if ARKaryawan.Departemen = 'Accounting' then cmbDepartemen.ItemIndex := 3;
  //else if ARKaryawan.Departemen = 'Manufacturing' then cmbDepartemen.ItemIndex := 4;
  //else if ARKaryawan.Departemen = 'Sub Cont' then cmbDepartemen.ItemIndex := 5;
//end;

//function TfrmTambahKaryawan.IsSaved: Boolean;
//begin

//end;

//function TfrmTambahKaryawan.IsValid: Boolean;
//begin

//end;


end.


