unit MenuUtama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmMenu = class(TForm)
    btnDaftarKaryawan: TButton;
    BtnInputBonus: TButton;
    btnLaporan: TButton;
    btnClose: TButton;
    btnLaporanTransaksiBiaya: TButton;
    procedure btnCloseClick(Sender: TObject);
    procedure btnDaftarKaryawanClick(Sender: TObject);
    procedure BtnInputBonusClick(Sender: TObject);
    procedure btnLaporanClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenu: TfrmMenu;

implementation

uses DaftarKaryawan, InputBonus, LaporanGaji;

{$R *.dfm}

procedure TfrmMenu.btnCloseClick(Sender: TObject);
begin
  application.Terminate;
end;

procedure TfrmMenu.btnDaftarKaryawanClick(Sender: TObject);
begin
  frmDaftarKaryawan.execute;
end;

procedure TfrmMenu.BtnInputBonusClick(Sender: TObject);
begin
  frmInputBonus.execute;
end;

procedure TfrmMenu.btnLaporanClick(Sender: TObject);
begin
  frmLaporanGaji.execute;
end;

end.
