unit UData;

interface

uses
  UGlobal, Classes, Controls, StrUtils, SysUtils, ADODB, Koneksi;

{Data Karyawan}
  Procedure SaveKaryawan(RKaryawan : TRKaryawan);
  Function GetKaryawan(NIK : integer) : TRKaryawan;
  Procedure GetDaftarKaryawan(Nama,Posisi : string; Departemen : byte; var AKaryawan : ARKaryawan);
  procedure deleteKaryawan(NIK : Integer);

{Data Departemen}
  procedure SaveDepartemen(RDepartemen : TRDepartemen);
  procedure GetDepartemen(NoDepartemen : integer; var ADepartemen : ARDepartemen);

{Data Bonus}
  procedure GetBonus(NIK : integer; var ABonus : ARBonus);
  procedure SaveBonus(RBonus : TRBonus);

{System Users}
  procedure GetSystem_Users(User_Name: string; var ASystem_Users : ARSystem_Users);
  procedure updateSystem_Users(System_Users : TRSystem_Users);
  procedure deleteSystem_Users(System_Users: TRSystem_Users);

var sql : string;
    buffer : _RecordSet;

implementation

{Data karyawan}
  Procedure SaveKaryawan(RKaryawan : TRKaryawan);
  {fungsi untuk menyimpan data karyawan}
  begin
    with RKaryawan do begin
      sql := 'INSERT INTO Karyawan (NIK, Nama, Alamat, TglLahir, TglMasuk, GajiPokok, '+
                                   'Tunjangan, Departemen, Posisi, Keterangan) '+
             'VALUES ('+FormatSQLNumber(NIK)+','+
                        FormatSQLString(Nama)+','+
                        FormatSQLString(Alamat)+','+
                        FormatSQLDate(TglLahir)+','+
                        FormatSQLDate(TglMasuk)+','+
                        FormatSQLNumber(GajiPokok)+','+
                        FormatSQLNumber(Tunjangan)+','+
                        FormatSQLNumber(Departemen)+','+
                        FormatSQLString(Posisi)+','+
                        FormatSQLString(Keterangan)+')';
      dmkoneksi.ExecSQL(sql);
    end;
  end;

  Function GetKaryawan(NIK : integer) : TRKaryawan;
  {fungsi untuk mengambil satu data karyawan berdasarkan NIK}
  begin
    sql := 'SELECT NIK, Nama, Alamat, TglLahir, TglMasuk, GajiPokok, Tunjangan, '+
                  'Departemen, Posisi, Keterangan '+
           'FROM Karyawan '+
           'WHERE NIK = '+FormatSQLNumber(NIK);
    buffer := dmkoneksi.OpenSQL(sql);
    if buffer.RecordCount <> 0 then begin
      Result.NIK        := buftoint(buffer.Fields[0].Value);
      Result.Nama       := buftostr(buffer.Fields[1].Value);
      Result.Alamat     := buftostr(buffer.Fields[2].Value);
      Result.TglLahir   := buftodatetime(buffer.Fields[3].Value);
      Result.TglMasuk   := buftodatetime(buffer.Fields[4].Value);
      Result.GajiPokok  := buftofloat(buffer.Fields[5].Value);
      Result.Tunjangan  := buftofloat(buffer.Fields[6].Value);
      Result.Departemen := buftoint(buffer.Fields[7].Value);
      Result.Posisi     := buftostr(buffer.Fields[8].Value);
      Result.Keterangan := buftostr(buffer.Fields[9].Value);
    end;
    buffer.Close;
  end;

  Procedure GetDaftarKaryawan(Nama,Posisi : string; Departemen : byte; var AKaryawan : ARKaryawan);
  {fungsi untuk mengambil data karyawan berdasarkan kemiripan nama, Departemen dan/atau posisi}
  var i : integer;
  begin
    sql := 'SELECT NIK, Nama, Alamat, TglLahir, TglMasuk, GajiPokok, Tunjangan, '+
                  'Departemen, Posisi, Keterangan '+
           'FROM Karyawan '+
           'WHERE Nama LIKE '+FormatSQLString('%'+Nama+'%')+' AND '+
                 'Posisi LIKE '+FormatSQLString('%'+Posisi+'%');
    if Departemen <> 0 then begin
      sql := sql + ' AND Departemen = '+FormatSQLNumber(Departemen);
    end;
    buffer := dmkoneksi.OpenSQL(sql);
    if buffer.RecordCount <> 0 then begin
      setlength(AKaryawan,buffer.RecordCount);
      for i := 0 to buffer.RecordCount-1 do begin
        AKaryawan[i].NIK        := buftoint(buffer.Fields[0].Value);
        AKaryawan[i].Nama       := buftostr(buffer.Fields[1].Value);
        AKaryawan[i].Alamat     := buftostr(buffer.Fields[2].Value);
        AKaryawan[i].TglLahir   := buftodatetime(buffer.Fields[3].Value);
        AKaryawan[i].TglMasuk   := buftodatetime(buffer.Fields[4].Value);
        AKaryawan[i].GajiPokok  := buftofloat(buffer.Fields[5].Value);
        AKaryawan[i].Tunjangan  := buftofloat(buffer.Fields[6].Value);
        AKaryawan[i].Departemen := buftoint(buffer.Fields[7].Value);
        AKaryawan[i].Posisi     := buftostr(buffer.Fields[8].Value);
        AKaryawan[i].Keterangan := buftostr(buffer.Fields[9].Value);
        buffer.MoveNext;
      end;
    end else begin
      AKaryawan := nil;
    end;
    buffer.Close;
  end;

{Data Departemen}
  procedure SaveDepartemen(RDepartemen : TRDepartemen);
  {fungsi untuk menyimpan data departemen}
  begin
    with RDepartemen do begin
      sql := 'INSERT INTO (NoDepartemen, NamaDepartemen, Keterangan) '+
             'VALUES ('+FormatSQLNumber(NoDepartemen)+','+
                        FormatSQLString(NamaDepartemen)+','+
                        FormatSQLString(Keterangan)+')';
      dmkoneksi.ExecSQL(sql);
    end;
  end;

  procedure GetDepartemen(NoDepartemen : integer; var ADepartemen : ARDepartemen);
  {fungsi untuk mengambil data departemen berdasarkan NoDepartemen}
  var i : integer;
  begin
    sql := 'SELECT NoDepartemen, NamaDepartemen, Keterangan FROM Departemen ';
    if NoDepartemen <> 0 then begin
      sql := sql + ' WHERE NoDepartemen = '+FormatSQLNumber(NoDepartemen);
    end;
    buffer := dmkoneksi.OpenSQL(sql);
    if buffer.RecordCount <> 0 then begin
      setlength(ADepartemen,buffer.RecordCount);
      for i := 0 to buffer.RecordCount-1 do begin
        ADepartemen[i].NoDepartemen := buftoint(buffer.Fields[0].Value);
        ADepartemen[i].NamaDepartemen := buftostr(buffer.Fields[1].Value);
        ADepartemen[i].Keterangan := buftostr(buffer.Fields[2].Value);
        buffer.MoveNext;
      end;
    end else begin
      ADepartemen := nil;
    end;
    buffer.Close;
  end;

  procedure deleteKaryawan(NIK: Integer);
  begin
   sql := 'Delete from Karyawan WHERE NIK = '+FormatSQLNumber(NIK);
  dmkoneksi.ExecSQL(sql);
  end;

{Data Bonus}
  procedure GetBonus(NIK : integer; var ABonus : ARBonus);
  var i : integer;
  begin
    sql := 'SELECT NIK, Bonus, Keterangan FROM Bonus ';
    if NIK <> 0 then begin
      sql := sql + 'WHERE NIK = '+FormatSQLNumber(NIK);
    end;
    buffer := dmkoneksi.OpenSQL(sql);
    if buffer.RecordCount <> 0 then begin
      setlength(ABonus,buffer.RecordCount);
      for i := 0 to buffer.RecordCount-1 do begin
        ABonus[i].NIK := buftoint(buffer.Fields[0].Value);
        ABonus[i].Bonus := buftofloat(buffer.Fields[1].Value);
        ABonus[i].Keterangan := buftostr(buffer.Fields[2].Value);
        buffer.MoveNext;
      end;
    end else begin
      ABonus := nil;
    end;
    buffer.Close;
  end;

  procedure SaveBonus(RBonus : TRBonus);
  begin
    sql := 'SELECT Count(*) FROM Bonus WHERE NIK = '+FormatSQLNumber(RBonus.NIK);
    buffer := dmkoneksi.OpenSQL(sql);
    if buftoint(buffer.Fields[0].Value) = 0 then begin
      sql := 'INSERT INTO Bonus (NIK, Bonus, Keterangan) '+
             'VALUES('+FormatSQLNumber(RBonus.NIK)+','+
                       FormatSQLNumber(RBonus.Bonus)+','+
                       FormatSQLString(RBonus.Keterangan)+')';
    end else begin
      sql := 'UPDATE Bonus SET Bonus = '+FormatSQLNumber(RBonus.Bonus)+', '+
                              'Keterangan = '+FormatSQLString(RBonus.Keterangan)+' '+
             'WHERE NIK = '+FormatSQLNumber(RBonus.NIK);
    end;
    dmkoneksi.ExecSQL(sql);
    buffer.Close;
  end;

  {Data System_Users}
  procedure GetSystem_Users(User_Name: string; var ASystem_Users : ARSystem_Users);
  var i : integer;
  {fungsi untuk mengambil data System_Users berdasarkan User_Name}
  begin
  sql := 'SELECT User_Name FROM System_Users ';
    if User_Name <> '' then begin
      sql := sql + 'WHERE User_Name = '+FormatSQLString(User_Name);
    end;
    buffer := dmkoneksi.OpenSQL(sql);
    if buffer.RecordCount <> 0 then begin
      setlength(ASystem_Users,buffer.RecordCount);
      for i := 0 to buffer.RecordCount-1 do begin
        ASystem_Users[i].User_Name := buftostr(buffer.Fields[0].Value);
        ASystem_Users[i].NIK := buftoint(buffer.Fields[1].Value);
        ASystem_Users[i].Passwd := buftostr(buffer.Fields[2].Value);
        //ASystem_Users[i].Valid := buftostr(buffer.Fields[3].Value);
        buffer.MoveNext;
      end;
    end else begin
      ASystem_Users := nil;
    end;
    buffer.Close
  end;

  procedure updateSystem_Users(System_Users : TRSystem_Users);
  begin
  sql :='Update System_Users SET '+
        'User_Name = '+FormatSQLString(System_Users.User_Name)+','+
        'NIK = '+FormatsqlNumber(System_Users.NIK)+','+
        'Passwd = '+FormatsqlString(System_Users.Passwd)+','+
        'Where User_Name = '+FormatsqlString(System_Users.User_Name);
  dmkoneksi.ExecSQL(sql);
end;

 procedure deleteSystem_Users(System_Users: TRSystem_Users);
 begin
 sql := 'Delete from System_Users where User_Name = '+FormatSQLString(System_Users.User_Name);
  dmkoneksi.ExecSQL(sql);
 end;

end.
