unit DaftarKaryawan;

interface

uses
  UGlobal, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, AdvEdit;

type
  TfrmDaftarKaryawan = class(TForm)
    asgDaftar: TAdvStringGrid;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    txtNama: TAdvEdit;
    txtPosisi: TAdvEdit;
    btnCari: TButton;
    btnTambah: TButton;
    btnPrint: TButton;
    btnClose: TButton;
    btnEdit: TButton;
    btnHapus: TButton;
    procedure btnCloseClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure asgDaftarGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgDaftarPrintSetColumnWidth(Sender: TObject; ACol: Integer;
      var Width: Integer);
    procedure asgDaftarGetCellPrintColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure btnCariClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    //procedure btnEditClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillGrid;
    procedure ResetForm;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmDaftarKaryawan: TfrmDaftarKaryawan;

implementation

uses UData, AsgPrintPrev, Koneksi, TambahKaryawan;

const

  ColNo         = 0;
  ColNIK        = 1;
  ColNama       = 2;
  ColAlamat     = 3;
  ColTglLahir   = 4;
  ColTglMasuk   = 5;
  ColGaji       = 6;
  ColTunjangan  = 7;
  ColDepartemen = 8;
  ColPosisi     = 9;
  ColKeterangan = 10;
{$R *.dfm}

procedure TfrmDaftarKaryawan.btnCloseClick(Sender: TObject);
begin
  ModalResult := MrCancel;
end;

procedure TfrmDaftarKaryawan.Execute;
begin
  ResetForm;
  FillGrid;
  Showmodal;
end;

procedure TfrmDaftarKaryawan.FillGrid;
var i : integer;
    AKaryawan : ARKaryawan;
    ADepartemen : ARDepartemen;
begin
  asgDaftar.ClearNormalCells;
  asgDaftar.RowCount := 1;
  UData.GetDaftarKaryawan(txtNama.Text,txtPosisi.Text,0,AKaryawan);
  for i := 0 to length(AKaryawan)-1 do begin
    asgDaftar.AddRow;
    asgDaftar.Cells[ColNIK,i+1] := NumberToString(AKaryawan[i].NIK,7);
    asgDaftar.Cells[ColNama,i+1] := AKaryawan[i].Nama;
    asgDaftar.Cells[ColAlamat,i+1] := AKaryawan[i].Alamat;
    asgDaftar.Dates[ColTglLahir,i+1] := AKaryawan[i].TglLahir;
    asgDaftar.Dates[ColTglMasuk,i+1] := AKaryawan[i].TglMasuk;
    asgDaftar.Floats[ColGaji,i+1] := Akaryawan[i].GajiPokok;
    asgDaftar.Floats[ColTunjangan,i+1] := Akaryawan[i].Tunjangan;
    UData.GetDepartemen(AKaryawan[i].Departemen,ADepartemen);
    asgDaftar.Cells[ColDepartemen,i+1] := ADepartemen[0].NamaDepartemen;
    asgDaftar.Cells[ColPosisi,i+1] := AKaryawan[i].Posisi;
    asgDaftar.Cells[ColKeterangan,i+1] := AKaryawan[i].Keterangan;
  end;
  asgDaftar.AutoNumberCol(ColNo);
  asgDaftar.AutoSizeColumns(true,2);
end;

procedure TfrmDaftarKaryawan.ResetForm;
begin
  txtNama.Clear;
  txtPosisi.Clear;
end;

procedure TfrmDaftarKaryawan.btnPrintClick(Sender: TObject);
var p:tASGPrintPreview;
begin
  showmessage('Print report ini menggunakan advance string grid print. '+#13+
              'Pelajari pembuatan report menggunakan Quick Report untuk test.');
  p:=tASGPrintPreview.CreatePreview(self,asgDaftar);
  p.ShowModal;
  p.Destroy;
end;

procedure TfrmDaftarKaryawan.asgDaftarGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ACol in [ColNo, ColGaji, ColTunjangan] then HAlign := TARightJustify;
  if Arow = 0 then HAlign := TACenter
end;

procedure TfrmDaftarKaryawan.asgDaftarPrintSetColumnWidth(Sender: TObject;
  ACol: Integer; var Width: Integer);
begin
  if ACol = asgDaftar.ColCount-1 then Width := -20;
end;

procedure TfrmDaftarKaryawan.asgDaftarGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  if ARow = 0 then AFont.Style := [fsBold];
end;

procedure TfrmDaftarKaryawan.btnCariClick(Sender: TObject);
begin
  FillGrid;
end;

procedure TfrmDaftarKaryawan.btnTambahClick(Sender: TObject);
begin
  frmTambahKaryawan.execute;
  FillGrid;
end;

procedure TfrmDaftarKaryawan.btnHapusClick(Sender: TObject);
begin
 if (MessageDlg('Data hendak dihapus ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
    dmKoneksi.BeginSQL;
    try
    UData.deleteKaryawan(asgDaftar.Ints[ColNIK, asgDaftar.Row]);
      dmkoneksi.EndSQL;
      FillGrid;
    except
      dmkoneksi.UndoSQL;
      ShowMessage('Hapus data GAGAL...');
    end;
  end;
end;

//procedure TfrmDaftarKaryawan.btnEditClick(Sender: TObject);
//begin
 //if frmTambahKaryawan.Execute(asgDaftar.Ints[ColNIK, asgDaftar.Row]) then FillGrid;
//end;

procedure TfrmDaftarKaryawan.btnEditClick(Sender: TObject);
begin
 //if frmTambahKaryawan.Execute(asgDaftar.Ints[ColNIK, asgDaftar.Row]) then load_data;
end;


end.

