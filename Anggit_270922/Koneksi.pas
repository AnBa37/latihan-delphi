unit Koneksi;

interface
//{$R WINXP.RES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, ADODB, DBTables,jpeg;

type
  TdmKoneksi = class(TDataModule)
    ADCData: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ExecSQL(ASQL: string);
    function OpenSQL(ASQL: string): _RecordSet;
    procedure BeginSQL;
    procedure UndoSQL;
    procedure EndSQL;
  end;

var
  dmKoneksi: TdmKoneksi;

  procedure ValueListToList(AList, AValueList: TStrings);
  procedure SQLToStringList(var AStringList: TStringList; ASQL: string);
  procedure SQLToStringValueList(var AStringList: TStringList; ASQL: string);
  procedure SQLToStringValueListDef(var AStringList: TStringList; ASQL: string; ADefault: string);

  function IsAlphaExist(AString: string): boolean;
  function IsNumericExist(AString: string): boolean;
  function IsNonAlphaNumerinExist(AString: string): boolean;
  function IntToStrFixed(MinLen: integer; Input: integer): string;
  function CekInteger(AString: string): boolean;
  function CekFloat(AString: string): boolean;
  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  function TrimAll(Input: string): string;
  procedure FillString(var Input: string; Count: integer; Value: char);

  function CekIntegerFmt(AString: string): boolean;
  function StrFmtToIntDef(AString: string; ADefault: integer): integer;
  function StrFmtToInt(AString: string): integer;
  function StrFmtToFloatDef(AString: string; ADefault: real): real;
  function StrFmtToFloat(AString: string): real;
  function CekFloatFmt(AString: string): boolean;
  function IntToStrFmt(AInteger: integer): string;
  function FloatToStrFmt(AFloat: real): string;
  function FloatToStrFmt1(AFloat: real): string;
  function FloatToStrFmt2(AFloat: real): string;
  function FloatToStrFmt3(AFloat: real): string;
  function IntToStrCurrency(AInteger: integer): string;

  function BufToStr(ABuffer: variant): string;
  function BufToStrDef(ABuffer: variant; ADefault: string): string;
  function BufToInt(ABuffer: variant): integer;
  function BufToIntDef(ABuffer: variant; ADefault: integer): integer;
  function BufToFloat(ABuffer: variant): real;
  function BufToFloatDef(ABuffer: variant; ADefault: real): real;
  function BufToDateTime(ABuffer: variant): TDateTime;
  function BufToDateTimeDef(ABuffer: variant; ADefault: TDateTime): TDateTime;

//  function FormatSQLDateTime(ADateTime: TDateTime): string;
  function FormatSQLDate(ADate: TDate): string;
//  function FormatSQLTime(ATime: TTime): string;
  //function FormatSQLDateTimeNow: string;
  //function FormatSQLDateNow: string;
  function FormatSQLString(AString: string): string;
  function FormatSQLNumber(ANumber: integer): string; overload;
  function FormatSQLNumber(ANumber: real): string; overload;
  //function FormatSQLNumber(ANumber: string): string; overload;
//  function FormatSQLNULL: string;

implementation

uses Variants, DateUtils;
{$R *.dfm}

  procedure TdmKoneksi.DataModuleCreate(Sender: TObject);
  begin
    adcData.Connected:=true;
  end;

  procedure TdmKoneksi.DataModuleDestroy(Sender: TObject);
  begin
    adcData.Connected:=false;
  end;

  procedure TdmKoneksi.ExecSQL(ASQL: string);
  begin
    adcData.Execute(ASQL);
  end;

  function TdmKoneksi.OpenSQL(ASQL: string): _RecordSet;
  begin
    Result := adcData.Execute(ASQL);
  end;

  procedure TdmKoneksi.BeginSQL;
  begin
    adcData.BeginTrans;
  end;

  procedure TdmKoneksi.UndoSQL;
  begin
    adcData.RollbackTrans;
  end;

  procedure TdmKoneksi.EndSQL;
  begin
    adcData.CommitTrans;
  end;

{---------------------------------------------------------------}

  procedure ValueListToList(AList, AValueList: TStrings);
  var i: integer;
  begin
    AList.Clear;
    for i:=0 to AValueList.Count-1 do
      AList.Add(AValueList.Values[AValueList.Names[i]]);
  end;

  procedure SQLToStringList(var AStringList: TStringList; ASQL: string);
  var buffer: _RecordSet;
    i: integer;
  begin
    AStringList.Clear;
    buffer := dmKoneksi.OpenSQL(ASQL);
    for i:=1 to buffer.RecordCount do begin
      AStringList.Add(BufToStr(buffer.Fields[0].Value));
      buffer.MoveNext;
    end;
    buffer.Close;
  end;

  procedure SQLToStringValueList(var AStringList: TStringList; ASQL: string);
  var buffer: _RecordSet;
    i: integer;
  begin
    buffer := dmKoneksi.OpenSQL(ASQL);
    for i:=1 to buffer.RecordCount do begin
      AStringList.Add(BufToStr(buffer.Fields[0].Value)+'='+BufToStr(buffer.Fields[1].Value));
      buffer.MoveNext;
    end;
    buffer.Close;
  end;

  procedure SQLToStringValueListDef(var AStringList: TStringList; ASQL: string; ADefault: string);
  var buffer: _RecordSet;
    i: integer;
  begin
    buffer := dmKoneksi.OpenSQL(ASQL);
    for i:=1 to buffer.RecordCount do begin
      AStringList.Add(BufToStr(buffer.Fields[0].Value)+'='+BufToStrDef(buffer.Fields[1].Value,ADefault));
      buffer.MoveNext;
    end;
    buffer.Close;
  end;

{ ---------------------------------------------------------------------------- }

  function CekIntegerFmt(AString: string): boolean;
  begin
    Result := CekInteger(ReplaceSubStr(AString,ThousandSeparator,''));
  end;

  function CekFloatFmt(AString: string): boolean;
  begin
    Result := CekFloat(ReplaceSubStr(AString,ThousandSeparator,''));
  end;

  function StrFmtToIntDef(AString: string; ADefault: integer): integer;
  begin
    if (CekIntegerFmt(AString)) then
      Result := StrToInt(ReplaceSubStr(AString,ThousandSeparator,''))
    else
      Result := ADefault;
  end;

  function StrFmtToInt(AString: string): integer;
  begin
    Result := StrFmtToIntDef(AString, 0);
  end;

  function StrFmtToFloatDef(AString: string; ADefault: real): real;
  begin
    if (CekFloatFmt(AString)) then
      Result := StrToFloat(ReplaceSubStr(AString,ThousandSeparator,''))
    else
      Result := ADefault;
  end;

  function StrFmtToFloat(AString: string): real;
  begin
    Result := StrFmtToFloatDef(AString,0.00);
  end;

  function IntToStrFmt(AInteger: integer): string;
  begin
    Result := FormatFloat('#,##0',AInteger);
  end;

  function FloatToStrFmt(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0',AFloat);
  end;

  function FloatToStrFmt1(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.0',AFloat);
  end;

  function FloatToStrFmt2(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.00',AFloat);
  end;

  function FloatToStrFmt3(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.000',AFloat);
  end;

  function IntToStrCurrency(AInteger: integer): string;
  begin
    Result := 'Rp '+FormatFloat('#,##0.00',AInteger);
  end;

{ ---------------------------------------------------------------------------- }

  function BufToStr(ABuffer: variant): string;
  begin
    Result := BufToStrDef(ABuffer,'');
  end;

  function BufToStrDef(ABuffer: variant; ADefault: string): string;
  begin
    if (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufToInt(ABuffer: variant): integer;
  begin
    Result := BufToIntDef(ABuffer,0);
  end;

  function BufToIntDef(ABuffer: variant; ADefault: integer): integer;
  begin
    if (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufToFloat(ABuffer: variant): real;
  begin
    Result := BufToFloatDef(ABuffer,0);
  end;

  function BufToFloatDef(ABuffer: variant; ADefault: real): real;
  begin
    if (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufToDateTime(ABuffer: variant): TDateTime;
  begin
    Result := BufToDateTimeDef(ABuffer,0);
  end;

  function BufToDateTimeDef(ABuffer: variant; ADefault:TDateTime): TDateTime;
  begin
    if (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

{ ---------------------------------------------------------------------------- }

  function IsNumericExist(AString: string): boolean;
  var i: integer;
  begin
    Result := false;
    for i:=1 to length(AString) do
      if (AString[i] in ['0'..'9']) then Result := true;
  end;

  function IsAlphaExist(AString: string): boolean;
  var i: integer;
  begin
    Result := false;
    for i:=1 to length(AString) do
      if (AString[i] in ['a'..'z','A'..'Z']) then Result := true;
  end;

  function IsNonAlphaNumerinExist(AString: string): boolean;
  var i: integer;
  begin
    Result := false;
    for i:=1 to length(AString) do
      if not (AString[i] in ['0'..'9','a'..'z','A'..'Z']) then Result := true;
  end;

  function IntToStrFixed(MinLen: integer; Input: integer): string;
  var TmpStr: string;
    i: integer;
  begin
    TmpStr:=IntToStr(Input);
    for i:=1 to MinLen-length(TmpStr) do
      TmpStr:='0'+TmpStr;
    result:=TmpStr;
  end;

  function CekInteger(AString: string): boolean;
  begin
    try
      StrToInt(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  function CekFloat(AString: string): boolean;
  begin
    try
      StrToFloat(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  { I.S. : Input, Find, dan ReplaceWith terdefinisi
    F.S. : menghasilkan string Input di mana substring Find diganti dengan
           substring ReplaceWith }
  var i: integer;
      Tmp: string;
  begin
    while (Pos(Find,Input)>0) do begin
      i:=Pos(Find,Input);
      Delete(Input,i,Length(Find));
      Tmp:=Copy(Input,i,Length(Input)-i+2);
      Delete(Input,i,Length(Input)-i+2);
      Input:=Input+ReplaceWith+Tmp;
    end;

    Result:=Input;
  end;

  function TrimAll(Input: string): string;
  { I.S. : Input terdefinisi
    F.S. : menghasilkan string dengan spasi awal dan akhir dihilangkan
           dan spasi antar kata sebanyak satu spasi }
  var i: integer;
    TmpStr: string;
  begin
    Input:=Trim(Input);
    TmpStr:='';
    for i:=1 to Length(Input) do
      if (Input[i]<>' ') or ((Input[i]=' ') and (TmpStr[Length(TmpStr)]<>' ')) then
        TmpStr:=TmpStr+Input[i];

    Result:=TmpStr;
  end;

  procedure FillString(var Input: string; Count: integer; Value: char);
  var i: integer;
  begin
    Input := '';
    for i:=1 to Count do
      Input := Input + Value;
  end;

{ ---------------------------------------------------------------------------- }
  function FormatSQLNULL: string;
  begin
    Result := 'NULL';
  end;

  function FormatSQLNumber(ANumber: integer): string; overload;
  begin
    Result := IntToStr(ANumber);
  end;

  function FormatSQLNumber(ANumber: real): string; overload;
  begin
    Result := ReplaceSubStr(FloatToStr(ANumber),DecimalSeparator,'.');
  end;

  function FormatSQLString(AString: string): string;
  begin
    Result := QuotedStr(AString);
  end;

  function FormatSQLDate(ADate: TDate): string;
  begin
    Result := 'DATEVALUE('+FormatSQLString(FormatDateTime(ShortDateFormat, ADate))+')';
  end;

  function FormatSQLTime(ATime: TTime): string;
  begin          
    Result := 'TIMEVALUE('+FormatSQLString(FormatDateTime(LongTimeFormat, ATime))+')';
  end;

end.
