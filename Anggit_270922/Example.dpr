program Example;

uses
  Forms,
  MenuUtama in 'MenuUtama.pas' {frmMenu},
  Koneksi in 'Koneksi.pas' {dmKoneksi: TDataModule},
  AsgPrintPrev in 'AsgPrintPrev.pas' {ASGPrintPreview},
  DaftarKaryawan in 'DaftarKaryawan.pas' {frmDaftarKaryawan},
  UGlobal in 'UGlobal.pas',
  UData in 'UData.pas',
  TambahKaryawan in 'TambahKaryawan.pas' {frmTambahKaryawan},
  InputBonus in 'InputBonus.pas' {frmInputBonus},
  LaporanGaji in 'LaporanGaji.pas' {frmLaporanGaji},
  TransaksiBiaya in 'TransaksiBiaya.pas' {Form1},
  LaporanBiaya in 'LaporanBiaya.pas' {Form2};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TdmKoneksi, dmKoneksi);
  Application.CreateForm(TASGPrintPreview, ASGPrintPreview);
  Application.CreateForm(TfrmDaftarKaryawan, frmDaftarKaryawan);
  Application.CreateForm(TfrmTambahKaryawan, frmTambahKaryawan);
  Application.CreateForm(TfrmInputBonus, frmInputBonus);
  Application.CreateForm(TfrmLaporanGaji, frmLaporanGaji);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
