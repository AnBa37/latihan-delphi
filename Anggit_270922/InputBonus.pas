unit InputBonus;

interface

uses
  UGlobal, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, BaseGrid, AdvGrid;

type
  TfrmInputBonus = class(TForm)
    asgBonus: TAdvStringGrid;
    Label1: TLabel;
    btnSave: TButton;
    btnCancel: TButton;
    procedure btnCancelClick(Sender: TObject);
    procedure asgBonusCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgBonusGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgBonusGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgBonusKeyPress(Sender: TObject; var Key: Char);
    procedure asgBonusCellValidate(Sender: TObject; Col, Row: Integer;
      var Value: String; var Valid: Boolean);
    procedure asgBonusGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure btnSaveClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillGrid;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmInputBonus: TfrmInputBonus;

implementation

uses UData, Koneksi;

const
  ColNo         = 0;
  ColNIK        = 1;
  ColNama       = 2;
  ColGaji       = 3;
  ColTunjangan  = 4;
  ColBonus      = 5;
  ColTotal      = 6;
  ColKeterangan = 7;
{$R *.dfm}

{ TfrmInputBonus }

procedure TfrmInputBonus.Execute;
begin
  FillGrid;
  Showmodal;
end;

procedure TfrmInputBonus.FillGrid;
var i : integer;
    AKaryawan : ARKaryawan;
    ABonus : ARBonus;
begin
  asgBonus.ClearNormalCells;
  asgBonus.RowCount := 2;
  UData.GetDaftarKaryawan('','',0,AKaryawan);
  for i := 0 to length(AKaryawan)-1 do begin
    asgBonus.AddRow;
    asgBonus.Cells[ColNIK,i+1] := NumberToString(AKaryawan[i].NIK,7);
    asgBonus.Cells[ColNama,i+1] := AKaryawan[i].Nama;
    asgBonus.Floats[ColGaji,i+1] := AKaryawan[i].GajiPokok;
    asgBonus.Floats[ColTunjangan,i+1] := AKaryawan[i].Tunjangan;
    UData.GetBonus(AKaryawan[i].NIK,ABonus);
    if ABonus <> nil then begin
      asgBonus.Floats[ColBonus,i+1] := ABonus[0].Bonus;
      asgBonus.Floats[ColTotal,i+1] := AKaryawan[i].GajiPokok + AKaryawan[i].Tunjangan +
                                       ABonus[0].Bonus;
    end else begin
      asgBonus.Floats[ColTotal,i+1] := AKaryawan[i].GajiPokok + AKaryawan[i].Tunjangan;
    end;
  end;
  asgBonus.FloatingFooter.ColumnCalc[ColGaji] := AcSum;
  asgBonus.FloatingFooter.ColumnCalc[ColTunjangan] := AcSum;
  asgBonus.FloatingFooter.ColumnCalc[ColBonus] := AcSum;
  asgBonus.FloatingFooter.ColumnCalc[ColTotal] := AcSum;
  asgBonus.AutoNumberCol(ColNo);
  asgBonus.AutoSizeColumns(true,2);
  asgBonus.Col := ColBonus;
  asgBonus.Row := 1;
end;

procedure TfrmInputBonus.btnCancelClick(Sender: TObject);
begin
  ModalResult := MrCancel;
end;

procedure TfrmInputBonus.asgBonusCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if Acol in [ColBonus,ColKeterangan] then CanEdit := true;
end;

procedure TfrmInputBonus.asgBonusGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ACol in [ColNo, ColGaji..ColTotal] then HAlign := TARightJustify;
  if ARow = 0 then HAlign := TACenter;
end;

procedure TfrmInputBonus.asgBonusGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  if ACol = ColBonus then AEditor := edNumeric;
end;

procedure TfrmInputBonus.asgBonusKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    asgBonus.HideInplaceEdit;
    asgBonus.Row := asgBonus.Row + 1;
    asgBonus.Col := ColBonus;
  end;
end;

procedure TfrmInputBonus.asgBonusCellValidate(Sender: TObject; Col,
  Row: Integer; var Value: String; var Valid: Boolean);
begin
  if value <> '' then begin
    if Col = ColBonus then begin
      asgBonus.Floats[ColTotal,asgBonus.Row] := asgBonus.RowSum(asgBonus.Row,ColGaji,ColBonus);
      asgBonus.FloatingFooter.ColumnCalc[ColBonus] := AcSum;
      asgBonus.FloatingFooter.ColumnCalc[ColTotal] := AcSum;
      value := floattostrfmt(strtofloat(value));
    end;
    asgBonus.AutoSizeColumns(true,2);
  end;
end;

procedure TfrmInputBonus.asgBonusGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if ARow = asgBonus.RowCount-1 then AFont.Style := [fsBold];
end;

procedure TfrmInputBonus.btnSaveClick(Sender: TObject);
var i : integer;
    RBonus : TRBonus;
begin
  if (MessageDlg('Data yang akan disimpan telah benar ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
    dmkoneksi.BeginSQL;
    try
      for i := 1 to asgBonus.RowCount-2 do begin
        if asgBonus.Cells[ColNIK,i] <> '' then begin
          RBonus.NIK := asgBonus.Ints[ColNIK,i];
          RBonus.Bonus := asgBonus.Floats[ColBonus,i];
          RBonus.Keterangan := asgBonus.Cells[ColKeterangan,i];
          SaveBonus(RBonus);
        end;
      end;
      dmkoneksi.EndSQL;
      ModalResult := MrOK;
    except
      dmkoneksi.UndoSQL;
      showmessage('Penyimpanan data tidak berhasil.');
    end;
  end;
end;

end.


