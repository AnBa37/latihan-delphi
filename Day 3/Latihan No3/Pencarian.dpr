program Pencarian;

uses
  Forms,
  MenuCari in 'MenuCari.pas' {frmMenu},
  MenuProses in 'MenuProses.pas' {frmProses};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmProses, frmProses);
  Application.Run;
end.
