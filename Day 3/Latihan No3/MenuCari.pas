unit MenuCari;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmMenu = class(TForm)
    MainPanel: TPanel;
    txtAngka1: TAdvEdit;
    txtHuruf1: TAdvEdit;
    txtAngka2: TAdvEdit;
    txtHuruf2: TAdvEdit;
    txtAngka3: TAdvEdit;
    btnShowAll: TButton;
    btnProses: TButton;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
    procedure btnShowAllClick(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtHuruf1Change(Sender: TObject);
  private
    { Private declarations }


  public
    { Public declarations }
  end;

var
  frmMenu: TfrmMenu;

implementation
 uses MenuProses;

{$R *.dfm}

procedure TfrmMenu.btnTutupClick(Sender: TObject);
begin
 Close;
end;

procedure TfrmMenu.btnShowAllClick(Sender: TObject);
begin
 ShowMessage(txtAngka1.Text);
 //if txtHuruf1.Text = '' then
 //ShowMessage('Kolom 2 Ini Harus Diisi dengan String!!');
 ShowMessage(txtHuruf1.Text);
 ShowMessage(txtAngka2.Text);
 //if txtHuruf2.Text = '' then
 //ShowMessage('Kolom 4 Ini Harus Diisi dengan String!!');
 showMessage(txtHuruf2.Text);
 ShowMessage(txtAngka3.Text);
 //ShowMessage(txtHuruf1.Text);

 //showMessage(txtHuruf2.Text);


end;

procedure TfrmMenu.btnProsesClick(Sender: TObject);
begin
 frmProses.Show;
end;

procedure TfrmMenu.FormShow(Sender: TObject);
begin
 InitForm;
end;

procedure TfrmMenu.txtHuruf1Change(Sender: TObject);
begin
 if not (key in['a'..'z',#8,#13]) then key = #0;  //hingga itu pake ..
end;

end.
