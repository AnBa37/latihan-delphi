unit MenuProses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmProses = class(TForm)
    SearchPanel: TPanel;
    txtPencari: TAdvEdit;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProses: TfrmProses;

implementation
 uses MenuCari;
{$R *.dfm}

procedure TfrmProses.btnTutupClick(Sender: TObject);
begin
 close;
 frmMenu.Show;
end;

procedure TfrmProses.btnTampilClick(Sender: TObject);
begin
  if txtPencari.Text = '1' then
  ShowMessage('Isi Kolom Ke-1 = ' +frmMenu.txtAngka1.Text);
  if txtPencari.Text = '2' then
  ShowMessage('Isi Kolom Ke-2 = ' +frmMenu.txtHuruf1.Text);
  if txtPencari.Text = '3' then
  ShowMessage('Isi Kolom Ke-3 = ' +frmMenu.txtAngka2.Text);
  if txtPencari.Text = '4' then
  ShowMessage('Isi Kolom Ke-4 = ' +frmMenu.txtHuruf2.Text);
  if txtPencari.Text = '5' then
  ShowMessage('Isi Kolom Ke-5 = ' +frmMenu.txtAngka3.Text);
  if (txtPencari.Text >= '6') or (txtPencari.Text <= '0') then
  ShowMessage('Input 1-5');

end;

end.
