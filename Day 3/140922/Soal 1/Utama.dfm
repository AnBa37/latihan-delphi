object frmUtama: TfrmUtama
  Left = 235
  Top = 178
  BorderStyle = bsDialog
  Caption = 'Form Utama'
  ClientHeight = 83
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblJudul: TLabel
    Left = 8
    Top = 16
    Width = 199
    Height = 20
    Caption = 'Selamat Datang Kembali'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnProses: TButton
    Left = 32
    Top = 45
    Width = 75
    Height = 25
    Caption = 'Proses'
    TabOrder = 0
    OnClick = btnProsesClick
  end
  object btnTutup: TButton
    Left = 112
    Top = 45
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 1
    OnClick = btnTutupClick
  end
end
