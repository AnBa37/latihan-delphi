program Tahap1No1;

uses
  Forms,
  Utama in 'Utama.pas' {frmUtama},
  Tampil in 'Tampil.pas' {frmTampil},
  Proses in 'Proses.pas' {frmProses};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmUtama, frmUtama);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.CreateForm(TfrmProses, frmProses);
  Application.Run;
end.
