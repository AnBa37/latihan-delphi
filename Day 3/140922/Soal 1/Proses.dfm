object frmProses: TfrmProses
  Left = 206
  Top = 183
  BorderStyle = bsDialog
  Caption = 'Hasil Proses'
  ClientHeight = 81
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblHasil: TLabel
    Left = 8
    Top = 16
    Width = 59
    Height = 20
    Caption = 'lblHasil'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnTutup: TButton
    Left = 8
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 0
    OnClick = btnTutupClick
  end
end
