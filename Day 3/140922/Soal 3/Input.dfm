object frmInput: TfrmInput
  Left = 553
  Top = 295
  BorderStyle = bsDialog
  Caption = 'Input'
  ClientHeight = 110
  ClientWidth = 173
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object txt1: TAdvEdit
    Left = 8
    Top = 8
    Width = 153
    Height = 21
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 0
    Text = 'txt1'
    Visible = True
    Version = '2.9.2.1'
  end
  object txt2: TAdvEdit
    Left = 8
    Top = 40
    Width = 153
    Height = 21
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 1
    Text = 'txt2'
    Visible = True
    Version = '2.9.2.1'
  end
  object btnProses: TButton
    Left = 8
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Proses'
    TabOrder = 2
    OnClick = btnProsesClick
  end
  object btnTutup: TButton
    Left = 88
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 3
    OnClick = btnTutupClick
  end
end
