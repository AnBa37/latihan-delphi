object frmTampil: TfrmTampil
  Left = 288
  Top = 216
  BorderStyle = bsDialog
  Caption = 'Tampil Data'
  ClientHeight = 135
  ClientWidth = 185
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnShowAll: TButton
    Left = 8
    Top = 8
    Width = 169
    Height = 25
    Caption = 'Tampilkan SEMUA DATA'
    TabOrder = 0
    OnClick = btnShowAllClick
  end
  object btnByNama: TButton
    Left = 8
    Top = 40
    Width = 169
    Height = 25
    Caption = 'Tampilkan Berdasarkan Nama'
    TabOrder = 1
    OnClick = btnByNamaClick
  end
  object btnByNilai: TButton
    Left = 8
    Top = 72
    Width = 169
    Height = 25
    Caption = 'Tampilkan Berdasarkan Nilai'
    TabOrder = 2
    OnClick = btnByNilaiClick
  end
  object btnByTgl: TButton
    Left = 8
    Top = 104
    Width = 169
    Height = 25
    Caption = 'Tampilkan Berdasarkan Tanggal'
    TabOrder = 3
    OnClick = btnByTglClick
  end
end
