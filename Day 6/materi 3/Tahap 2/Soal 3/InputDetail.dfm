object frmIsiData: TfrmIsiData
  Left = 222
  Top = 209
  BorderStyle = bsDialog
  Caption = 'Isi Data Detail'
  ClientHeight = 135
  ClientWidth = 182
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 12
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label2: TLabel
    Left = 8
    Top = 44
    Width = 20
    Height = 13
    Caption = 'Nilai'
  end
  object Label3: TLabel
    Left = 8
    Top = 76
    Width = 39
    Height = 13
    Caption = 'Tanggal'
  end
  object txtNama: TAdvEdit
    Left = 56
    Top = 8
    Width = 121
    Height = 21
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 0
    Text = 'txtNama'
    Visible = True
    Version = '2.9.2.1'
  end
  object txtNilai: TAdvEdit
    Left = 56
    Top = 40
    Width = 75
    Height = 21
    EditAlign = eaRight
    EditType = etMoney
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 1
    Text = '0'
    Visible = True
    Version = '2.9.2.1'
  end
  object dtpTgl: TDateTimePicker
    Left = 56
    Top = 72
    Width = 90
    Height = 21
    Date = 40585.447703263900000000
    Time = 40585.447703263900000000
    TabOrder = 2
    OnKeyPress = dtpTglKeyPress
  end
  object btnOk: TButton
    Left = 96
    Top = 104
    Width = 75
    Height = 25
    Caption = '&Ok'
    TabOrder = 3
    OnClick = btnOkClick
  end
end
