unit InputDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit;

type
  TR_Data = Record
    nama : string;
    nilai : integer;
    tgl : TDate;
  end;
  AR_Data = array of TR_Data;

  TfrmIsiData = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    txtNama: TAdvEdit;
    txtNilai: TAdvEdit;
    dtpTgl: TDateTimePicker;
    btnOk: TButton;
    procedure btnOkClick(Sender: TObject);
    procedure dtpTglKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    IdxBtn : integer;
    tampung : TR_Data;
    Arr1, Arr2, ArrGabung : AR_Data;
    procedure Execute(i : integer);
  end;

var
  frmIsiData: TfrmIsiData;

implementation

{$R *.dfm}

{ TfrmIsiData }

procedure TfrmIsiData.Execute(i: integer);
begin
  InitForm;
  ShowModal;
  if IdxBtn = 1 then begin //isi array 1
    Arr1[i].nama := txtNama.Text;
    Arr1[i].nilai := txtNilai.IntValue;
    Arr1[i].tgl := dtpTgl.Date;
  end else if IdxBtn = 2 then begin //isi array 2
    Arr2[i].nama := txtNama.Text;
    Arr2[i].nilai := txtNilai.IntValue;
    Arr2[i].tgl := dtpTgl.Date;
  end;
end;

procedure TfrmIsiData.InitForm;
begin
  txtNama.Text := '';
  txtNilai.IntValue := 0;
  dtpTgl.Date := Now;
  ActiveControl := txtNama;
end;

procedure TfrmIsiData.btnOkClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmIsiData.dtpTglKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnOk.SetFocus;
end;

end.
