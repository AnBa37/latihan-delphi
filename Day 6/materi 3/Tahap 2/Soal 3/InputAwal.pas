unit InputAwal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    txt1: TAdvEdit;
    txt2: TAdvEdit;
    btnIsi1: TButton;
    btnIsi2: TButton;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnIsi1Click(Sender: TObject);
    procedure btnIsi2Click(Sender: TObject);
    procedure txt1Exit(Sender: TObject);
    procedure txt2Exit(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

uses InputDetail, TampilData;

{$R *.dfm}

{ TfrmInput }

procedure TfrmInput.InitForm;
begin
  txt1.IntValue := 0;
  txt2.IntValue := 0;
  setlength(frmIsiData.Arr1, 0);
  setlength(frmIsiData.Arr2, 0);
  setlength(frmIsiData.ArrGabung, 0);
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

procedure TfrmInput.btnIsi1Click(Sender: TObject);
var i : integer;
begin
  if (txt1.IntValue >= 2) and (txt1.IntValue <= 5) then begin
    frmIsiData.IdxBtn := 1;
    setlength(frmIsiData.Arr1, txt1.IntValue);
    for i := 0 to txt1.IntValue-1 do begin
      frmIsiData.Execute(i);
    end;
  end else ShowMessage('Input antara 2 - 5.');
end;

procedure TfrmInput.btnIsi2Click(Sender: TObject);
var i : integer;
begin
  if (txt2.IntValue >= 2) and (txt2.IntValue <= 5) then begin
    frmIsiData.IdxBtn := 2;
    setlength(frmIsiData.Arr2, txt2.IntValue);
    for i := 0 to txt2.IntValue-1 do begin
      frmIsiData.Execute(i);
    end;
  end else ShowMessage('Input antara 2 - 5.');
end;

procedure TfrmInput.txt1Exit(Sender: TObject);
begin
  setlength(frmIsiData.Arr1,0);
end;

procedure TfrmInput.txt2Exit(Sender: TObject);
begin
  setlength(frmIsiData.Arr2, 0);
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInput.btnTampilClick(Sender: TObject);
var i, idx : integer;
begin
  //gabungkan semua array yang telah diinput
  setlength(frmIsiData.ArrGabung, length(frmIsiData.Arr1) + length(frmIsiData.Arr2));
  for i := 0 to length(frmIsiData.Arr1)-1 do begin
    frmIsiData.ArrGabung[i].nama := frmIsiData.Arr1[i].nama;
    frmIsiData.ArrGabung[i].nilai := frmIsiData.Arr1[i].nilai;
    frmIsiData.ArrGabung[i].tgl := frmIsiData.Arr1[i].tgl;
  end;
  idx := length(frmIsiData.Arr1);
  for i := 0 to length(frmIsiData.Arr2)-1 do begin
    frmIsiData.ArrGabung[idx + i].nama := frmIsiData.Arr2[i].nama;
    frmIsiData.ArrGabung[idx + i].nilai := frmIsiData.Arr2[i].nilai;
    frmIsiData.ArrGabung[idx + i].tgl := frmIsiData.Arr2[i].tgl;
  end;

  frmTampil.Execute;
end;

end.
