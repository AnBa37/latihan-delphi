object frmTampil: TfrmTampil
  Left = 419
  Top = 399
  BorderStyle = bsDialog
  Caption = 'Tampil'
  ClientHeight = 72
  ClientWidth = 247
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 12
    Width = 58
    Height = 13
    Caption = 'Pilih urutan :'
  end
  object cmbPilihan: TComboBox
    Left = 80
    Top = 8
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'Menaik'
      'Menurun')
  end
  object btnProses: TButton
    Left = 8
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Proses'
    TabOrder = 1
    OnClick = btnProsesClick
  end
  object btnTampil: TButton
    Left = 88
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Tampilkan'
    TabOrder = 2
    OnClick = btnTampilClick
  end
  object btnTutup: TButton
    Left = 168
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 3
    OnClick = btnTutupClick
  end
end
