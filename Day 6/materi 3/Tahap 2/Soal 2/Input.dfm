object frmInput: TfrmInput
  Left = 406
  Top = 289
  BorderStyle = bsDialog
  Caption = 'Input Data'
  ClientHeight = 145
  ClientWidth = 168
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object txtInput: TAdvEdit
    Left = 32
    Top = 16
    Width = 121
    Height = 21
    EditAlign = eaRight
    EditType = etMoney
    FocusColor = clWindow
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 0
    Text = '0'
    Visible = True
    OnExit = txtInputExit
    Version = '2.9.2.1'
  end
  object btnIsiData: TButton
    Left = 8
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Isi Data'
    TabOrder = 1
    OnClick = btnIsiDataClick
  end
  object btnTampil: TButton
    Left = 88
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Tampil'
    TabOrder = 2
    OnClick = btnTampilClick
  end
  object btnMin: TButton
    Left = 8
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Min'
    TabOrder = 3
    OnClick = btnMinClick
  end
  object btnMax: TButton
    Left = 88
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Max'
    TabOrder = 4
    OnClick = btnMaxClick
  end
  object btnTutup: TButton
    Left = 88
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 5
    OnClick = btnTutupClick
  end
end
