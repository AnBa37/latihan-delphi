program belajar_grid;

uses
  Forms,
  PostgresConnection in 'PostgresConnection.pas' {myConnection: TDataModule},
  UEngine in 'UEngine.pas',
  URecord in 'URecord.pas',
  Utama in 'Utama.pas' {frmUtama};

{$R *.res}

begin
  Application.Initialize;
  // Application.CreateForm(TmyConnection, myConnection); //tutup dulu krn blm ada koneksi dan databasenya
  Application.CreateForm(TfrmUtama, frmUtama);
  Application.Run;
end.
