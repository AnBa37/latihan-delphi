unit URecord;

interface

uses   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls;

type
  tr_data = record
    seq : integer;
    kode, nama : string[20];
    tglMasuk : tdate;
    gaji : real;
    jumAnak : integer;
    tunj_anak : string[1];
    JK : string[1];
  end;

  arData = array of tr_data;
  
implementation

end.
