unit inputData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, ExtCtrls, StdCtrls;

type
  tr_barang = record
    barang : string [20];
    stok : integer;
    harga, total : real;
    tgl_beli : tdate;
    keterangan : string[255];
  end;
  arBarang = array of tr_BARANG;

  TfrmInputData = class(TForm)
    mainPanel: TPanel;
    asgInput: TAdvStringGrid;
    btnSimpan: TButton;
    btnTampil: TButton;
    asgTampilData: TAdvStringGrid;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgInputCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure asgInputAutoAddRow(Sender: TObject; ARow: Integer);
    procedure asgInputGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure btnSimpanClick(Sender: TObject);
    procedure asgTampilDataGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgTampilDataGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure btnTampilClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    arData : arBarang;
    procedure setgrid;
    procedure arrangeColSize;
    procedure setgrid_new (idx : integer = 0);
    procedure arrangeColSize_new(idx : integer = 0);
  public
    { Public declarations }
    procedure execute;
  end;

var
  frmInputData: TfrmInputData;

implementation

{$R *.dfm}

const
  colNo = 0;
  colCek = 1;
  colBarang = 2;
  colStok = 3;
  colHarga = 4;
  ColTotal = 5;
  colTglBeli = 6;
  ColKeterangan = 7;

  colBarang2 = 1;
  colStok2 = 2;
  colHarga2 = 3;
  ColTotal2 = 4;
  colTglBeli2 = 5;
  ColKeterangan2 = 6;

procedure TfrmInputData.arrangeColSize;
begin
  asgInput.AutoNumberCol(0);
  asgInput.AutoSizeColumns(true);
end;

procedure TfrmInputData.arrangeColSize_new(idx: integer = 0);
begin
  if (idx = 0) or (idx = 1) then begin
    asgInput.AutoNumberCol(0);
    asgInput.AutoSizeColumns(true);
  end;

  if (idx = 0) or (idx = 2) then begin
    asgTampilData.AutoNumberCol(0);
    asgTampilData.cells[ColBarang2, asgTampilData.RowCount-1] := 'Total';
    asgTampilData.FloatingFooter.ColumnCalc[colStok2] := acsum;
    asgTampilData.FloatingFooter.ColumnCalc[ColTotal2] := acsum;
    asgTampilData.AutoSizeColumns(true);
  end;
end;

procedure TfrmInputData.asgInputAutoAddRow(Sender: TObject; ARow: Integer);
begin
  asgInput.AddCheckBox(colCek, arow, false, false);
end;

procedure TfrmInputData.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if acol in [colCek..colHarga, colTglBeli, ColKeterangan] then CanEdit := true
  else canedit := false;
end;

procedure TfrmInputData.asgInputCellValidate(Sender: TObject; ACol,
  ARow: Integer; var Value: string; var Valid: Boolean);
begin
  if acol = colStok then begin
    asgInput.Floats[ColTotal, arow] := asgInput.Ints[acol, arow] * asgInput.Floats[ColHarga, arow];
  end;

  if acol = colHarga then begin
    asgInput.Floats[ColTotal, arow] := asgInput.Ints[colStok, arow] * asgInput.Floats[acol, arow];
  end;

  arrangeColSize;
end;

procedure TfrmInputData.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (arow = 0) or (acol = colCek) then HAlign := tacenter
  else if acol in [colNo, colStok, colHarga, colTotal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInputData.asgInputGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case acol of
    colCek : AEditor := edCheckBox;
    colStok : AEditor := edPositiveNumeric;
    colHarga : AEditor := edPositiveFloat;
    colBarang : AEditor := edUpperCase;
    colTglBeli : AEditor := edDateEdit;
  end;
end;

procedure TfrmInputData.asgInputGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if acol in [colNo, colStok, colHarga, ColTotal] then begin
    isfloat := true;
    if acol in [colNo, ColStok] then FloatFormat := '%.0n'
    else if acol in [colHarga, ColTotal] then FloatFormat := '%.2n';
  end else isfloat := false;
end;

procedure TfrmInputData.asgTampilDataGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (arow <= 1) then HAlign := tacenter
  else if acol in [colNo, colStok2, colHarga2, colTotal2] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInputData.asgTampilDataGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if acol in [colNo, colStok2, colHarga2, ColTotal2] then begin
    isfloat := true;
    if acol in [colNo, ColStok2] then FloatFormat := '%.0n'
    else if acol in [colHarga2, ColTotal2] then FloatFormat := '%.2n';
  end else isfloat := false;
end;

procedure TfrmInputData.btnSimpanClick(Sender: TObject);
VAR i, idx : integer; test : string;
begin
  idx := 0;
  for i := 1 to asgInput.RowCount-1 do begin
    setlength(arData, idx+1);
    arData[idx].barang := trim(asgInput.cells[colbarang, i]);
    arData[idx].stok := asgInput.ints[colStok, i];
    arData[idx].harga := asgInput.Floats[colHarga, i];
    arData[idx].total := asgInput.floats[ColTotal, i];
    arData[idx].tgl_beli := asgInput.dates[colTglBeli, i];
    arData[idx].keterangan := trim(asgInput.cells[colketerangan, i]);

    test := test + arData[idx].barang + ' - '+inttostr(arData[idx].stok)+ #13;
    idx := idx + 1;
  end;

  showmessage(test);
  arrangeColSize_new(1);
end;

procedure TfrmInputData.btnTampilClick(Sender: TObject);
var i, row : integer;
begin
  setgrid_new(2);
  for i := 0 to length(arData)-1 do begin
    if i > 0 then asgTampilData.AddRow;
    row := asgTampilData.RowCount-2;
    asgTampilData.cells[colbarang2, row] := arData[i].barang;
    asgTampilData.ints[colStok2, row] := arData[i].stok;
    asgTampilData.Floats[colHarga2, row] := arData[i].harga;
    asgTampilData.floats[ColTotal2, row] := arData[i].total;
    asgTampilData.dates[colTglBeli2, row] := arData[i].tgl_beli;
    asgTampilData.cells[colketerangan2, row] := arData[i].keterangan;   
  end;
  arrangeColSize_new(2);
end;

procedure TfrmInputData.Button1Click(Sender: TObject);
VAR i, idx : integer; test : string; status : boolean;
begin
  idx := 0;
  for i := 1 to asgInput.RowCount-1 do begin
    asgInput.GetCheckBoxState(colCek, i, status);
    if status = true then begin
      setlength(arData, idx+1);
      arData[idx].barang := trim(asgInput.cells[colbarang, i]);
      arData[idx].stok := asgInput.ints[colStok, i];
      arData[idx].harga := asgInput.Floats[colHarga, i];
      arData[idx].total := asgInput.floats[ColTotal, i];
      arData[idx].tgl_beli := asgInput.dates[colTglBeli, i];
      arData[idx].keterangan := trim(asgInput.cells[colketerangan, i]);

      test := test + arData[idx].barang + #13;
      idx := idx + 1;
    end;
  end;

  arrangeColSize_new(1); 
end;

procedure TfrmInputData.execute;
begin
//  setgrid;
  setgrid_new(0);
end;

procedure TfrmInputData.FormShow(Sender: TObject);
begin
  execute;
end;

procedure TfrmInputData.setgrid;
begin
  asgInput.ClearNormalCells;
  asgInput.RowCount := 2;
  asgInput.ColCount := 9;
  asgInput.FixedRows := 1;
  asgInput.FixedCols := 1;

  asgINput.cells[colNo, 0] := 'No.';
  asgINput.cells[colCek, 0] := '[V]';
  asgINput.cells[colBarang, 0] := 'Barang';
  asgINput.cells[colStok, 0] := 'Stok';
  asgINput.cells[colHarga, 0] := 'Harga';
  asgINput.cells[colTotal, 0] := 'Total';
  asgINput.cells[colTglBeli, 0] := 'Tgl Beli';
  asgINput.cells[ColKeterangan, 0] := 'Keterangan';

  asgInput.AddCheckBox(colCek, 1, false, false);
  arrangeColSize;
end;

procedure TfrmInputData.setgrid_new(idx: integer = 0);
begin
  if (idx = 0) or (idx = 1) then begin
    asgInput.ClearNormalCells;
    asgInput.RowCount := 2;
    asgInput.ColCount := 9;
    asgInput.FixedRows := 1;
    asgInput.FixedCols := 1;

    asgINput.cells[colNo, 0] := 'No.';
    asgINput.cells[colCek, 0] := '[V]';
    asgINput.cells[colBarang, 0] := 'Barang';
    asgINput.cells[colStok, 0] := 'Stok';
    asgINput.cells[colHarga, 0] := 'Harga';
    asgINput.cells[colTotal, 0] := 'Total';
    asgINput.cells[colTglBeli, 0] := 'Tgl Beli';
    asgINput.cells[ColKeterangan, 0] := 'Keterangan';
    asgInput.AddCheckBox(colCek, 1, false, false);
  end;                                            

  if (idx = 0) or (idx = 2) then begin
    asgTampilData.ClearNormalCells;
    asgTampilData.RowCount := 4;
    asgTampilData.ColCount := 8;
    asgTampilData.FixedRows := 2;
    asgTampilData.FixedCols := 1;

    asgTampilData.MergeCells(colNo, 0, 1, 2);
    asgTampilData.cells[colNo, 0] := 'No.';

    asgTampilData.mergeCells(colBarang2, 0, 1, 2);
    asgTampilData.cells[colBarang2, 0] := 'Barang';

    asgTampilData.mergeCells(colStok2, 0, 4, 1);
    asgTampilData.cells[colStok2, 0] := 'Detail';
    asgTampilData.cells[colStok2, 1] := 'Stok';
    asgTampilData.cells[colHarga2, 1] := 'Harga';
    asgTampilData.cells[colTotal2, 1] := 'Total';
    asgTampilData.cells[colTglBeli2, 1] := 'Tgl Beli';

    asgTampilData.mergeCells(colKeterangan2, 0, 1, 2);
    asgTampilData.cells[ColKeterangan2, 0] := 'Keterangan';
  end;

  arrangeColSize_new(idx);
end;

end.
