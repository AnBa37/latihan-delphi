object frmInputData: TfrmInputData
  Left = 771
  Top = 188
  BorderStyle = bsDialog
  Caption = 'Input Data'
  ClientHeight = 179
  ClientWidth = 228
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblId: TLabel
    Left = 8
    Top = 16
    Width = 11
    Height = 13
    Caption = 'ID'
  end
  object lblNama: TLabel
    Left = 8
    Top = 48
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object lblDept: TLabel
    Left = 8
    Top = 84
    Width = 26
    Height = 13
    Caption = 'Dept.'
  end
  object lblGaji: TLabel
    Left = 8
    Top = 120
    Width = 18
    Height = 13
    Caption = 'Gaji'
  end
  object txtNama: TAdvEdit
    Left = 40
    Top = 44
    Width = 145
    Height = 21
    AutoFocus = False
    EditAlign = eaLeft
    EditType = etMixedCase
    ErrorColor = clRed
    ErrorFontColor = clWhite
    ExcelStyleDecimalSeparator = False
    Flat = False
    FlatLineColor = clBlack
    FlatParentColor = True
    FocusAlign = eaDefault
    FocusBorder = False
    FocusColor = clWindow
    FocusFontColor = clWindowText
    FocusLabel = False
    FocusWidthInc = 0
    ModifiedColor = clHighlight
    DisabledColor = clSilver
    URLColor = clBlue
    ReturnIsTab = True
    LengthLimit = 0
    TabOnFullLength = False
    Precision = 0
    LabelPosition = lpLeftTop
    LabelMargin = 4
    LabelTransparent = False
    LabelAlwaysEnabled = False
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Persistence.Enable = False
    Persistence.Location = plInifile
    Color = clWindow
    Enabled = True
    HintShowLargeText = False
    OleDropTarget = False
    OleDropSource = False
    Signed = False
    TabOrder = 0
    Transparent = False
    Visible = True
  end
  object cmbDept: TComboBox
    Left = 40
    Top = 80
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnKeyPress = cmbDeptKeyPress
    Items.Strings = (
      'Pembelian'
      'Marketing'
      'Finance')
  end
  object txtGaji: TAdvEdit
    Left = 40
    Top = 112
    Width = 145
    Height = 21
    AutoFocus = False
    EditAlign = eaLeft
    EditType = etString
    ErrorColor = clRed
    ErrorFontColor = clWhite
    ExcelStyleDecimalSeparator = False
    Flat = False
    FlatLineColor = clBlack
    FlatParentColor = True
    FocusAlign = eaDefault
    FocusBorder = False
    FocusColor = clWindow
    FocusFontColor = clWindowText
    FocusLabel = False
    FocusWidthInc = 0
    ModifiedColor = clHighlight
    DisabledColor = clSilver
    URLColor = clBlue
    ReturnIsTab = False
    LengthLimit = 0
    TabOnFullLength = False
    Precision = 0
    LabelPosition = lpLeftTop
    LabelMargin = 4
    LabelTransparent = False
    LabelAlwaysEnabled = False
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Persistence.Enable = False
    Persistence.Location = plInifile
    Color = clWindow
    Enabled = True
    HintShowLargeText = False
    OleDropTarget = False
    OleDropSource = False
    Signed = False
    TabOrder = 2
    Transparent = False
    Visible = True
  end
  object sttId: TStaticText
    Left = 40
    Top = 16
    Width = 24
    Height = 17
    Caption = 'sttId'
    TabOrder = 3
  end
  object btnOk: TButton
    Left = 112
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Ok'
    TabOrder = 4
    OnClick = btnOkClick
  end
end
