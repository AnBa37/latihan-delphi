unit InputData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  tr_data = record
  id : integer;
  nama : String;
  dept : String;
  gaji : real;
  end;
  ar_data = array of tr_data;

  TfrmInputData = class(TForm)
    lblId: TLabel;
    lblNama: TLabel;
    lblDept: TLabel;
    lblGaji: TLabel;
    txtNama: TAdvEdit;
    cmbDept: TComboBox;
    txtGaji: TAdvEdit;
    sttId: TStaticText;
    btnOk: TButton;
    procedure cmbDeptKeyPress(Sender: TObject; var Key: Char);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
    idx : integer;
    EditMode, DetailMode : Boolean;
    procedure InitForm;
    procedure LoadData;
  public
    { Public declarations }
    ArrData : ar_data;
    procedure Execute(Id : integer; isEdit, isDetail : Boolean);
  end;

var
  frmInputData: TfrmInputData;

implementation

{$R *.dfm}

{ TfrmInputData }

procedure TfrmInputData.Execute(Id: integer; isEdit, isDetail: Boolean);
begin
 idx := id; EditMode := isEdit; DetailMode := isDetail;
  InitForm; sttId.Caption := IntToStr(Id);
  if EditMode or DetailMode then LoadData;
  if EditMode then frmInputData.Caption := 'Edit Data'
  else if DetailMode then frmInputData.Caption := 'Detail Data'
  else frmInputData.Caption := 'Input Data';
  frmInputData.Position := poDesktopCenter;
  ShowModal;

end;

procedure TfrmInputData.InitForm;
begin

end;

procedure TfrmInputData.cmbDeptKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then txtGaji.SetFocus
end;

procedure TfrmInputData.InitForm;
begin
  sttId.Caption := '';
  txtNama.Text := '';
  cmbDept.ItemIndex := -1;
  txtGaji.FloatValue := 0;

  txtNama.ReadOnly := DetailMode;
  cmbDept.Enabled := not DetailMode;
  txtGaji.ReadOnly := DetailMode;
end;

procedure TfrmInputData.btnOkClick(Sender: TObject);
begin
 if DetailMode and not EditMode then Close
  else begin //isi ke array data
    if Trim(txtNama.Text) = '' then begin
      ShowMessage('Nama tidak boleh kosong.');
      txtNama.SetFocus;
      exit; //supaya tidak keluar dari form input dan tidak masuk ke array
    end else if cmbDept.ItemIndex = -1 then begin
      ShowMessage('Dept belum dipilih.');
      cmbDept.SetFocus;
      exit;
    end else if txtGaji.FloatValue = 0 then begin
      ShowMessage('Gaji tidak boleh 0.');
      txtGaji.SetFocus;
      exit;
    end;

    ArrData[idx].id := idx;
    ArrData[idx].nama := Trim(txtNama.Text);
    ArrData[idx].dept := cmbDept.Text;
    ArrData[idx].gaji := txtGaji.FloatValue;
    close;
  end;
end;

procedure TfrmInputData.LoadData;
begin
  txtNama.Text := ArrData[idx].nama;
  txtGaji.FloatValue := ArrData[idx].gaji;
  cmbDept.Text := ArrData[idx].dept;
  if ArrData[idx].dept = 'Pembelian' then cmbDept.ItemIndex := 0
  else if ArrData[idx].dept = 'Marketing' then cmbDept.ItemIndex := 1
  else if ArrData[idx].dept = 'Finance' then cmbDept.ItemIndex := 2;
end;

end.
