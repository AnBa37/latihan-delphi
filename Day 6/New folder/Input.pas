unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmInput = class(TForm)
    MainPanel: TPanel;
    txtInput: TAdvEdit;
    btnIsi: TButton;
    btnReport: TButton;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnIsiClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

uses InputData, Tampil;

{$R *.dfm}

procedure TfrmInput.btnIsiClick(Sender: TObject);
var i : integer;
begin
 if (txtinput.IntValue >= 1) and (txtInput.IntValue <= 20) then begin
 SetLength(frmInputData.Arr, txtInput.IntValue);
 for i = 0 to txtInput.IntValue-1 do begin
 frmInputData.Execute(i, False, False);
 end;
 end else ShowMessage('Input Nilai 1-20.');
end;

procedure TfrmInput.InitForm;
begin
 txtInput.IntValue := 0;
  setlength(frmInputDetail.ArrData, 0);
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
 close;
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
 InitForm;
end;

procedure TfrmInput.txtInputExit(Sender: TObject);
begin
  setlength(frmInputDetail.ArrData, 0);
end;

procedure TfrmInput.btnTampilClick(Sender: TObject);
begin
  frmTampil.Execute;
end;

procedure TfrmInput.btnReportClick(Sender: TObject);
begin
  frmReport.Execute;
end;

end.
