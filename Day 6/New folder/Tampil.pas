unit Tampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls;

type
  TfrmTampil = class(TForm)
    asgTampil: TAdvStringGrid;
    btnEdit: TButton;
    btnTutup: TButton;
    procedure asgTampilGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnTutupClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure asgTampilDblClickCell(Sender: TObject; ARow, ACol: Integer);
  private
    { Private declarations }
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTampil: TfrmTampil;

implementation
uses InputData;


{$R *.dfm}
Const
  ColId = 0;
  ColNama = 1;
  ColDept = 2;
  ColGaji = 3;

procedure TfrmTampil.ArrangeColSize;
begin
  asgTampil.AutoSizeColumns(True, 2);
end;

procedure TfrmTampil.asgTampilGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if ACol in [ColId, ColGaji] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmTampil.Execute;
begin
  SetGrid;
  LoadData;
  ShowModal;
end;

procedure TfrmTampil.LoadData;
var i, vRow : integer;
begin
  //inisialisasi ulang si grid --> supaya datanya gak nambah terusss
  asgTampil.ClearNormalCells;
  asgTampil.RowCount := 2; //berarti fixed row = 1 dan 1 baris kosong, betul????
  //isikan data grid dari array
  for i := 0 to length(frmInputData.ArrData)-1 do begin
    //saat i = 0 sudah ada 1 baris kosong, jika i > 0 maka sistem menambahkan 1 baris kosong untuk diisi
    if i > 0 then asgTampil.AddRow; //karena grid di-set ulang dgn default rowcount = 2
    vRow := asgTampil.RowCount-1; //karena tidak da floating footer
    asgTampil.Ints[ColId, vRow] := frmInputData.ArrData[i].id;
    asgTampil.Cells[ColNama, vRow] := frmInputData.ArrData[i].nama;
    asgTampil.Cells[ColDept, vRow] := frmInputData.ArrData[i].dept;
    asgTampil.Floats[ColGaji, vRow] := frmInputData.ArrData[i].gaji;
  end;
  ArrangeColSize;
end;

procedure TfrmTampil.SetGrid;
begin
  asgTampil.ClearNormalCells;
  asgTampil.RowCount := 2;
  asgTampil.ColCount := 5;
  asgTampil.FixedCols := 0;
  asgTampil.FixedRows := 1;
  ArrangeColSize;  
end;

procedure TfrmTampil.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmTampil.btnEditClick(Sender: TObject);
begin
  frmInputDetail.Execute(asgTampil.Ints[ColId, asgTampil.Row], True, False);
  LoadData;
end;

procedure TfrmTampil.asgTampilDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if ARow > 0 then frmInputDetail.Execute(asgTampil.Ints[ColId, ARow], False, True);
end;
end.
