program Inputan;

uses
  Forms,
  Input in 'Input.pas' {frmInput},
  InputData in 'InputData.pas' {frmInputData},
  Tampil in 'Tampil.pas' {frmTampil},
  Report in 'Report.pas' {frmReport};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TfrmInputData, frmInputData);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.CreateForm(TfrmReport, frmReport);
  Application.Run;
end.
