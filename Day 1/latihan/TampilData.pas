unit TampilData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmTampilData = class(TForm)
    mmKeterangan: TMemo;
    btnKembali: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnKembaliClick(Sender: TObject);
  private
    { Private declarations }
    procedure initform;
  public
    { Public declarations }
  end;

var
  frmTampilData: TfrmTampilData;

implementation

uses menuUtama;

{$R *.dfm}

{ TfrmTampilData }

procedure TfrmTampilData.btnKembaliClick(Sender: TObject);
begin
  close;
  frmMenuUtama.Show;
end;

procedure TfrmTampilData.FormShow(Sender: TObject);
begin
  initform;
end;

procedure TfrmTampilData.initform;
begin
//  self.Position := poDesktopCenter;
  mmKeterangan.lines.Text := '';
  mmKeterangan.SetFocus;  
end;

end.
