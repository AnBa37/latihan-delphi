object frmMenuUtama: TfrmMenuUtama
  Left = 559
  Top = 264
  BorderStyle = bsDialog
  Caption = 'Menu Utama'
  ClientHeight = 329
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 283
    Height = 329
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 28
      Width = 33
      Height = 13
      Caption = 'Nama*'
    end
    object Label2: TLabel
      Left = 16
      Top = 55
      Width = 47
      Height = 13
      Caption = 'Tgl Masuk'
    end
    object Label3: TLabel
      Left = 16
      Top = 82
      Width = 33
      Height = 13
      Caption = 'Agama'
    end
    object Label4: TLabel
      Left = 16
      Top = 108
      Width = 51
      Height = 13
      Caption = 'Pendidikan'
    end
    object Label5: TLabel
      Left = 16
      Top = 182
      Width = 18
      Height = 13
      Caption = 'Gaji'
    end
    object Label6: TLabel
      Left = 16
      Top = 205
      Width = 56
      Height = 13
      Caption = 'Keterangan'
    end
    object txtNama: TAdvEdit
      Left = 78
      Top = 24
      Width = 121
      Height = 21
      AutoFocus = False
      EditAlign = eaLeft
      EditType = etMixedCase
      ErrorColor = clRed
      ErrorFontColor = clWhite
      ExcelStyleDecimalSeparator = False
      Flat = False
      FlatLineColor = clBlack
      FlatParentColor = True
      FocusAlign = eaDefault
      FocusBorder = False
      FocusColor = clWindow
      FocusFontColor = clWindowText
      FocusLabel = False
      FocusWidthInc = 0
      ModifiedColor = clHighlight
      DisabledColor = clSilver
      URLColor = clBlue
      ReturnIsTab = True
      LengthLimit = 0
      TabOnFullLength = False
      Precision = 0
      LabelPosition = lpLeftTop
      LabelMargin = 4
      LabelTransparent = False
      LabelAlwaysEnabled = False
      LabelFont.Charset = DEFAULT_CHARSET
      LabelFont.Color = clWindowText
      LabelFont.Height = -11
      LabelFont.Name = 'Tahoma'
      LabelFont.Style = []
      Persistence.Enable = False
      Persistence.Location = plInifile
      Color = clWindow
      Enabled = True
      HintShowLargeText = False
      MaxLength = 50
      OleDropTarget = False
      OleDropSource = False
      Signed = False
      TabOrder = 0
      Transparent = False
      Visible = True
    end
    object dtpTglMasuk: TDateTimePicker
      Left = 78
      Top = 51
      Width = 94
      Height = 21
      CalAlignment = dtaLeft
      Date = 44816.4017554051
      Time = 44816.4017554051
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 1
      OnKeyPress = dtpTglMasukKeyPress
    end
    object cmbAgama: TComboBox
      Left = 78
      Top = 78
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnKeyPress = cmbAgamaKeyPress
      Items.Strings = (
        'Islam'
        'Kristen'
        'Hindu'
        'Budha'
        'Lain-lain')
    end
    object rbtSarjana: TRadioButton
      Left = 78
      Top = 109
      Width = 46
      Height = 17
      Caption = 'S1'
      TabOrder = 3
    end
    object rbtDiploma: TRadioButton
      Left = 78
      Top = 132
      Width = 46
      Height = 17
      Caption = 'D3'
      TabOrder = 4
    end
    object rbtSMA: TRadioButton
      Left = 78
      Top = 155
      Width = 70
      Height = 17
      Caption = 'SMA/SMK'
      TabOrder = 5
    end
    object txtGaji: TAdvEdit
      Left = 78
      Top = 178
      Width = 121
      Height = 21
      AutoFocus = False
      EditAlign = eaRight
      EditType = etMoney
      ErrorColor = clRed
      ErrorFontColor = clWhite
      ExcelStyleDecimalSeparator = False
      Flat = False
      FlatLineColor = clBlack
      FlatParentColor = True
      FocusAlign = eaDefault
      FocusBorder = False
      FocusColor = clWindow
      FocusFontColor = clWindowText
      FocusLabel = False
      FocusWidthInc = 0
      ModifiedColor = clHighlight
      DisabledColor = clSilver
      URLColor = clBlue
      ReturnIsTab = True
      LengthLimit = 0
      TabOnFullLength = False
      Precision = 2
      LabelPosition = lpLeftTop
      LabelMargin = 4
      LabelTransparent = False
      LabelAlwaysEnabled = False
      LabelFont.Charset = DEFAULT_CHARSET
      LabelFont.Color = clWindowText
      LabelFont.Height = -11
      LabelFont.Name = 'Tahoma'
      LabelFont.Style = []
      Persistence.Enable = False
      Persistence.Location = plInifile
      Color = clWindow
      Enabled = True
      HintShowLargeText = False
      OleDropTarget = False
      OleDropSource = False
      Signed = False
      TabOrder = 6
      Text = '0,00'
      Transparent = False
      Visible = True
    end
    object mmKeterangan: TMemo
      Left = 78
      Top = 205
      Width = 185
      Height = 68
      Lines.Strings = (
        'mmKeterangan')
      MaxLength = 250
      ScrollBars = ssBoth
      TabOrder = 7
    end
    object btnSimpan: TButton
      Left = 107
      Top = 296
      Width = 75
      Height = 25
      Caption = '&Simpan'
      TabOrder = 8
      OnClick = btnSimpanClick
    end
    object btnBatal: TButton
      Left = 188
      Top = 296
      Width = 75
      Height = 25
      Caption = '&Batal'
      TabOrder = 9
      OnClick = btnBatalClick
    end
  end
end
