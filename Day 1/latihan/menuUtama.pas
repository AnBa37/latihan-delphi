unit menuUtama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, ComCtrls;

type
  TfrmMenuUtama = class(TForm)
    MainPanel: TPanel;
    Label1: TLabel;
    txtNama: TAdvEdit;
    dtpTglMasuk: TDateTimePicker;
    Label2: TLabel;
    cmbAgama: TComboBox;
    Label3: TLabel;
    rbtSarjana: TRadioButton;
    rbtDiploma: TRadioButton;
    rbtSMA: TRadioButton;
    Label4: TLabel;
    txtGaji: TAdvEdit;
    Label5: TLabel;
    mmKeterangan: TMemo;
    btnSimpan: TButton;
    Label6: TLabel;
    btnBatal: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnBatalClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure dtpTglMasukKeyPress(Sender: TObject; var Key: Char);
    procedure cmbAgamaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure initform;
  public
    { Public declarations }
  end;

var
  frmMenuUtama: TfrmMenuUtama;

implementation

uses tampilData;

{$R *.dfm}

{ TfrmMenuUtama }

procedure TfrmMenuUtama.btnBatalClick(Sender: TObject);
begin
  close;
end;

procedure TfrmMenuUtama.btnSimpanClick(Sender: TObject);
var pendidikan : string;
begin

  if trim(txtNama.Text) = '' then begin
    ShowMessage('Nama harus diisi.');
    txtNama.SetFocus;
    exit;
  end;

  if cmbAgama.ItemIndex = -1 then begin
    ShowMessage('Agama harus dipilih.');
    cmbAgama.SetFocus;
    exit;
  end;

  if (rbtSarjana.Checked = false) and (rbtDiploma.Checked = false) and (rbtSMA.Checked = false) then begin
    ShowMessage('Pendidikan harus dipilih.');
    rbtSarjana.SetFocus;
    exit;
  end;

  if txtGaji.FloatValue = 0 then begin
    ShowMessage('Gaji harus diisi.');
    txtGaji.SetFocus;
    exit;
  end;


  frmTampilData.Show;

  if rbtSarjana.Checked = true then pendidikan := 'Sarjana Informatika'
  else if rbtDiploma.Checked = true then pendidikan := 'Diploma Informatika'
  else if rbtSMA.Checked = true then pendidikan := 'SMA / SMK RPL';

  frmTampilData.mmKeterangan.Lines.Text := 'Nama : '+trim(txtNama.Text) +#13+'Tgl Masuk : '+DateToStr(dtpTglMasuk.date)+ #13+
                                           'Agama : '+cmbAgama.Text+#13 +'Pendidikan : '+ pendidikan+#13 + 'Gaji : '+FloatToStr(txtGaji.FloatValue)+#13 +
                                           'Keterangan : '+mmKeterangan.Lines.Text;
end;

procedure TfrmMenuUtama.cmbAgamaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    rbtSarjana.SetFocus;
end;

procedure TfrmMenuUtama.dtpTglMasukKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then cmbAgama.SetFocus;
end;

procedure TfrmMenuUtama.FormShow(Sender: TObject);
begin
  initform;
end;

procedure TfrmMenuUtama.initform;
begin
  txtNama.Text := '';
  txtGaji.FloatValue := 0;
  dtpTglMasuk.date := now;
  cmbAgama.ItemIndex := -1; cmbAgama.Text := '';
  rbtSarjana.Checked := false;
  rbtDiploma.Checked := false;
  rbtSMA.Checked := false;
  mmKeterangan.Lines.Text := '';
end;

end.
