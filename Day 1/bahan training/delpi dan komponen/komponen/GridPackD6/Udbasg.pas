 unit Udbasg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, AdvGrid, dbadvgrd, ComCtrls, DBTables, StdCtrls, BaseGrid,
  ExtCtrls, DBCtrls, Mask;

type
  TForm1 = class(TForm)
    DataSource1: TDataSource;
    Table1: TTable;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBAdvStringGrid1: TDBAdvStringGrid;
    DBAdvStringGrid2: TDBAdvStringGrid;
    Table1EmpNo: TIntegerField;
    Table1LastName: TStringField;
    Table1FirstName: TStringField;
    Table1PhoneExt: TStringField;
    Table1HireDate: TDateTimeField;
    Table1Salary: TFloatField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    TabSheet3: TTabSheet;
    Label6: TLabel;
    Label7: TLabel;
    DBAdvStringGrid3: TDBAdvStringGrid;
    TabSheet4: TTabSheet;
    DBAdvStringGrid4: TDBAdvStringGrid;
    Table2: TTable;
    DataSource2: TDataSource;
    TabSheet5: TTabSheet;
    DBAdvStringGrid5: TDBAdvStringGrid;
    Table3: TTable;
    DataSource3: TDataSource;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    DBNavigator1: TDBNavigator;
    Label10: TLabel;
    Label11: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

end.
