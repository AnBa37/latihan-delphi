{*************************************************************************}
{ TMATHLIBS components                                                    }
{ for Delphi 3.0,4.0,5.0,6.0 & C++Builder 3.0,4.0,5.0,6.0                 }
{ version 1.0, April 2002                                                 }
{                                                                         }
{ written by                                                              }
{    TMS Software                                                         }
{    copyright � 2002                                                     }
{    Email:info@tmssoftware.com                                           }
{    Web:http://www.tmssoftware.com                                       }
{                                                                         }
{*************************************************************************}

unit MiscMathLibReg;

interface

uses
  Classes, MiscMathLib;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('TMS Grids', [TMiscMathLib]);
  RegisterComponents('TMS Grids', [TFinanceMathLib]);
  RegisterComponents('TMS Grids', [TConversionMathLib]);
  RegisterComponents('TMS Grids', [TStringMathLib]);
end;

end.
