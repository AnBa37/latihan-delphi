{*******************************************************************
TADVSTRINGGRID EDITLINKS
version 1.0 - rel. November 2000

written by TMS Software
           copyright � 2000
           Email : info@tmssoftware.com
           Web : http://www.tmssoftware.com

The source code is given as is. The author is not responsible
for any possible damage done due to the use of this code.
The component can be freely used in any application. The complete
source code remains property of the author and may not be distributed,
published, given or sold in any form as such. No parts of the source
code can be included in any other component or application without
written authorization of the author.
********************************************************************}

unit EditLinks;

interface

uses
 windows,classes,controls,stdctrls,graphics,forms,sysutils,
 advgrid,advedit;

type
  TAdvEditEditLink = class(TEditLink)
  private
   fEdit:TAdvEdit;

   fAutoFocus:boolean;
   fAutoThousandSeparator:boolean;
   fCanUndo:boolean;
   //fColor:TColor;
   //fDisabledColor:TColor;
   fEditAlign:TEditAlign;
   fEditType:TAdvEditType;
   fEmptyText:string;
   //fEnabled:boolean;
   fErrorColor:TColor;
   fErrorFontColor:TColor;
   fExcelStyleDecimalSeparator:boolean;
   fFlat:boolean;
   fFlatLineColor:TColor;
   fFlatParentColor:boolean;
   //fFloatValue:double;
   fFocusAlign:TEditALign;
   fFocusBorder:boolean;
   fFocusColor:TColor;
   fFocusFontColor:TColor;
   //fFocusLabel:boolean;
   fFocusWidthInc:integer;
   fHintShowLargeText:boolean;
   //fIntValue:integer;
   //fIsError:boolean;
   //fLabelCaption:string;
   //fLabelFont:TFont;
   //fLabelMargin:integer;
   //fLabelPosition:TLabelPosition;
   //fLabelTransparent:boolean;
   fLengthLimit:integer;
   fModified:boolean;
   fModifiedColor:TColor;
   //fOleDropSource:boolean;
   //fOleDropTarget:boolean;
   fPersistence:TPersistence;
   fPrecision:integer;
   fPrefix:string;
   //fReturnIsTab:boolean;
   fShowError:boolean;
   fShowModified:boolean;
   fShowURL:boolean;
   fSigned:boolean;
   fSoftBorder:boolean;
   fSuffix:string;
   //fTabOnFullLength:boolean;
   //fText:string;
   fTransparent:boolean;
   fURLColor:TColor;
   //fVisible:boolean;
  protected
   procedure EditExit(Sender:TObject);
  public
   procedure CreateEditor(aParent:TWinControl); override;
   procedure DestroyEditor; override;
   function GetEditorValue:string; override;
   procedure SetEditorValue(s:string); override;
   function GetEditControl:TWinControl; override;
   procedure SetProperties; override;
  published
   property AutoFocus:boolean read fAutoFocus write fAutoFocus default false;
   property AutoThousandSeparator:boolean read fAutoThousandSeparator write fAutoThousandSeparator default true;
   property CanUndo:boolean read fCanUndo write fCanUndo default true;
   //fColor:TColor;
   //fDisabledColor:TColor;
   property EditAlign:TEditAlign read fEditAlign write fEditAlign default eaLeft;
   property EditType:TAdvEditType read fEditType write fEditType default etString;
   property EmptyText:string read fEmptyText write fEmptyText;
   //fEnabled:boolean;
   property ErrorColor:TColor read fErrorColor write fErrorColor default clRed;
   property ErrorFontColor:TColor read fErrorFontColor write fErrorFontColor default clWhite;
   property ExcelStyleDecimalSeparator:boolean read fExcelStyleDecimalSeparator write fExcelStyleDecimalSeparator default false;
   property Flat:boolean read fFlat write fFlat default false;
   property FlatLineColor:TColor read fFlatLineColor write fFlatLineColor default clBlack;
   property FlatParentColor:boolean read fFlatParentColor write fFlatParentColor default true;
   //fFloatValue:double;
   property FocusAlign:TEditALign read fFocusAlign write fFocusAlign default eaDefault;
   property FocusBorder:boolean read fFocusBorder write fFocusBorder default false;
   property FocusColor:TColor read fFocusColor write fFocusColor default clWindow;
   property FocusFontColor:TColor read fFocusFontColor write fFocusFontColor default clWindowText;
   //fFocusLabel:boolean;
   property FocusWidthInc:integer read fFocusWidthInc write fFocusWidthInc default 0;
   property HintShowLargeText:boolean read fHintShowLargeText write fHintShowLargeText default false;
   //fIntValue:integer;
   //property IsError:boolean read fIsError write fIsError;
   //fLabelCaption:string;
   //fLabelFont:TFont;
   //fLabelMargin:integer;
   //fLabelPosition:TLabelPosition;
   //fLabelTransparent:boolean;
   property LengthLimit:integer read fLengthLimit write fLengthLimit;
   property Modified:boolean read fModified write fModified;
   property ModifiedColor:TColor read fModifiedColor write fModifiedColor default clHighlight;
   //fOleDropSource:boolean;
   //fOleDropTarget:boolean;
   property Persistence:TPersistence read fPersistence write fPersistence;
   property Precision:integer read fPrecision write fPrecision;
   property Prefix:string read fPrefix write fPrefix;
   //fReturnIsTab:boolean;
   property ShowError:boolean read fShowError write fShowError;
   property ShowModified:boolean read fShowModified write fShowModified;
   property ShowURL:boolean read fShowURL write fShowURL;
   property Signed:boolean read fSigned write fSigned;
   property SoftBorder:boolean read fSoftBorder write fSoftBorder;
   property Suffix:string read fSuffix write fSuffix;
   //fTabOnFullLength:boolean;
   //fText:string;
   property Transparent:boolean read fTransparent write fTransparent;
   property URLColor:TColor read fURLColor write fURLColor default clBlue;
   //fVisible:boolean;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('TMS Edits', [TAdvEditEditLink]);
end;

{ TAdvEditEditLink }

procedure TAdvEditEditLink.CreateEditor(aParent:TWinControl);
begin
 fEdit:=TAdvEdit.Create(Grid);
 fEdit.Color:=clYellow;
 fEdit.ShowModified:=true;
 fEdit.ModifiedColor:=clRed;
 fEdit.BorderStyle := bsNone;
 fEdit.OnKeydown:= EditKeyDown;
 fEdit.OnExit := EditExit;
 fEdit.Width:=0;
 fEdit.Height:=0;
 fEdit.Parent:=aParent;
 WantKeyLeftRight:=true;
 WantKeyHomeEnd:=true;
end;

procedure TAdvEditEditLink.DestroyEditor;
begin
 if assigned(fEdit) then fEdit.Free;
 fEdit:=nil;
end;

procedure TAdvEditEditLink.SetProperties;
begin
 inherited;
 fEdit.AutoFocus := fAutoFocus;
 fEdit.AutoThousandSeparator := fAutoThousandSeparator;
 fEdit.CanUndo := fCanUndo;
 fEdit.Color := fFocusColor; //fColor:TColor;
 //fDisabledColor:TColor;
 fEdit.EditAlign := fEditAlign;
 fEdit.EditType := fEditType;
 fEdit.EmptyText := fEmptyText;
 //fEnabled:boolean;
 fEdit.ErrorColor := fErrorColor;
 fEdit.ErrorFontColor := fErrorFontColor;
 fEdit.ExcelStyleDecimalSeparator := fExcelStyleDecimalSeparator;
 fEdit.Flat := fFlat;
 fEdit.FlatLineColor := fFlatLineColor;
 fEdit.FlatParentColor := fFlatParentColor;
 //fFloatValue:double;
 fEdit.FocusAlign := fFocusAlign;
 fEdit.FocusBorder := fFocusBorder;
 fEdit.FocusColor := fFocusColor;
 fEdit.FocusFontColor := fFocusFontColor;
 //fFocusLabel:boolean;
 fEdit.FocusWidthInc := fFocusWidthInc;
 fEdit.HintShowLargeText := fHintShowLargeText;
 //fIntValue:integer;
 //fEdit.IsError := fIsError;
 //fLabelCaption:string;
 //fLabelFont:TFont;
 //fLabelMargin:integer;
 //fLabelPosition:TLabelPosition;
 //fLabelTransparent:boolean;
 fEdit.LengthLimit := fLengthLimit;
 fEdit.Modified := fModified;
 fEdit.ModifiedColor := fModifiedColor;
 //fOleDropSource:boolean;
 //fOleDropTarget:boolean;
 fEdit.Persistence := fPersistence;
 fEdit.Precision := fPrecision;
 fEdit.Prefix := fPrefix;
 //fReturnIsTab:boolean;
 fEdit.ShowError := fShowError;
 fEdit.ShowModified := fShowModified;
 fEdit.ShowURL := fShowURL;
 fEdit.Signed := fSigned;
 fEdit.SoftBorder := fSoftBorder;
 fEdit.Suffix := fSuffix;
 //fTabOnFullLength:boolean;
 //fText:string;
 fEdit.Transparent := fTransparent;
 fEdit.URLColor := fURLColor;
 //fVisible:boolean;

 {if (fEditType in [etFloat]) then begin
  fEdit.ExcelStyleDecimalSeparator := true;
  fEdit.Precision := StrToIntDef(GetLocaleStr(GetThreadLocale, LOCALE_IDIGITS, '0'), 0);
 end;}
end;

procedure TAdvEditEditLink.SetEditorValue(s: string);
begin
 if (fEditType in [etMoney]) and (pos(CurrencyString,s)>0) then begin
   delete(s,pos(CurrencyString,s),length(CurrencyString));
   fEdit.Prefix := CurrencyString;
 end;
 fEdit.Text:=s;
end;

function TAdvEditEditLink.GetEditorValue:string;
begin
 if (fEditType in [etFloat,etMoney]) then
   result:=FloatToStr(fEdit.FloatValue)
 else if (fEditType in [etNumeric]) then
   result:=IntToStr(fEdit.IntValue)
 else
   result:=fEdit.Text;
end;

procedure TAdvEditEditLink.EditExit(sender: TObject);
begin
 HideEditor;
end;

function TAdvEditEditLink.GetEditControl: TWinControl;
begin
 result:=fEdit;
end;

end.
