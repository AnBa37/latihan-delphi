cara install componen

ADVEdit
1. pilih menu component
   - masukin file pas dengan cara klik tombol [...], cari di folder ..\Delphi Components\AdvEdD6 dan pilih file
     aereg.pas dan DBAEReg.pas lalu OK
   - masukin File DPK dengan cara klik tombol [...], cari di folder ..\Delphi Components\AdvEdD6 dan pilih file
     AdvEdits.dpk
2. setelah muncul dialog Project Manager, lalu klik tombol Compile dan jika tak ada eror pilih install
   jika berhasil maka akan muncul pesan bahwa component sudah ditambahkan

GridPackD6
1. dari menu file - open GridPackd6.dpk yang ada di folder ..\Delphi Components\GridPackD6
2. setelah muncul dialog Project Manager, lalu klik tombol Compile dan jika tak ada eror pilih install
   jika berhasil maka akan muncul pesan bahwa component sudah ditambahkan
3. dari menu Tools pilih environment option
   masukan library path GridPackD6 [..\Delphi Components\GridPackD6] pilih add lalu ok