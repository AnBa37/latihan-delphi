unit LaporanBeli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, AdvObj;

type
  TfrmLaporan = class(TForm)
    asgLaporan: TAdvStringGrid;
    procedure asgLaporanGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
  private
    { Private declarations }
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmLaporan: TfrmLaporan;

implementation

uses PostgresConnection, Uengine, Urecord;

{$R *.dfm}
Const
  ColNode = 0;
  ColTanggal = 1;
  ColNomor = 2;
  ColTotal = 3;
  ColKet = 4;

{ TfrmLaporan }

procedure TfrmLaporan.ArrangeColSize;
begin
  asgLaporan.AutoSizeColumns(True, 2);
end;

procedure TfrmLaporan.Execute;
begin
  SetGrid;
  LoadData;
  ShowModal;
end;

procedure TfrmLaporan.LoadData;
var i, R1, R2, j : integer;
    ArrKtgr : array of string;
    ArrMst : Arr_trans;
    Ktgr : string;
    TotKtgr, GTot : real;
begin
  setlength(ArrKtgr, 3);
  ArrKtgr[0] := 'Bahan Baku';
  ArrKtgr[1] := 'Brg 1/2 Jadi';
  ArrKtgr[2] := 'Barang Jadi';

{  ColNode = 0;
  ColTanggal = 1;
  ColNomor = 2;
  ColTotal = 3;
  ColKet = 4;}
  setlength(ArrMst, 0);
  asgLaporan.ClearNormalCells;
  asgLaporan.ClearRows(asgLaporan.RowCount-1, 1);
  asgLaporan.RowCount := 4; GTot := 0;
  //isi master bdskan kategori
  for i := 0 to length(ArrKtgr)-1 do begin
    if i > 0 then asgLaporan.AddRow;
    R1 := asgLaporan.RowCount-2;
    asgLaporan.MergeCells(ColTanggal, R1, 2, 1);
    asgLaporan.Cells[ColTanggal, R1] := ArrKtgr[i];

    //isi detail anak
    if i = 0 then Ktgr := 'B'
    else if i = 1 then Ktgr := 'H'
    else if i = 2 then Ktgr := 'J'
    else ktgr := ''; TotKtgr := 0;
    ArrMst := UEngine.load_Trans(0, 0, 0, Ktgr);
    for j := 0 to length(ArrMst)-1 do begin
      asgLaporan.AddRow;
      R2 := asgLaporan.RowCount-2;
      asgLaporan.Dates[COlTanggal, R2] := ArrMst[j].Tanggal;
      asgLaporan.Cells[ColNomor, R2] := ArrMst[j].Nomor;
      asgLaporan.Floats[ColTotal, R2] := ArrMst[j].Total; TotKtgr := TotKtgr + ArrMst[j].Total;
      asgLaporan.Cells[ColKet, R2] := ArrMst[j].Keterangan;
    end;
    asgLaporan.Floats[ColTotal, R1] := TotKtgr; GTot := GTot + TotKtgr;
    R2 := asgLaporan.RowCount-2;
    if R1 <> R2 then asgLaporan.AddNode(R1, R2-R1+1);
  end;
  asgLaporan.Floats[ColTotal, asgLaporan.RowCount-1] := GTot;

  ArrangeColSize;
end;

procedure TfrmLaporan.SetGrid;
begin
  asgLaporan.Clear;
  asgLaporan.ColCount := 6;
  asgLaporan.RowCount := 4;
  asgLaporan.FixedCols := 0;
  asgLaporan.FixedRows := 2;

  asgLaporan.MergeCells(ColNode, 0, 1, 2);
  asgLaporan.Cells[ColNode, 0] := '[+]';

  asgLaporan.MergeCells(ColTanggal, 0, 2, 1);
  asgLaporan.Cells[ColTanggal, 0] := 'Kategori';
  asgLaporan.Cells[ColTanggal, 1] := 'Tanggal';
  asgLaporan.Cells[ColNomor, 1] := 'Nomor';

  asgLaporan.MergeCells(ColTotal, 0, 1, 2);
  asgLaporan.Cells[ColTotal, 0] := 'Total';

  asgLaporan.MergeCells(ColKet, 0, 1, 2);
  asgLaporan.Cells[ColKet, 0] := 'Keterangan';

  asgLaporan.MergeCells(ColKet+1, 0, 1, 2);

  ArrangeColSize;
end;

procedure TfrmLaporan.asgLaporanGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 2 then HAlign := taCenter
  else if ACol = ColTotal then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

end.
