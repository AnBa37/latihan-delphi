unit pengolahanbarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, BaseGrid, AdvGrid, AdvObj;

type
  Tfrmpeng_brg = class(TForm)
    btnTambah: TButton;
    btnEdit: TButton;
    btnHapus: TButton;
    btnTutup: TButton;
    asgBarang: TAdvStringGrid;
    procedure asgBarangGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnTutupClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetGrid;
    procedure ArrangeColsize;
    procedure load_data;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmpeng_brg: Tfrmpeng_brg;

implementation

uses inputbarang, Uengine, Urecord, PostgresConnection;

{$R *.dfm}

{ Tfrmpeng_brg }
Const
  colno = 0;
  ColSeq = 1;
  ColKode = 2;
  ColNama = 3;
  ColH_Beli = 4;
  ColH_JUal = 5;
  Colket = 6;
  ColKat = 7;

procedure Tfrmpeng_brg.SetGrid;
begin
  asgBarang.ClearNormalCells;
  asgBarang.ColCount := 9;
  asgBarang.RowCount := 2;
  asgBarang.FixedCols := 1;
  asgBarang.FixedRows := 1;
  ArrangeColsize;
end;

procedure Tfrmpeng_brg.asgBarangGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
 if ARow = 0 then HAlign := taCenter
  else if ACol in [ColNo, ColH_beli, Colh_Jual] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure Tfrmpeng_brg.Execute;
begin
  SetGrid;
  load_data;
  frmpeng_brg.Height := 600;
  frmpeng_brg.Width := 800;
  frmpeng_brg.Position := poDesktopCenter;
  ShowModal;
end;

procedure Tfrmpeng_brg.ArrangeColsize;
begin
 asgBarang.AutoNumberCol(Colno);
 asgBarang.AutoSizeColumns(true,2);
 asgBarang.ColWidths[Colseq]:=0;
end;

procedure Tfrmpeng_brg.btnTutupClick(Sender: TObject);
begin
 close;
end;

procedure Tfrmpeng_brg.btnTambahClick(Sender: TObject);
begin
  if frmInput.Execute then load_data;
end;

procedure Tfrmpeng_brg.load_data;
var i, vRow : integer;
    Arr : Arr_brg;
begin
  asgBarang.ClearNormalCells;
  asgBarang.RowCount := 2;
  Arr := load_Brg;
  for i := 0 to length(Arr)-1 do begin
    if i > 0 then asgbarang.AddRow;
    vRow := asgBarang.RowCount-1;
    asgBarang.Ints[ColNIK, vRow] := Arr[i].seq;
    asgBarang.Cells[ColNama, vRow] := Arr[i].kode;
    asgBarang.Cells[ColAlamat, vRow] := Arr[i].Nama;
    asgBarang.Floats[ColTanggal, vRow] := Arr[i].h_beli;
    asgBarang.Floats[ColH_jual,vrow] := Arr[i].h_jual;
    asgBarang.Cells[Colket,vrow] := Arr[i].keterangan;
    if Arr[i].kategori = 'B' then
      asgBarang.Cells[Colkat,vrow] := 'Bahan Baku'
    else if Arr[i].kategori = 'H' then
      asgBarang.Cells[ColKat,vrow] := 'Bahan 1/2 Jadi'
    else if Arr[i].kategori = 'J' then
      asgBarang.Cells[Colkat,Vrow] := 'Bahan Jadi';
  end;
  ArrangeColSize;
end;

procedure Tfrmpeng_brg.btnEditClick(Sender: TObject);
begin
  if frmInput.Execute(asgBarang.Ints[ColSeq, asgBarang.Row]) then load_data;
end;

procedure Tfrmpeng_brg.btnHapusClick(Sender: TObject);
begin
  if (MessageDlg('Data hendak dihapus ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
    myConnection.BeginSQL;
    try
      UEngine.delete_Brg(asgBarang.Ints[ColSeq, asgBarang.Row]);
      myConnection.EndSQL;
      load_data;
    except
      myConnection.UndoSQL;
      ShowMessage('Hapus data GAGAL...');
    end;
  end;
end;

end.
