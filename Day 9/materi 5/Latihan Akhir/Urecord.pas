unit Urecord;

interface
uses Controls, ADOInt, Math, StrUtils, DateUtils;

type
  TR_Master_Barang = record
    seq : integer;
    kode : string[20];
    Nama : string[50];
    h_beli : real;
    h_jual : real;
    keterangan : string[250];
    kategori : string[1];
  end;

  TR_Transaksi_Beli_Master = record
    seq : integer;
    Tanggal : TDate;
    Nomor : String[20];
    Total : real;
    Keterangan : String[150];
  end;

  TR_Transaksi_Beli_Detail = record
    seq : integer;
    master_seq : integer;
    barang_seq : integer;
    qty : real;
    harga : real;
    total : real;
    keterangan : String[250];
  end;

Arr_brg = array of TR_Master_Barang;
Arr_trans = array of TR_Transaksi_Beli_Master;
Arr_Det = array of TR_Transaksi_Beli_Detail;

implementation

end.
