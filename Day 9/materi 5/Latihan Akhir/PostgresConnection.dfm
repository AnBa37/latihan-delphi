object myConnection: TmyConnection
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 457
  Top = 176
  Height = 188
  Width = 326
  object ADOConnection: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\ORION REKRUTMEN\' +
      'TRAINING\Pertemuan 4\Latihan Akhir\latihan_akhir.mdb;Persist Sec' +
      'urity Info=False'
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 32
    Top = 24
  end
  object ADOCommand: TADOCommand
    CommandType = cmdStoredProc
    Connection = ADOConnection
    Prepared = True
    Parameters = <>
    Left = 34
    Top = 88
  end
  object ADODataSet: TADODataSet
    Connection = ADOConnection
    Parameters = <>
    Left = 106
    Top = 24
  end
  object SaveToExcell: TSaveDialog
    DefaultExt = '*.xls'
    Filter = 'Excel Files|*.xls'
    Left = 256
    Top = 24
  end
end
