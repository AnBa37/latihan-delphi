unit inputtransaksi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, AdvEdit, ComCtrls, Uengine, Urecord, PostgresConnection,
  AdvObj;

type
  TfrmInputTrans = class(TForm)
    dtptanggal: TDateTimePicker;
    txtnobon: TAdvEdit;
    Label1: TLabel;
    Label2: TLabel;
    asgDetail: TAdvStringGrid;
    mmket: TMemo;
    Label3: TLabel;
    btnSimpan: TButton;
    btnBatal: TButton;
    btnHapusBaris: TButton;
    procedure asgDetailGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgDetailGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgDetailCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgDetailCellValidate(Sender: TObject; Col, Row: Integer;
      var Value: String; var Valid: Boolean);
    procedure btnSimpanClick(Sender: TObject);
    procedure dtptanggalKeyPress(Sender: TObject; var Key: Char);
    procedure btnHapusBarisClick(Sender: TObject);
    procedure asgDetailCellChanging(Sender: TObject; OldRow, OldCol,
      NewRow, NewCol: Integer; var Allow: Boolean);
    procedure asgDetailComboChange(Sender: TObject; ACol, ARow,
      AItemIndex: Integer; ASelection: String);
  private
    { Private declarations }
    EditMode, DetailMode : boolean;
    idx : integer;
    ArrBrgGlob : Arr_brg;
    procedure InitForm;
    procedure SetGrid;
    procedure arrangeColsize;
    function IsValid : Boolean;
    function IsSaved : Boolean;
    procedure LoadData;
  public
    { Public declarations }
    function Execute(vKey : integer = 0; IsEdit : boolean = False) : Boolean;
  end;

var
  frmInputTrans: TfrmInputTrans;

implementation

{$R *.dfm}

{ TfrmInputTrans }
Const
  ColNo = 0;
  Colkode = 1;
  ColNama = 2;
  ColQty = 3;
  ColHarga = 4;
  ColTotal = 5;
  ColKet = 6;
  ColSeqBrg = 7;

procedure TfrmInputTrans.arrangeColsize;
begin
 asgDetail.AutoNumberCol(Colno);
 asgDetail.ClearRows(asgDetail.RowCount-1, 1);
 asgDetail.Cells[ColNama, asgDetail.RowCount-1] := 'Total : ';
 asgDetail.FloatingFooter.ColumnCalc[ColQty] := acSUM;
 asgDetail.FloatingFooter.ColumnCalc[ColTotal] := acSUM;
 asgDetail.AutoSizeColumns(true,2);
 asgDetail.ColWidths[Colseqbrg]:=0;
end;

procedure TfrmInputTrans.InitForm;
begin
 dtptanggal.Date := now;
 txtnobon.Text :='';
 mmket.Lines.Text := '';

 dtptanggal.Enabled := (not DetailMode) or EditMode;
 txtnobon.Enabled := (not DetailMode) or EditMode;
 mmket.Enabled := (not DetailMode) or EditMode;
 btnSimpan.Enabled := (not DetailMode) or EditMode;
 btnHapusBaris.Enabled := (not DetailMode) or EditMode;
end;

procedure TfrmInputTrans.SetGrid;
begin
  asgDetail.ClearNormalCells;
  asgDetail.ClearRows(asgDetail.RowCount-1, 1);
  asgDetail.ColCount := 9;
  asgDetail.RowCount := 3;
  asgDetail.FixedCols := 1;
  asgDetail.FixedRows := 1;
  ArrangeColsize;
end;

procedure TfrmInputTrans.asgDetailGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
var i : integer;
begin
 case ACol of
    ColKode :
    begin
      AEditor := edComboList;
      asgDetail.Combobox.Items.Clear;
      for i:=0 to length(ArrBrgGlob)-1 do asgDetail.Combobox.Items.Add(ArrBrgGlob[i].kode);
    end;

    ColNama :
    begin
      AEditor := edComboList;
      asgDetail.Combobox.Items.Clear;
      for i:=0 to length(ArrBrgGlob)-1 do asgDetail.Combobox.Items.Add(ArrBrgGlob[i].Nama);
    end;
    
    ColQty, ColHarga : AEditor := edFloat;
  end;
end;

procedure TfrmInputTrans.asgDetailGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if ACol in [colno, ColQty, ColHarga, ColTotal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

function TfrmInputTrans.Execute(vKey : integer = 0; IsEdit : boolean = False) : Boolean;
begin
  ArrBrgGlob := load_Brg; //load semua master barang
  idx := vKey;
  EditMode := IsEdit;
  DetailMode := vKey <> 0;
  Initform;
  SetGrid;
  if DetailMode then LoadData;
  if EditMode then Self.Caption := 'Edit Trans Beli'
  else if DetailMode then Self.Caption := 'Detail Trans Beli'
  else Self.Caption := 'Tambah Trans Beli';
  Self.Position := poDesktopCenter;
  Self.Width := 800;
  Self.Height := 600;
  ShowModal;
  Result := ModalResult = mrOk;
end;

procedure TfrmInputTrans.asgDetailCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if (not DetailMode) or EditMode then
    CanEdit := ACol in [ColKode, ColNama, ColQty, ColHarga, ColKet]
  else CanEdit := False;
end;

procedure TfrmInputTrans.asgDetailCellValidate(Sender: TObject; Col,
  Row: Integer; var Value: String; var Valid: Boolean);
begin
  Valid := Value <> '';
  case Col of
    ColQty :
      begin
        if Valid then
          asgDetail.Floats[ColTotal, Row] := asgDetail.Floats[ColHarga, Row] * asgDetail.Floats[Col, Row];
      end;

    ColHarga :
      begin
        if Valid then
          asgDetail.Floats[ColTotal, Row] := asgDetail.Floats[ColQty, Row] * asgDetail.Floats[Col, Row];
      end;
  end;
  ArrangeColSize;
end;

procedure TfrmInputTrans.btnSimpanClick(Sender: TObject);
begin
  asgDetail.Col := ColQty;
  if IsValid and (MessageDlg('Apakah data hendak disimpan ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
    if IsSaved then ModalResult := mrOk
    else ShowMessage('Save GAGAL...');
  end;
end;

procedure TfrmInputTrans.dtptanggalKeyPress(Sender: TObject;
  var Key: Char);
begin
 if key=#13 then txtnobon.SetFocus;
end;

procedure TfrmInputTrans.btnHapusBarisClick(Sender: TObject);
begin
  asgDetail.Col := ColKet;
  if asgDetail.RowCount = 3 then
    asgDetail.ClearRows(asgDetail.Row, 1)
  else asgDetail.RemoveRows(asgDetail.Row, 1);
  arrangeColsize;
  asgDetail.Col := ColQty;
  asgDetail.Row := asgDetail.RowCount-2;
end;

function TfrmInputTrans.IsValid: Boolean;
var i, j : integer;
begin
  Result := True;
  if trim(txtnobon.Text) = '' then begin
    ShowMessage('No. Bon masih kosong.');
    txtnobon.SetFocus;
    Result := False; exit;
  end;
  for i := 1 to asgDetail.RowCount-2 do begin
    if asgDetail.Cells[ColQty, i] = '' then asgDetail.Floats[ColQty, i] := 0;
    if asgDetail.Cells[ColHarga, i] = '' then asgDetail.Floats[ColHarga, i] := 0;
    if asgDetail.Cells[ColTotal, i] = '' then asgDetail.Floats[ColTotal, i] := 0;
    if asgDetail.Cells[ColSeqBrg, i] = '' then asgDetail.Ints[ColSeqBrg, i] := 0;
  end;
  i := 0;
  while (i <= asgDetail.RowCount-2) do begin
    i := i+1;
    if (i <= asgDetail.RowCount-2) and (asgDetail.Ints[ColSeqBrg, i] = 0) and (asgDetail.Floats[ColQty, i] = 0) then begin
      if asgDetail.RowCount > 3 then begin
        asgDetail.RemoveRows(i, 1);
        i := i-1;
      end else asgDetail.ClearRows(i, 1);
    end;
    if (asgDetail.RowCount = 3) and (asgDetail.Cells[ColSeqBrg, 1] = '') then begin
      arrangeColsize;
      ShowMessage('Tidak ada transaksi yang disimpan.');
      asgDetail.Col := ColQty; asgDetail.Row := 1; asgDetail.SetFocus;
      Result := False; exit;
    end;
  end;
  arrangeColsize;
  for i := 1 to asgDetail.RowCount-2 do begin
    if (asgDetail.Floats[ColSeqBrg, i] = 0) and (asgDetail.Floats[ColQty, i] <> 0) and //ym+251120
       (asgDetail.Cells[ColKode, i] <> '') then begin
        ShowMessage('Barang baris '+IntToStr(i)+' belum terpilih.');
        asgDetail.Col := Colkode; asgDetail.Row := i; asgDetail.SetFocus;
        Result := False; exit;
    end;
    if (asgDetail.Cells[ColSeqBrg, i] <> '') and (asgDetail.Floats[ColSeqBrg, i] <> 0) then begin
      if asgDetail.Floats[ColQty, i] = 0 then begin
        ShowMessage('Qty barang baris '+IntToStr(i)+' = 0.');
        asgDetail.Col := ColQty; asgDetail.Row := i; asgDetail.SetFocus;
        Result := False; exit;
      end else if asgDetail.Floats[ColHarga, i] = 0 then begin
        ShowMessage('Harga barang baris '+IntToStr(i)+' = 0.');
        asgDetail.Col := ColHarga; asgDetail.Row := i; asgDetail.SetFocus;
        Result := False; exit;
      end;
      for j := i+1 to asgDetail.RowCount-2 do begin
        if (asgDetail.Cells[ColSeqBrg, i] <> '') and (asgDetail.Cells[ColSeqBrg, j] <> '') and
           (asgDetail.Ints[ColSeqBrg, i] <> 0) and (asgDetail.Ints[ColSeqBrg, j] <> 0) then begin
          if (asgDetail.Ints[ColSeqBrg, i] = asgDetail.Ints[ColSeqBrg, j]) then begin
            ShowMessage('Barang baris '+IntToStr(i)+' SAMA DENGAN barang baris '+IntToStr(j)+#13+'SIlahkan diubah salah satu.');
            asgDetail.Col := ColQty; asgDetail.Row := j; asgDetail.SetFocus;
            Result := False; exit;
          end;
        end;
      end;
    end;
  end;
end;

function TfrmInputTrans.IsSaved: Boolean;
var DataMst : TR_Transaksi_Beli_Master;
    DataDet : TR_Transaksi_Beli_Detail;
//    ArrDet : Arr_Det;
    i : integer;
begin
  myConnection.BeginSQL;
  try
    //save master --> 1 baris
    DataMst.Tanggal := dtptanggal.Date;
    DataMst.Nomor := trim(txtnobon.Text);
    DataMst.Total := asgDetail.Floats[ColTotal, asgDetail.RowCount-1];
    DataMst.Keterangan := trim(mmket.Lines.Text);
    if EditMode then begin
      DataMst.seq := idx;
      UEngine.update_trans(DataMst);
    end else begin
      DataMst.seq := CreateNewSeq('seq', 'Transaksi_Beli_Master');
      UEngine.Insert_trans(DataMst);
      //ambil seq tab master
    end;

    //KLO EDIT --> proses hapus detail awal
    if EditMode then begin
      UEngine.Delete_Detail_By_Master(idx);
  {    ArrDet := UEngine.load_Detail(idx);
      for i := 0 to length(ArrDet)-1 do begin
        UEngine.delete_Detail(ArrDet[i].seq);
      end;}
    end;

    //save detail --> sejumlah grid
    for i := 1 to asgDetail.RowCount-2 do begin
      DataDet.master_seq := DataMst.seq;
      DataDet.barang_seq := asgDetail.Ints[ColSeqBrg, i];
      DataDet.qty := asgDetail.Floats[ColQty, i];
      DataDet.harga := asgDetail.Floats[ColHarga, i];
      DataDet.total := asgDetail.Floats[ColTotal, i];
      DataDet.keterangan := asgDetail.Cells[ColKet, i];
      UEngine.Insert_Detail(DataDet);
    end;

    myConnection.EndSQL;
    Result := True;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

procedure TfrmInputTrans.LoadData;
var DataMst : TR_Transaksi_Beli_Master;
    ArrDet : Arr_Det;
    i, vRow, j : integer;
begin
  DataMst := UEngine.select_Trans(idx);
  ArrDet := UEngine.load_Detail(idx);

  //isi data master
  dtptanggal.Date := DataMst.Tanggal;
  txtnobon.Text := DataMst.Nomor;
  mmket.Lines.Text := DataMst.Keterangan;

  //isi data detail
  asgDetail.ClearNormalCells;
  asgDetail.ClearRows(asgDetail.RowCount-1, 1);
  asgDetail.RowCount := 3;
  for i := 0 to length(ArrDet)-1 do begin
    if i > 0 then asgDetail.AddRow;
    vRow := asgDetail.RowCount-2;
    asgDetail.Ints[ColSeqBrg, vRow] := ArrDet[i].barang_seq;
    for j := 0 to length(ArrBrgGlob) -1 do begin
      if ArrDet[i].barang_seq = ArrBrgGlob[j].seq then begin
        asgDetail.Cells[ColKode, vRow] := ArrBrgGlob[j].kode;
        asgDetail.Cells[ColNama, vRow] := ArrBrgGlob[j].Nama;
        break;
      end;
    end;
    asgDetail.Floats[ColQty, vRow] := ArrDet[i].qty;
    asgDetail.Floats[ColHarga, vRow] := ArrDet[i].harga;
    asgDetail.Floats[ColTotal, vRow] := ArrDet[i].total;
    asgDetail.Cells[ColKet, vRow] := ArrDet[i].keterangan;
  end;
  arrangeColsize;
end;

procedure TfrmInputTrans.asgDetailCellChanging(Sender: TObject; OldRow,
  OldCol, NewRow, NewCol: Integer; var Allow: Boolean);
begin
        if OldRow <> NewRow then
                asgDetailComboChange(asgDetail, NewCol, NewRow, asgDetail.Combobox.ItemIndex, asgDetail.Cells[newcol, NewRow])
end;

procedure TfrmInputTrans.asgDetailComboChange(Sender: TObject; ACol, ARow,
  AItemIndex: Integer; ASelection: String);
begin
  if (ACol in [ColKode, ColNama]) and (AItemIndex <> -1) then begin
    asgDetail.Ints[ColSeqBrg, ARow] := ArrBrgGlob[AItemIndex].seq;
    if ACol = ColKode then asgDetail.Cells[ColNama, ARow] := ArrBrgGlob[AItemIndex].Nama
    else asgDetail.Cells[ColKode, ARow] := ArrBrgGlob[AItemIndex].kode;
    asgDetail.Floats[ColHarga, ARow] := ArrBrgGlob[AItemIndex].h_beli;
  end;
end;

end.
