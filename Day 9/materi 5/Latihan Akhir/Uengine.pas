unit Uengine;

interface
uses ADOInt, URecord, StrUtils, DateUtils, Math, PostgresConnection, Controls;

procedure Insert_Brg(Data : TR_Master_Barang);
procedure update_Brg(Data : TR_Master_Barang);
procedure delete_Brg(seq : Integer);
function select_Brg(seq : integer):TR_Master_Barang;
function load_Brg : Arr_Brg;
function Is_Kode_Brg_Exist(kode : string; vKey : integer = 0) : Boolean;
function Get_Seq_Brg_By_Kode(Kode : string) : integer;

procedure Insert_trans(Data : TR_Transaksi_Beli_Master);
procedure update_trans(Data : TR_Transaksi_Beli_Master);
procedure delete_trans(seq : integer);
function select_Trans(seq : Integer):TR_Transaksi_Beli_Master;
function load_Trans(PerDari, PerSampai : TDate; SeqBrg : integer = 0; Ktgr : string = '') : Arr_Trans;

procedure Insert_Detail(Data : TR_Transaksi_Beli_Detail);
procedure update_Detail(Data : TR_Transaksi_Beli_Detail);
procedure delete_Detail(seq : integer);
procedure Delete_Detail_By_Master(SeqMst : integer);
function select_Detail(seq : Integer):TR_Transaksi_Beli_Detail;
function load_Detail(SeqMst : integer; SeqBrg : integer = 0) : Arr_Det;

function Create_New_Seq(TabName, FieldName : string) : integer;

implementation
var SQL : String;

procedure Insert_Brg(Data : TR_Master_Barang);
begin
  Data.seq := CreateNewSeq('seq', 'master_barang');
  SQL :='Insert into Master_Barang (seq,kode,nama,h_beli,h_jual,keterangan,kategori) values('
         +FormatsqlNumber(Data.seq)+','+
          FormatsqlString(Data.Kode)+','+
          FormatsqlString(Data.Nama)+','+
          FormatsqlNumber(Data.h_beli)+','+
          FormatsqlNUmber(Data.h_jual)+','+
          FormatsqlString(Data.keterangan)+','+
          FormatsqlString(Data.kategori)+')';
  Myconnection.ExecSQL(sql);
end;
procedure update_Brg(Data : TR_Master_Barang);
begin
 SQL :='Update Master_Barang SET '+
        'kode = '+FormatSqlString(Data.kode)+','+
        'nama = '+FormatsqlString(Data.Nama)+','+
        'h_beli = '+FormatsqlNumber(Data.h_beli)+','+
        'h_jual = '+Formatsqlnumber(Data.h_jual)+','+
        'keterangan = '+FormatsqlString(Data.keterangan)+','+
        'Kategori = '+FormatsqlString(Data.kategori)+' '+
        'Where seq = '+FormatsqlNumber(Data.seq);
  Myconnection.ExecSQL(sql);
end;

procedure delete_Brg(seq : integer);
begin
 SQL := 'Delete from Master_Barang where seq = '+FormatsqlNumber(seq);
  Myconnection.ExecSQL(sql);
end;

function select_Brg(seq : Integer):TR_Master_Barang;
var
buffer : _Recordset;
begin
  SQL := 'select seq,kode,nama,h_beli,h_jual,keterangan,kategori from Master_Barang where seq = '+FormatSqlNumber(seq);
  buffer := MyConnection.OpenSQL(sql);
  result.seq := buffertoInteger(buffer.Fields[0].value) ;
  result.Kode := buffertostring(buffer.Fields[1].value) ;
  result.Nama := buffertostring(buffer.Fields[2].value) ;
  result.h_beli := buffertoFloat(buffer.Fields[3].value);
  result.h_jual := buffertoFloat(buffer.Fields[4].value);
  result.keterangan := buffertoString(buffer.Fields[5].value);
  result.Kategori := buffertoString(buffer.Fields[6].value);
  buffer.Close;
end;

function load_Brg : Arr_Brg;
var
  buffer : _Recordset;
  i : integer;
begin
  SQL := 'select seq,kode,nama,h_beli,h_jual,keterangan,kategori from Master_Barang order by kode';
  buffer := MyConnection.OpenSQL(sql);
  setlength(result,buffer.RecordCount);
  for i:=0 to buffer.RecordCount-1 do
  begin
    result[i].seq := buffertoInteger(buffer.Fields[0].value) ;
    result[i].Kode := buffertostring(buffer.Fields[1].value) ;
    result[i].Nama := buffertostring(buffer.Fields[2].value) ;
    result[i].h_beli := buffertoFloat(buffer.Fields[3].value);
    result[i].h_jual := buffertoFloat(buffer.Fields[4].value);
    result[i].keterangan := buffertostring(buffer.Fields[5].value);
    result[i].kategori := buffertoString(buffer.Fields[6].value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Is_Kode_Brg_Exist(kode : string; vKey : integer = 0) : Boolean;
begin
  SQL := 'SELECT seq FROM master_barang WHERE kode = '+FormatSQLString(kode)+
         IfThen(vKey <> 0, ' AND seq <> '+FormatSQLNumber(vKey));
  Result := getIntegerFromSQL(SQL) > 0;
end;

function Get_Seq_Brg_By_Kode(Kode : string) : integer;
var buffer : _Recordset;
begin
  SQL := 'SELECT seq FROM master_barang WHERE kode = '+FormatSQLString(Kode);
//  Result := getIntegerFromSQL(SQL);
  buffer := myConnection.OpenSQL(SQL);
  Result := BufferToInteger(buffer.Fields[0].Value);
  buffer.Close;
end;

procedure Insert_trans(Data : TR_Transaksi_Beli_Master);
begin
  SQL :='Insert into Transaksi_Beli_master (seq,Tanggal,nomor,total,keterangan) values('
         +FormatsqlNumber(Data.seq)+','+
          FormatsqlDate(Data.Tanggal)+','+
          FormatsqlString(Data.Nomor)+','+
          FormatsqlNumber(Data.total)+','+
          FormatsqlString(Data.Keterangan)+')';
  Myconnection.ExecSQL(sql);
end;

procedure update_trans(Data : TR_Transaksi_Beli_Master);
begin
SQL :='Update Transaksi_Beli_Master SET '+
        'tanggal = '+FormatSQLDate(Data.Tanggal)+','+
        'nomor = '+FormatsqlString(Data.Nomor)+','+
        'total = '+FormatsqlNumber(Data.total)+','+
        'keterangan = '+FormatsqlString(Data.Keterangan)+' '+
        'Where seq = '+FormatsqlNumber(Data.seq);
  Myconnection.ExecSQL(sql);
end;

procedure delete_trans(seq : integer);
begin
 SQL := 'Delete from Transaksi_Beli_master where seq = '+FormatsqlNumber(seq);
  Myconnection.ExecSQL(sql);
end;

function select_trans(seq : Integer):TR_Transaksi_Beli_Master;
var buffer : _Recordset;
begin
  SQL := 'select seq,tanggal,nomor,total,keterangan from Transaksi_Beli_master where seq = '+FormatSqlNumber(seq);
  buffer := MyConnection.OpenSQL(sql);
  result.seq := buffertoInteger(buffer.Fields[0].value) ;
  result.tanggal := BufferToDateTime(buffer.Fields[1].value) ;
  result.nomor := buffertostring(buffer.Fields[2].value) ;
  result.total := buffertoFloat(buffer.Fields[3].value);
  result.keterangan := buffertoString(buffer.Fields[4].value);
  buffer.Close;
end;

function load_Trans(PerDari, PerSampai : TDate; SeqBrg : integer = 0; Ktgr : string = '') : Arr_Trans;
var
  buffer : _Recordset;
  Filter : string;
  i : integer;
begin
  Filter := '';
  Filter := Filter + IfThen((PerDari <> 0) and (PerSampai <> 0),
                            ' AND tanggal BETWEEN '+FormatSQLDate(PerDari)+' AND '+FormatSQLDate(PerSampai));

  if Ktgr <> '' then begin
    SQL := 'SELECT DISTINCT m.seq, m.tanggal, m.nomor, m.total, m.keterangan '+
           'FROM transaksi_beli_master m, transaksi_beli_detail d, master_barang b '+
           'WHERE m.seq = d.master_seq AND d.barang_Seq = b.seq '+Filter+
           IfThen(SeqBrg <> 0, ' AND d.barang_Seq = '+FormatSQLNumber(SeqBrg))+
           ' AND b.kategori = '+FormatSQLString(Ktgr)+' ORDER BY m.tanggal';
  end else begin
    if SeqBrg <> 0 then
      SQL := 'SELECT DISTINCT m.seq, m.tanggal, m.nomor, m.total, m.keterangan '+
             'FROM transaksi_beli_master m, transaksi_beli_detail d '+
             'WHERE m.seq = d.master_seq AND d.barang_seq = '+FormatSQLNumber(SeqBrg)+Filter
    else
      SQL := 'SELECT seq, tanggal, nomor, total, keterangan FROM Transaksi_Beli_master'+
             IfThen(Filter <> '', ' WHERE '+copy(Filter, 5, length(Filter)))+' ORDER BY tanggal';
  end;
  buffer := MyConnection.OpenSQL(sql);
  setlength(result,buffer.RecordCount);
  for i:=0 to buffer.RecordCount-1 do
  begin
    result[i].seq := buffertoInteger(buffer.Fields[0].value) ;
    result[i].Tanggal := BufferToDateTime(buffer.Fields[1].value) ;
    result[i].Nomor := buffertostring(buffer.Fields[2].value) ;
    result[i].Total := buffertoFloat(buffer.Fields[3].value);
    result[i].keterangan := buffertostring(buffer.Fields[4].value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

procedure Insert_Detail(Data : TR_Transaksi_Beli_Detail);
begin
  Data.seq := CreateNewSeq('seq', 'Transaksi_Beli_Detail');
  SQL :='Insert into Transaksi_Beli_Detail (seq,master_seq,barang_seq,qty,harga,total,keterangan) values('
         +FormatsqlNumber(Data.seq)+','+
          FormatsqlNumber(Data.master_seq)+','+
          FormatsqlNumber(Data.barang_seq)+','+
          FormatsqlNumber(Data.qty)+','+
          FormatsqlNumber(Data.harga)+','+
          FormatsqlNUmber(Data.total)+','+
          FormatsqlString(Data.keterangan)+')';
  Myconnection.ExecSQL(sql);
end;

procedure update_Detail(Data : TR_Transaksi_Beli_Detail);
begin
  SQL :='Update Transaksi_Beli_Detail SET '+
        'master_seq = '+FormatSqlNumber(Data.master_seq)+','+
        'barang_seq = '+FormatSqlNumber(Data.barang_seq)+','+
        'qty = '+FormatSqlNumber(Data.qty)+','+
        'harga= '+FormatSqlNumber(Data.harga)+','+
        'total = '+FormatSqlNumber(Data.total)+','+
        'keterangan = '+FormatsqlString(Data.keterangan)+' '+
        'Where seq = '+FormatsqlNumber(Data.seq);
  Myconnection.ExecSQL(sql);
end;

procedure delete_Detail(seq : integer);
begin
  SQL := 'Delete from Transaksi_Beli_Detail where seq = '+FormatsqlNumber(seq);
  Myconnection.ExecSQL(sql);
end;

procedure Delete_Detail_By_Master(SeqMst : integer);
begin
  SQL := 'DELETE FROM transaksi_Beli_detail WHERE master_Seq = '+FormatSQLNumber(SeqMst);
  myConnection.ExecSQL(SQL);
end;

function select_Detail(seq : Integer):TR_Transaksi_Beli_Detail;
var buffer : _Recordset;
begin
  SQL := 'select seq,master_seq,barang_seq,qty,harga,total,keterangan from Transaksi_Beli_Detail where seq = '+FormatSqlNumber(seq);
  buffer := MyConnection.OpenSQL(sql);
  result.seq := buffertoInteger(buffer.Fields[0].value) ;
  result.master_seq := BufferToInteger(buffer.Fields[1].value) ;
  result.barang_seq := BufferToInteger(buffer.Fields[2].value) ;
  result.qty := buffertoFloat(buffer.Fields[3].value);
  result.harga := buffertoFloat(buffer.Fields[4].value);
  result.total := buffertoFloat(buffer.Fields[5].value);
  result.keterangan := buffertoString(buffer.Fields[6].value);
  buffer.Close;
end;

function load_Detail(SeqMst : integer; SeqBrg : integer = 0) : Arr_Det;
var
  buffer : _Recordset;
  i : integer;
begin
  SQL := 'SELECT seq, master_seq, barang_seq, qty, harga, total, keterangan '+
         'FROM Transaksi_Beli_Detail WHERE master_seq = '+FormatSQLNumber(SeqMst)+
         IfThen(SeqBrg <> 0, ' AND barang_seq = '+FormatSQLNumber(SeqBrg));
  buffer := MyConnection.OpenSQL(sql);
  setlength(result,buffer.RecordCount);
  for i:=0 to buffer.RecordCount-1 do
  begin
    result[i].seq := buffertoInteger(buffer.Fields[0].value) ;
    result[i].master_seq := BufferToInteger(buffer.Fields[1].value) ;
    result[i].barang_seq := BufferToInteger(buffer.Fields[2].value) ;
    Result[i].qty := BufferToFloat(buffer.Fields[3].Value);
    result[i].harga := buffertoFloat(buffer.Fields[4].value);
    result[i].total := buffertoFloat(buffer.Fields[5].value);
    result[i].keterangan := buffertostring(buffer.Fields[6].value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Create_New_Seq(TabName, FieldName : string) : integer;
begin
  SQL := 'SELECT max('+FieldName+') FROM '+TabName;
  Result := getIntegerFromSQL(SQL) + 1;
end;

end.
