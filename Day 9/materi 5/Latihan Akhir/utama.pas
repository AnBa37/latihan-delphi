unit utama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmUtama = class(TForm)
    btnbarang: TButton;
    btntransBeli: TButton;
    Button3: TButton;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
    procedure btnbarangClick(Sender: TObject);
    procedure btntransBeliClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUtama: TfrmUtama;

implementation

uses pengolahanbarang, transaksibeli, LaporanBeli;

{$R *.dfm}

procedure TfrmUtama.btnTutupClick(Sender: TObject);
begin
 close;
end;

procedure TfrmUtama.btnbarangClick(Sender: TObject);
begin
 frmpeng_brg.Execute;
end;

procedure TfrmUtama.btntransBeliClick(Sender: TObject);
begin
 frmTransaksiBeli.Execute;
end;

procedure TfrmUtama.Button3Click(Sender: TObject);
begin
  frmLaporan.Execute;
end;

end.
