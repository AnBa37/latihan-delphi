unit transaksibeli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, Grids, BaseGrid, AdvGrid, AdvObj;

type
  TfrmTransaksibeli = class(TForm)
    GroupBox1: TGroupBox;
    cmbKode: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    btnLoadData: TButton;
    btnReset: TButton;
    btnTambah: TButton;
    btnEdit: TButton;
    btnHapus: TButton;
    btnDetail: TButton;
    asgTransaksi: TAdvStringGrid;
    cmbNama: TComboBox;
    procedure asgTransaksiGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnTambahClick(Sender: TObject);
    procedure btnLoadDataClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure cmbKodeSelect(Sender: TObject);
    procedure cmbNamaSelect(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDetailClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetGrid;
    procedure InitForm;
    procedure ArrangeColsize;
    procedure DataCombo;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTransaksibeli: TfrmTransaksibeli;

implementation

uses Uengine, inputtransaksi, Urecord, PostgresConnection;

{$R *.dfm}
Const
  ColNode = 0;
  ColKode = 1;
  ColNama = 2;
  ColQty = 3;
  ColHarga = 4;
  ColTotal = 5;
  ColKet = 6;
  ColSeq = 7;

procedure TfrmTransaksibeli.ArrangeColsize;
begin
//  asgTransaksi.FloatingFooter.ColumnCalc[ColTotal] := acSUM;
  asgTransaksi.AutoSizeColumns(True, 2);
  asgTransaksi.ColWidths[ColSeq] := 0;
end;

procedure TfrmTransaksibeli.DataCombo;
var i : integer;
    ArrBrg : Arr_brg;
begin
  cmbKode.Items.Clear;
  cmbNama.Items.Clear;
  ArrBrg := load_Brg;
  for i:=0 to length(ArrBrg)-1 do begin
   cmbKode.Items.Add(ArrBrg[i].kode);
   cmbNama.Items.Add(ArrBrg[i].Nama);
  end;
  cmbKode.Items.Insert(0, 'SEMUA');
  cmbNama.Items.Insert(0, 'SEMUA');
end;

procedure TfrmTransaksibeli.Execute;
begin
  DataCombo;
  Initform;
  SetGrid;

 // load_data;
  ShowModal;
end;

procedure TfrmTransaksibeli.InitForm;
begin
 cmbKode.ItemIndex := 0;
 cmbNama.ItemIndex := 0;
end;

procedure TfrmTransaksibeli.SetGrid;
begin
  asgTransaksi.Clear;
  asgTransaksi.ColCount := 9;
  asgTransaksi.RowCount := 4;
  asgTransaksi.FixedCols := 0;
  asgTransaksi.FixedRows := 2;

  //set isi grid
  asgTransaksi.MergeCells(ColNode, 0, 1, 2);
  asgTransaksi.Cells[ColNode, 0] := '[+]';

  asgTransaksi.MergeCells(ColKode, 0, 2, 1);
  asgTransaksi.Cells[ColKode, 0] := 'Tanggal';
  asgTransaksi.Cells[ColKode, 1] := 'Kode';
  asgTransaksi.Cells[ColNama, 1] := 'Nama';

  asgTransaksi.MergeCells(ColQty, 0, 2, 1);
  asgTransaksi.Cells[ColQty, 0] := 'Nomor';
  asgTransaksi.Cells[ColQty, 1] := 'Qty';
  asgTransaksi.Cells[ColHarga, 1] := '@ Harga';

  asgTransaksi.MergeCells(ColTotal, 0, 1, 2);
  asgTransaksi.Cells[ColTotal, 0] := 'Total';

  asgTransaksi.MergeCells(ColKet, 0, 1, 2);
  asgTransaksi.Cells[ColKet, 0] := 'Keterangan';

  asgTransaksi.MergeCells(ColSeq, 0, 1, 2);
  asgTransaksi.Cells[ColSeq, 0] := 'Seq';

  asgTransaksi.MergeCells(ColSeq+1, 0, 1, 2);

  ArrangeColsize;
end;

procedure TfrmTransaksibeli.asgTransaksiGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 2 then HAlign := taCenter
  else if ACol in [ColTotal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmTransaksibeli.btnTambahClick(Sender: TObject);
begin
  if frmInputTrans.Execute then btnLoadData.Click;
end;

procedure TfrmTransaksibeli.btnLoadDataClick(Sender: TObject);
var ArrMst : Arr_trans;
    ArrDet : Arr_Det;
    SeqBrg, i, vRow, j, row_anak : integer;
    DataBrg : TR_Master_Barang;
    GTotal : real;
begin
  //dapetin seq barang
  if cmbKode.ItemIndex = 0 then SeqBrg := 0
  else  //seq barang <> 0
    SeqBrg := UEngine.Get_Seq_Brg_By_Kode(cmbKode.Text);

  asgTransaksi.ClearNormalCells;
  asgTransaksi.ClearRows(asgTransaksi.RowCount-1, 1);
  asgTransaksi.RowCount := 4;
  setlength(ArrDet, 0);
  //load data master
  ArrMst := UEngine.load_Trans(0, 0, SeqBrg); GTotal := 0;
  for i := 0 to length(ArrMst)-1 do begin
    if i > 0 then asgTransaksi.AddRow;
    vRow := asgTransaksi.RowCount-2;
    asgTransaksi.MergeCells(ColKode, vRow, 2, 1);
    asgTransaksi.Dates[ColKode, vRow] := ArrMst[i].Tanggal;
    asgTransaksi.MergeCells(ColQty, vRow, 2, 1);
    asgTransaksi.Cells[ColQty, vRow] := ArrMst[i].Nomor;
    asgTransaksi.Floats[ColTotal, vRow] := ArrMst[i].Total; GTotal := GTotal + ArrMst[i].Total;
    asgTransaksi.Cells[ColKet, vRow] := ArrMst[i].Keterangan;
    asgTransaksi.Ints[ColSeq, vRow] := ArrMst[i].seq;

    //load data detail dari master terpilih
    ArrDet := UEngine.load_Detail(ArrMst[i].seq, SeqBrg);
    for j := 0 to length(ArrDet)-1 do begin
      asgTransaksi.AddRow;
      row_anak := asgTransaksi.RowCount-2;
      DataBrg := UEngine.select_Brg(ArrDet[j].barang_seq);
      asgTransaksi.Cells[ColKode, row_anak] := DataBrg.kode;
      asgTransaksi.Cells[ColNama, row_anak] := DataBrg.Nama;
      asgTransaksi.Alignments[ColQty, row_anak] := taRightJustify;
      asgTransaksi.Floats[ColQty, row_anak] := ArrDet[j].qty;
      asgTransaksi.Alignments[ColHarga, row_anak] := taRightJustify;
      asgTransaksi.Floats[ColHarga, row_anak] := ArrDet[j].harga;
      asgTransaksi.Floats[ColTotal, row_anak] := ArrDet[j].total;
      asgTransaksi.Cells[ColKet, row_anak] := ArrDet[j].keterangan;
    end;

    row_anak := asgTransaksi.RowCount-2;
    if vRow <> row_anak then asgTransaksi.AddNode(vRow, row_anak-vRow+1);
  end;
  asgTransaksi.Floats[ColTotal, asgTransaksi.RowCount-1] := GTotal;
  ArrangeColSize;
end;

procedure TfrmTransaksibeli.btnResetClick(Sender: TObject);
begin
  InitForm; SetGrid;
end;

procedure TfrmTransaksibeli.cmbKodeSelect(Sender: TObject);
begin
  //pilih nama dari kode terpilih
  cmbNama.ItemIndex := cmbKode.ItemIndex;
end;

procedure TfrmTransaksibeli.cmbNamaSelect(Sender: TObject);
begin
  //pilih kode dari nama terpilih
  cmbKode.ItemIndex := cmbNama.ItemIndex;
end;

procedure TfrmTransaksibeli.btnEditClick(Sender: TObject);
begin
  if asgTransaksi.Cells[ColSeq, asgTransaksi.Row] <> '' then begin
    if frmInputTrans.Execute(asgTransaksi.Ints[ColSeq, asgTransaksi.Row], True) then
      btnLoadData.Click;
  end else ShowMessage('Data yang dipilih bukan data master.');
end;

procedure TfrmTransaksibeli.btnDetailClick(Sender: TObject);
begin
  if asgTransaksi.Cells[ColSeq, asgTransaksi.Row] <> '' then begin
    frmInputTrans.Execute(asgTransaksi.Ints[ColSeq, asgTransaksi.Row]);
  end else ShowMessage('Data yang dipilih bukan data master.');
end;

procedure TfrmTransaksibeli.btnHapusClick(Sender: TObject);
begin
  if asgTransaksi.Cells[ColSeq, asgTransaksi.Row] <> '' then begin
    if (MessageDlg('Data hendak dihapus ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
      myConnection.BeginSQL;
      try
        UEngine.Delete_Detail_By_Master(asgTransaksi.Ints[ColSeq, asgTransaksi.Row]);
        UEngine.delete_trans(asgTransaksi.Ints[ColSeq, asgTransaksi.Row]);
        myConnection.EndSQL;
        btnLoadData.Click;
      except
        myConnection.UndoSQL;
        ShowMessage('HAPUS GAGAL...');
      end;
    end;
  end else ShowMessage('Data yang dipilih bukan data master.');
end;

end.
