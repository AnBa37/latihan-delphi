unit inputbarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txtKode: TAdvEdit;
    txtNama: TAdvEdit;
    txthbeli: TAdvEdit;
    txthjual: TAdvEdit;
    Label5: TLabel;
    mmket: TMemo;
    Label6: TLabel;
    btnSimpan: TButton;
    btnBatal: TButton;
    cmbkategori: TComboBox;
    procedure cmbkategoriKeyPress(Sender: TObject; var Key: Char);
    procedure btnBatalClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
  private
    { Private declarations }
    idx : Integer;
    EditMode : boolean;
    procedure Initform;
    procedure Load_Data;
    function IsValid : Boolean;
    function IsSaved : Boolean;
  public
    { Public declarations }
    function Execute(vKey : integer = 0) : Boolean;
  end;

var
  frmInput: TfrmInput;

implementation

uses Uengine, Urecord, PostgresConnection;

{$R *.dfm}

procedure TfrmInput.cmbkategoriKeyPress(Sender: TObject; var Key: Char);
begin
 if key=#13 then btnsimpan.SetFocus;
end;

function TfrmInput.Execute(vKey : integer = 0) : Boolean;
begin
  idx := vKey;
  EditMode := vKey <> 0;
  Initform;
  if EditMode then Load_Data;
  if EditMode then begin
    frmInput.Caption :='Edit Data';
    frmInput.btnSimpan.Caption := 'Edit';
  end else begin
    frmInput.Caption := 'Input Data';
    frmInput.btnSimpan.Caption := 'Simpan';
  end;
  frmInput.Position := poDesktopCenter;
  ActiveControl := txtKode;
  showmodal;
  Result := ModalResult = mrOk;
end;

procedure TfrmInput.Initform;
begin
  txtKode.Text := '';
  txtNama.Text :='';
  txthbeli.FloatValue :=0;
  txthjual.FloatValue :=0;
  mmKet.Lines.Text := '';
  cmbKategori.ItemIndex := -1;
end;

procedure TfrmInput.btnBatalClick(Sender: TObject);
begin
  close;
end;

procedure TfrmInput.Load_Data;
var Data : TR_Master_Barang;
begin
  Data := Select_brg(idx);
  txtKode.Text := Data.kode;
  txtNama.Text := Data.nama;
  txthbeli.FloatValue := Data.h_beli;
  txthjual.FloatValue := Data.h_jual;
  mmket.Lines.Text := Data.keterangan;

  if Data.kategori = 'B' then cmbkategori.ItemIndex := 0
  else if Data.kategori = 'H' then cmbkategori.ItemIndex := 1
  else if Data.kategori = 'J' then cmbkategori.ItemIndex := 2;
end;

function TfrmInput.IsSaved: Boolean;
var Data : TR_Master_Barang;
begin
 myConnection.BeginSQL;
 try
    Data.kode := trim(txtKode.Text);
    Data.Nama := trim(txtNama.Text);
    Data.h_beli := txthbeli.FloatValue;
    Data.h_jual := txthjual.FloatValue;
    Data.keterangan := trim(mmket.Lines.Text);
    if cmbKategori.ItemIndex = 0 then Data.kategori := 'B'
    else if cmbkategori.ItemIndex = 1 then Data.kategori := 'H'
    else if cmbkategori.ItemIndex = 2 then Data.kategori := 'J';
    if EditMode then begin
      Data.seq := idx;
      UEngine.update_Brg(Data);
    end else UEngine.Insert_Brg(Data);
    myConnection.EndSQL;
    Result := True;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

function TfrmInput.IsValid: Boolean;
begin
    Result := True;
    if trim(txtKode.Text) = '' then begin
    ShowMessage('Kode kosong.');
    txtKode.SetFocus;
    Result := False; exit;
  end else if UEngine.Is_Kode_Brg_Exist(trim(txtKode.Text), idx) then begin
    ShowMessage('Kode sudah ada, silahkan diganti.');
    txtKode.SetFocus;
    Result := False; exit;
  end else if trim(txtNama.Text) = '' then begin
    ShowMessage('Nama kosong.');
    txtNama.SetFocus;
    Result := False; exit;
  end else if txthbeli.FloatValue = 0 then begin
    ShowMessage('Harga beli = 0.');
    txthbeli.SetFocus;
    Result := False; exit;
  end else if txthjual.FloatValue = 0 then begin
    ShowMessage('Harga jual = 0.');
    txthjual.SetFocus;
    Result := False; exit;
  end else if cmbkategori.ItemIndex = -1 then begin
    ShowMessage('Kategori belum dipilih.');
    cmbkategori.SetFocus;
    Result := False; exit;
  end
end;

procedure TfrmInput.btnSimpanClick(Sender: TObject);
begin
  if IsValid and (MessageDlg('Apakah data hendak disimpan ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
    if IsSaved then ModalResult := mrOk
    else ShowMessage('Save GAGAL...');
  end;
end;

end.
