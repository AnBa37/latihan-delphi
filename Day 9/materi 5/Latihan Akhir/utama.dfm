object frmUtama: TfrmUtama
  Left = 534
  Top = 278
  BorderStyle = bsDialog
  Caption = 'Form Utama'
  ClientHeight = 145
  ClientWidth = 144
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnbarang: TButton
    Left = 16
    Top = 8
    Width = 113
    Height = 25
    Caption = 'Master Barang'
    TabOrder = 0
    OnClick = btnbarangClick
  end
  object btntransBeli: TButton
    Left = 16
    Top = 40
    Width = 113
    Height = 25
    Caption = 'Transaksi Beli'
    TabOrder = 1
    OnClick = btntransBeliClick
  end
  object Button3: TButton
    Left = 16
    Top = 72
    Width = 113
    Height = 25
    Caption = 'Laporan Pembelian'
    TabOrder = 2
    OnClick = Button3Click
  end
  object btnTutup: TButton
    Left = 16
    Top = 104
    Width = 113
    Height = 25
    Caption = 'Tutup'
    TabOrder = 3
    OnClick = btnTutupClick
  end
end
