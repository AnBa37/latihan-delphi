program latihan_akhir;

uses
  Forms,
  utama in 'utama.pas' {frmUtama},
  pengolahanbarang in 'pengolahanbarang.pas' {frmpeng_brg},
  Urecord in 'Urecord.pas',
  Uengine in 'Uengine.pas',
  PostgresConnection in 'PostgresConnection.pas' {myConnection: TDataModule},
  inputbarang in 'inputbarang.pas' {frmInput},
  transaksibeli in 'transaksibeli.pas' {frmTransaksibeli},
  inputtransaksi in 'inputtransaksi.pas' {frmInputTrans},
  LaporanBeli in 'LaporanBeli.pas' {frmLaporan};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmUtama, frmUtama);
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(Tfrmpeng_brg, frmpeng_brg);
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TfrmTransaksibeli, frmTransaksibeli);
  Application.CreateForm(TfrmInputTrans, frmInputTrans);
  Application.CreateForm(TfrmLaporan, frmLaporan);
  Application.Run;
end.
