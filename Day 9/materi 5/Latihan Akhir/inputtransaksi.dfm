object frmInputTrans: TfrmInputTrans
  Left = 229
  Top = 238
  Caption = 'Input Transaksi'
  ClientHeight = 367
  ClientWidth = 483
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  DesignSize = (
    483
    367)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 20
    Width = 39
    Height = 13
    Caption = 'Tanggal'
  end
  object Label2: TLabel
    Left = 16
    Top = 52
    Width = 36
    Height = 13
    Caption = 'No.Bon'
  end
  object Label3: TLabel
    Left = 8
    Top = 295
    Width = 55
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Keterangan'
  end
  object dtptanggal: TDateTimePicker
    Left = 72
    Top = 16
    Width = 90
    Height = 21
    Date = 40590.866102430600000000
    Time = 40590.866102430600000000
    TabOrder = 0
    OnKeyPress = dtptanggalKeyPress
  end
  object txtnobon: TAdvEdit
    Left = 72
    Top = 48
    Width = 121
    Height = 21
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    MaxLength = 20
    TabOrder = 1
    Text = 'txtnobon'
    Visible = True
    Version = '2.9.2.1'
  end
  object asgDetail: TAdvStringGrid
    Left = 8
    Top = 80
    Width = 472
    Height = 208
    Cursor = crDefault
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 9
    DefaultRowHeight = 21
    RowCount = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goTabs]
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 2
    OnGetAlignment = asgDetailGetAlignment
    OnCellChanging = asgDetailCellChanging
    OnCanEditCell = asgDetailCanEditCell
    OnCellValidate = asgDetailCellValidate
    OnGetEditorType = asgDetailGetEditorType
    OnComboChange = asgDetailComboChange
    ActiveCellFont.Charset = DEFAULT_CHARSET
    ActiveCellFont.Color = clWindowText
    ActiveCellFont.Height = -11
    ActiveCellFont.Name = 'MS Sans Serif'
    ActiveCellFont.Style = [fsBold]
    AutoSize = True
    CellNode.ShowTree = False
    ColumnHeaders.Strings = (
      'No.'
      'Kode'
      'Nama'
      'Qty'
      'Harga'
      'Total'
      'Keterangan'
      'SeqBrg')
    ColumnSize.Stretch = True
    ControlLook.FixedGradientHoverFrom = clGray
    ControlLook.FixedGradientHoverTo = clWhite
    ControlLook.FixedGradientDownFrom = clGray
    ControlLook.FixedGradientDownTo = clSilver
    ControlLook.ControlStyle = csClassic
    ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownHeader.Font.Color = clWindowText
    ControlLook.DropDownHeader.Font.Height = -11
    ControlLook.DropDownHeader.Font.Name = 'Tahoma'
    ControlLook.DropDownHeader.Font.Style = []
    ControlLook.DropDownHeader.Visible = True
    ControlLook.DropDownHeader.Buttons = <>
    ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
    ControlLook.DropDownFooter.Font.Color = clWindowText
    ControlLook.DropDownFooter.Font.Height = -11
    ControlLook.DropDownFooter.Font.Name = 'Tahoma'
    ControlLook.DropDownFooter.Font.Style = []
    ControlLook.DropDownFooter.Visible = True
    ControlLook.DropDownFooter.Buttons = <>
    EnhRowColMove = False
    Filter = <>
    FilterDropDown.Font.Charset = DEFAULT_CHARSET
    FilterDropDown.Font.Color = clWindowText
    FilterDropDown.Font.Height = -11
    FilterDropDown.Font.Name = 'Tahoma'
    FilterDropDown.Font.Style = []
    FilterDropDownClear = '(All)'
    FixedFooters = 1
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'Tahoma'
    FixedFont.Style = [fsBold]
    FloatFormat = '%.2n'
    FloatingFooter.Visible = True
    Navigation.AdvanceOnEnter = True
    Navigation.AdvanceInsert = True
    Navigation.AutoComboDropSize = True
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'MS Sans Serif'
    PrintSettings.Font.Style = []
    PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
    PrintSettings.FixedFont.Color = clWindowText
    PrintSettings.FixedFont.Height = -11
    PrintSettings.FixedFont.Name = 'Tahoma'
    PrintSettings.FixedFont.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'MS Sans Serif'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'MS Sans Serif'
    PrintSettings.FooterFont.Style = []
    PrintSettings.Borders = pbNoborder
    PrintSettings.Centered = False
    PrintSettings.PageNumSep = '/'
    SearchFooter.Font.Charset = DEFAULT_CHARSET
    SearchFooter.Font.Color = clWindowText
    SearchFooter.Font.Height = -11
    SearchFooter.Font.Name = 'Tahoma'
    SearchFooter.Font.Style = []
    SelectionColor = clHighlight
    SelectionTextColor = clHighlightText
    SortSettings.Column = 0
    Version = '6.0.4.2'
    WordWrap = False
    ColWidths = (
      64
      40
      44
      32
      46
      41
      78
      52
      70)
  end
  object mmket: TMemo
    Left = 72
    Top = 295
    Width = 248
    Height = 65
    Anchors = [akLeft, akRight, akBottom]
    Lines.Strings = (
      'mmket')
    MaxLength = 150
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object btnSimpan: TButton
    Left = 327
    Top = 335
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Simpan'
    TabOrder = 4
    OnClick = btnSimpanClick
  end
  object btnBatal: TButton
    Left = 407
    Top = 335
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Batal'
    ModalResult = 2
    TabOrder = 5
  end
  object btnHapusBaris: TButton
    Left = 405
    Top = 296
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Hapus Baris'
    TabOrder = 6
    OnClick = btnHapusBarisClick
  end
end
