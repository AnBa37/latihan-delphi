program Tahap3No1;

uses
  Forms,
  Input in 'Input.pas' {frmInput},
  InputDetail in 'InputDetail.pas' {frmInputDetail},
  Tampil in 'Tampil.pas' {frmTampil},
  Report in 'Report.pas' {frmReport};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TfrmInputDetail, frmInputDetail);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.CreateForm(TfrmReport, frmReport);
  Application.Run;
end.
