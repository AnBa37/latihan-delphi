unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    txtInput: TAdvEdit;
    btnIsi: TButton;
    btnReport: TButton;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnIsiClick(Sender: TObject);
    procedure txtInputExit(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
    procedure btnReportClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

uses InputDetail, Tampil, Report;

{$R *.dfm}

{ TfrmInput }

procedure TfrmInput.InitForm;
begin
  txtInput.IntValue := 0;
  setlength(frmInputDetail.ArrData, 0);
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

procedure TfrmInput.btnIsiClick(Sender: TObject);
var i : integer;
begin
  if (txtInput.IntValue >= 1) and (txtInput.IntValue <= 20) then begin
    setlength(frmInputDetail.ArrData, txtInput.IntValue);
    for i := 0 to txtInput.IntValue-1 do begin
      //apakah i???? i ADALAH index array ???  BETUL GAK???
      frmInputDetail.Execute(i, False, False);
    end;
  end else ShowMessage('Range data antara 1-20.');
end;

procedure TfrmInput.txtInputExit(Sender: TObject);
begin
  setlength(frmInputDetail.ArrData, 0);
end;

procedure TfrmInput.btnTampilClick(Sender: TObject);
begin
  frmTampil.Execute;
end;

procedure TfrmInput.btnReportClick(Sender: TObject);
begin
  frmReport.Execute;
end;

end.
