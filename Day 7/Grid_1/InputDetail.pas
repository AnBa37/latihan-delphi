unit InputDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TR_Data = Record
    id : integer;
    nama : string;
    dept : string;
    gaji : real;
  end;
  AR_Data = array of TR_Data;

  TfrmInputDetail = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    sttId: TStaticText;
    txtNama: TAdvEdit;
    txtGaji: TAdvEdit;
    cmbDept: TComboBox;
    btnOk: TButton;
    procedure cmbDeptKeyPress(Sender: TObject; var Key: Char);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
    idx : integer;
    EditMode, DetailMode : Boolean;
    procedure InitForm;
    procedure LoadData;
  public
    { Public declarations }
    ArrData : AR_Data;
    procedure Execute(Id : integer; isEdit, isDetail : Boolean);
  end;

var
  frmInputDetail: TfrmInputDetail;

implementation

{$R *.dfm}

procedure TfrmInputDetail.cmbDeptKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then txtGaji.SetFocus;
end;

procedure TfrmInputDetail.Execute(Id: integer; isEdit, isDetail: Boolean);
begin
  idx := id; EditMode := isEdit; DetailMode := isDetail;
  InitForm; sttId.Caption := IntToStr(Id);
  if EditMode or DetailMode then LoadData;
  if EditMode then frmInputDetail.Caption := 'Edit Data'
  else if DetailMode then frmInputDetail.Caption := 'Detail Data'
  else frmInputDetail.Caption := 'Input Data';
  frmInputDetail.Position := poDesktopCenter;
  ShowModal;
end;

procedure TfrmInputDetail.InitForm;
begin
  sttId.Caption := '';
  txtNama.Text := '';
  cmbDept.ItemIndex := -1;
  txtGaji.FloatValue := 0;

  txtNama.ReadOnly := DetailMode;
  cmbDept.Enabled := not DetailMode;
  txtGaji.ReadOnly := DetailMode;
end;

procedure TfrmInputDetail.btnOkClick(Sender: TObject);
begin
  if DetailMode and not EditMode then Close
  else begin //isi ke array data
    if Trim(txtNama.Text) = '' then begin
      ShowMessage('Nama tidak boleh kosong.');
      txtNama.SetFocus;
      exit; //supaya tidak keluar dari form input dan tidak masuk ke array
    end else if cmbDept.ItemIndex = -1 then begin
      ShowMessage('Dept belum dipilih.');
      cmbDept.SetFocus;
      exit;
    end else if txtGaji.FloatValue = 0 then begin
      ShowMessage('Gaji tidak boleh 0.');
      txtGaji.SetFocus;
      exit;
    end;

    ArrData[idx].id := idx;
    ArrData[idx].nama := Trim(txtNama.Text);
    ArrData[idx].dept := cmbDept.Text;
    ArrData[idx].gaji := txtGaji.FloatValue;
    close;
  end;
end;

procedure TfrmInputDetail.LoadData;
begin
  txtNama.Text := ArrData[idx].nama;
  txtGaji.FloatValue := ArrData[idx].gaji;
//  cmbDept.Text := ArrData[idx].dept;
  if ArrData[idx].dept = 'Pembelian' then cmbDept.ItemIndex := 0
  else if ArrData[idx].dept = 'Marketing' then cmbDept.ItemIndex := 1
  else if ArrData[idx].dept = 'Finance' then cmbDept.ItemIndex := 2;
end;

end.
