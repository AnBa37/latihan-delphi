program prjGrid02;

uses
  Forms,
  InputData in 'InputData.pas' {frmInput},
  PindahData in 'PindahData.pas' {frmPindah},
  ReportData in 'ReportData.pas' {frmReport};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TfrmPindah, frmPindah);
  Application.CreateForm(TfrmReport, frmReport);
  Application.Run;
end.
