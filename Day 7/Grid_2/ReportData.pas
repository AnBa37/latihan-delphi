unit ReportData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, AdvObj;

type
  TfrmReport = class(TForm)
    asgReport: TAdvStringGrid;
    procedure asgReportGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgReportContractNode(Sender: TObject; ARow,
      ARowreal: Integer);
    procedure asgReportExpandNode(Sender: TObject; ARow,
      ARowreal: Integer);
  private
    { Private declarations }
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
  public
    { Public declarations }
    ArrTgl : array of TDate;
    procedure Execute;
  end;

var
  frmReport: TfrmReport;

implementation

uses InputData;

{$R *.dfm}
Const
  ColNode = 0;
  ColKode = 1;
  ColNama = 2;
  ColHarga = 3;
  ColStok = 4;
  ColTotal = 5;

procedure TfrmReport.ArrangeColSize;
begin
  asgReport.AutoSizeColumns(True, 4);
end;

procedure TfrmReport.asgReportGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 2 then HAlign := taCenter
  else if ACol in [ColStok, ColTotal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmReport.Execute;
begin
//  setlength(ArrTgl, 0);
  SetGrid;
  LoadData;
  ShowModal;
end;

procedure TfrmReport.LoadData;
var i, j, R1, R2 : integer;
    stok, total : real;
begin
  SetGrid;
  for i := 0 to length(ArrTgl)-1 do begin //isi bapa
    if i > 0 then asgReport.AddRow;
    R1 := asgReport.RowCount-1;
    stok := 0; total := 0;
    asgReport.MergeCells(ColKode, R1, 3, 1);
    asgReport.Cells[ColKode, R1] := FormatDateTime('dd MMMM yyyy', ArrTgl[i]);
    for j := 0 to length(frmInput.Arr)-1 do begin //isi anak
      if frmInput.Arr[j].tanggal = ArrTgl[i] then begin
        asgReport.AddRow; R2 := asgReport.RowCount-1;
        asgReport.Cells[ColKode, R2] := frmInput.Arr[j].Kode;
        asgReport.Cells[ColNama, R2] := frmInput.Arr[j].Nama;
        asgReport.Alignments[ColHarga, R2] := taRightJustify;
        asgReport.Floats[ColHarga, R2] := frmInput.Arr[j].harga;
        asgReport.Floats[ColStok, R2] := frmInput.Arr[j].stok;
        stok := stok + frmInput.Arr[j].stok;
        asgReport.Floats[ColTotal, R2] := frmInput.Arr[j].total;
        total := total + frmInput.Arr[j].total;
      end;
    end;
    asgReport.Floats[ColStok, R1] := stok;
    asgReport.Floats[ColTotal, R1] := total;
    asgReport.FontStyles[ColKode, R1] := [fsBold];
    asgReport.FontStyles[ColStok, R1] := [fsBold];
    asgReport.FontStyles[ColTotal, R1] := [fsBold];
    asgReport.FontColors[ColKode, R1] := clTeal;
    asgReport.FontColors[ColStok, R1] := clTeal;
    asgReport.FontColors[ColTotal, R1] := clTeal;
    R2 := asgReport.RowCount-1;
    if R1 <> R2 then asgReport.AddNode(R1, R2-R1+1);
  end;
  asgReport.ContractAll;
  ArrangeColSize;
end;

procedure TfrmReport.SetGrid;
begin
  asgReport.Clear;
  asgReport.ColCount := 7;
  asgReport.RowCount := 3;
  asgReport.FixedCols := 0;
  asgReport.FixedRows := 2;

  asgReport.MergeCells(ColNode, 0, 1, 2);
  asgReport.Cells[ColNode, 0] := '[+]';

  asgReport.MergeCells(ColKode, 0, 3, 1);
  asgReport.Cells[ColKode, 0] := 'Tgl Beli';
  asgReport.Cells[ColKode, 1] := 'Kode';
  asgReport.Cells[ColNama, 1] := 'Nama';
  asgReport.Cells[ColHarga, 1] := 'Harga';

  asgReport.MergeCells(ColStok, 0, 1, 2);
  asgReport.Cells[ColStok, 0] := 'Stok';

  asgReport.MergeCells(ColTotal, 0, 1, 2);
  asgReport.Cells[ColTotal, 0] := 'Total';

  asgReport.MergeCells(ColTotal+1, 0, 1, 2);

  ArrangeColSize;
end;

procedure TfrmReport.asgReportContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmReport.asgReportExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

end.
