unit TampilData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmTampil = class(TForm)
    btnShowAll: TButton;
    btnByNama: TButton;
    btnByNilai: TButton;
    btnByTgl: TButton;
    procedure btnShowAllClick(Sender: TObject);
    procedure btnByNamaClick(Sender: TObject);
    procedure btnByNilaiClick(Sender: TObject);
    procedure btnByTglClick(Sender: TObject);
  private
    { Private declarations }
    function ShowHasilGabung : string;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTampil: TfrmTampil;

implementation

uses InputDetail;

{$R *.dfm}

{ TfrmTampil }

procedure TfrmTampil.Execute;
begin
  ShowModal;
end;

procedure TfrmTampil.btnShowAllClick(Sender: TObject);
var i : integer;
    hasil : string;
begin
  hasil := '';
  for i := 0 to length(frmIsiData.Arr1)-1 do begin
    hasil := hasil + frmIsiData.Arr1[i].nama + ' ' + IntToStr(frmIsiData.Arr1[i].nilai) + ' '+ DateToStr(frmIsiData.Arr1[i].tgl) + #13;
  end;
  for i := 0 to length(frmIsiData.Arr2)-1 do begin
    hasil := hasil + frmIsiData.Arr2[i].nama + ' ' + IntToStr(frmIsiData.Arr2[i].nilai) + ' '+ DateToStr(frmIsiData.Arr2[i].tgl) + #13;
  end;
  ShowMessage(hasil);
end;

procedure TfrmTampil.btnByNamaClick(Sender: TObject);
var i, j : integer;
begin
  for i := 0 to length(frmIsiData.ArrGabung)-1 do begin
    for j := 0 to length(frmIsiData.ArrGabung)-2  do begin
      if frmIsiData.ArrGabung[j+1].nama < frmIsiData.ArrGabung[j].nama then begin
        //proses penukaran
        frmIsiData.tampung := frmIsiData.ArrGabung[j];
        frmIsiData.ArrGabung[j] := frmIsiData.ArrGabung[j+1];
        frmIsiData.ArrGabung[j+1] := frmIsiData.tampung;
      end;
    end;
  end;
  ShowMessage(ShowHasilGabung);
end;

procedure TfrmTampil.btnByNilaiClick(Sender: TObject);
var i, j : integer;
begin
  for i := 0 to length(frmIsiData.ArrGabung)-1 do begin
    for j := 0 to length(frmIsiData.ArrGabung)-2  do begin
      if frmIsiData.ArrGabung[j+1].nilai < frmIsiData.ArrGabung[j].nilai then begin
        //proses penukaran
        frmIsiData.tampung := frmIsiData.ArrGabung[j];
        frmIsiData.ArrGabung[j] := frmIsiData.ArrGabung[j+1];
        frmIsiData.ArrGabung[j+1] := frmIsiData.tampung;
      end;
    end;
  end;
  ShowMessage(ShowHasilGabung);
end;

procedure TfrmTampil.btnByTglClick(Sender: TObject);
var i, j : integer;
begin
  for i := 0 to length(frmIsiData.ArrGabung)-1 do begin
    for j := 0 to length(frmIsiData.ArrGabung)-2  do begin
      if frmIsiData.ArrGabung[j+1].tgl < frmIsiData.ArrGabung[j].tgl then begin
        //proses penukaran
        frmIsiData.tampung := frmIsiData.ArrGabung[j];
        frmIsiData.ArrGabung[j] := frmIsiData.ArrGabung[j+1];
        frmIsiData.ArrGabung[j+1] := frmIsiData.tampung;
      end;
    end;
  end;
  ShowMessage(ShowHasilGabung);
end;

function TfrmTampil.ShowHasilGabung: string;
var i : integer;
begin
  Result := '';
  for i := 0 to length(frmIsiData.ArrGabung)-1 do begin
    Result := Result + frmIsiData.ArrGabung[i].nama + ' ' + IntToStr(frmIsiData.ArrGabung[i].nilai) + ' '+ DateToStr(frmIsiData.ArrGabung[i].tgl) + #13;
  end;
end;

end.
