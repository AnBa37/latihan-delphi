object frmInput: TfrmInput
  Left = 500
  Top = 377
  BorderStyle = bsDialog
  Caption = 'Input Awal'
  ClientHeight = 103
  ClientWidth = 169
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object txt1: TAdvEdit
    Left = 8
    Top = 8
    Width = 73
    Height = 21
    EditAlign = eaRight
    EditType = etMoney
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 0
    Text = '0'
    Visible = True
    OnExit = txt1Exit
    Version = '2.9.2.1'
  end
  object txt2: TAdvEdit
    Left = 8
    Top = 40
    Width = 73
    Height = 21
    EditAlign = eaRight
    EditType = etMoney
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 2
    Text = '0'
    Visible = True
    OnExit = txt2Exit
    Version = '2.9.2.1'
  end
  object btnIsi1: TButton
    Left = 88
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Isi'
    TabOrder = 1
    OnClick = btnIsi1Click
  end
  object btnIsi2: TButton
    Left = 88
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Isi'
    TabOrder = 3
    OnClick = btnIsi2Click
  end
  object btnTampil: TButton
    Left = 8
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Tampil'
    TabOrder = 4
    OnClick = btnTampilClick
  end
  object btnTutup: TButton
    Left = 88
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 5
    OnClick = btnTutupClick
  end
end
