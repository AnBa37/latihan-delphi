program Tahap2No3;

uses
  Forms,
  InputAwal in 'InputAwal.pas' {frmInput},
  InputDetail in 'InputDetail.pas' {frmIsiData},
  TampilData in 'TampilData.pas' {frmTampil};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TfrmIsiData, frmIsiData);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.Run;
end.
