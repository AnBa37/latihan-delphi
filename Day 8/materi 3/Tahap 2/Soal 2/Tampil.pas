unit Tampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, InputDetail;

type
  TfrmTampil = class(TForm)
    Label1: TLabel;
    cmbPilihan: TComboBox;
    btnProses: TButton;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnProsesClick(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTampil: TfrmTampil;

implementation

{$R *.dfm}

{ TfrmTampil }

procedure TfrmTampil.Execute;
begin
  InitForm;
  ShowModal;
end;

procedure TfrmTampil.InitForm;
begin
  cmbPilihan.Items.Clear;
  cmbPilihan.Items.Append('Menaik');
  cmbPilihan.Items.Append('Menurun');
  cmbPilihan.ItemIndex := -1; //default tidak ada yang dipilih
end;

procedure TfrmTampil.btnProsesClick(Sender: TObject);
var i, j : integer;                10   7    9   --> 7 10 9 --> 7 9 10
begin
  if cmbPilihan.ItemIndex = -1 then begin //tidak ada yang dipilih
    ShowMessage('Pilih proses...');
    cmbPilihan.SetFocus;
  end else if cmbPilihan.ItemIndex = 0 then begin //nilai menaik
    for i := 0 to length(frmInputDetail.Arr)-1 do begin
      for j := i to length(frmInputDetail.Arr)-2 do begin
        if frmInputDetail.Arr[j+1].nilai < frmInputDetail.Arr[j].nilai then begin  9 < 10
          frmInputDetail.tampung := frmInputDetail.Arr[j];  10 tampung
          frmInputDetail.Arr[j] := frmInputDetail.Arr[j+1];  9 arr[1]
          frmInputDetail.Arr[j+1] := frmInputDetail.tampung; 10 arr[2]
        end;
      end;
    end;
  end else begin //nilai menurun atau itemindex = 1
    for i := 0 to length(frmInputDetail.Arr)-1 do begin
      for j := i to length(frmInputDetail.Arr)-2 do begin
        if frmInputDetail.Arr[j+1].nilai > frmInputDetail.Arr[j].nilai then begin
          frmInputDetail.tampung := frmInputDetail.Arr[j];
          frmInputDetail.Arr[j] := frmInputDetail.Arr[j+1];
          frmInputDetail.Arr[j+1] := frmInputDetail.tampung;
        end;
      end;
    end;
  end;
end;

procedure TfrmTampil.btnTampilClick(Sender: TObject);
var i : integer;
    hasil : string;
begin
  hasil := '';
  for i := 0 to length(frmInputDetail.Arr)-1 do begin
    hasil := hasil + frmInputDetail.Arr[i].nama + ' --> ' + IntToStr(frmInputDetail.Arr[i].nilai)+#13;
  end;
  ShowMessage(hasil);
end;

procedure TfrmTampil.btnTutupClick(Sender: TObject);
begin
  Close;
end;

end.
