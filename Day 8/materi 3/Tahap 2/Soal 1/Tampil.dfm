object frmTampil: TfrmTampil
  Left = 271
  Top = 207
  BorderStyle = bsDialog
  Caption = 'Tampil Data'
  ClientHeight = 266
  ClientWidth = 263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 32
    Height = 13
    Caption = 'Alamat'
  end
  object Label3: TLabel
    Left = 8
    Top = 72
    Width = 41
    Height = 13
    Caption = 'No. Telp'
  end
  object Label4: TLabel
    Left = 8
    Top = 104
    Width = 41
    Height = 13
    Caption = 'Tgl Lahir'
  end
  object Label5: TLabel
    Left = 8
    Top = 136
    Width = 55
    Height = 13
    Caption = 'Keterangan'
  end
  object sttNama: TStaticText
    Left = 74
    Top = 8
    Width = 43
    Height = 17
    Caption = 'sttNama'
    TabOrder = 0
  end
  object sttAlamat: TStaticText
    Left = 74
    Top = 40
    Width = 47
    Height = 17
    Caption = 'sttAlamat'
    TabOrder = 1
  end
  object sttNoTelp: TStaticText
    Left = 74
    Top = 72
    Width = 50
    Height = 17
    Caption = 'sttNoTelp'
    TabOrder = 2
  end
  object sttTglLahir: TStaticText
    Left = 74
    Top = 104
    Width = 53
    Height = 17
    Caption = 'sttTglLahir'
    TabOrder = 3
  end
  object btnTutup: TButton
    Left = 184
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 4
    OnClick = btnTutupClick
  end
  object mmKet: TMemo
    Left = 72
    Top = 136
    Width = 185
    Height = 89
    Lines.Strings = (
      'mmKet')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 5
  end
end
