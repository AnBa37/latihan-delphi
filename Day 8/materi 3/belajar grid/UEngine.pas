unit UEngine;

interface

uses URecord, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, PostgresConnection, adoInt;

function get_data (vkey : integer) : tr_data;
function save_data (data : tr_data) : integer;
procedure Update_Data(Data : TR_Data);
procedure Delete_Data(vkey : integer);
function load_data : ardata;
procedure load_data_2 (var dataKar : ardata);

implementation
var sql : string;

function get_data (vkey : integer) : tr_data;
var buffer : _recordset;
begin
  sql := 'SELECT seq, kode, nama, tglmasuk, gaji, jumanak FROM data '+
         'where seq = '+formatsqlnumber(vkey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    result.seq := BufferToInteger(buffer.Fields[0].Value);
    result.kode := BufferToString(buffer.Fields[1].Value);
    result.nama := BufferToString(buffer.Fields[2].Value);
    result.tglMasuk := BufferToDateTime(buffer.Fields[3].Value);
    result.gaji := BufferToFloat(buffer.Fields[4].Value);
    result.jumAnak := BufferToInteger(buffer.Fields[5].Value);
  end else result.seq := 0;
end;

function save_data (data : tr_data) : integer;
begin
  sql := 'insert into data (seq, kode, nama, tglmasuk, gaji, jumanak) values ' +
          '('+FormatSQLNumber(data.seq)+','+
            FormatSQLString(data.kode)+','+
            FormatSQLString(data.nama)+','+
            FormatSQLDate(data.tglMasuk)+','+
            FormatSQLNumber(data.gaji)+', '+
            FormatSQLNumber(data.jumAnak)+' '+
          ')';
  myConnection.ExecSQL(sqL);
  result := data.seq;
end;

procedure Update_Data(Data : TR_Data);
begin
  sqL := 'UPDATE data SET '+
         'kode = '+FormatSQLString(Data.kode)+', '+
         'nama = '+FormatSQLString(Data.nama)+', '+
         'tglMasuk = '+FormatSQLDate(Data.tglMasuk)+', '+
         'gaji = '+FormatSQLNumber(Data.gaji)+', '+
         'jumAnak = '+FormatSQLNumber(Data.jumAnak)+' '+
         'WHERE seq = '+FormatSQLNumber(Data.Seq);
  myConnection.ExecSQL(sqL);
end;

procedure Delete_Data(vkey : integer);
begin
  sql := 'delete from data where seq = '+formatsqlnumber(vkey);
  myConnection.ExecSQL(sqL);
end;

function load_data : ardata;
var buffer : _recordset; i : integer;
begin
  sql := 'SELECT seq, kode, nama, tglmasuk, gaji, jumanak FROM data ';
  buffer := myConnection.OpenSQL(sql); setlength(result, buffer.recordcount);
  if buffer.RecordCount > 0 then begin
    for i := 0 to buffer.recordcount-1 do begin
      result[i].seq := BufferToInteger(buffer.Fields[0].Value);
      result[i].kode := BufferToString(buffer.Fields[1].Value);
      result[i].nama := BufferToString(buffer.Fields[2].Value);
      result[i].tglMasuk := BufferToDateTime(buffer.Fields[3].Value);
      result[i].gaji := BufferToFloat(buffer.Fields[4].Value);
      result[i].jumAnak := BufferToInteger(buffer.Fields[5].Value);
      buffer.movenext;
    end;
  end else result[0].seq := 0;
  buffer.close;
end;

procedure load_data_2 (var dataKar : ardata);
var buffer : _recordset; i : integer;
begin
  sql := 'SELECT seq, kode, nama, tglmasuk, gaji, jumanak FROM data ';
  buffer := myConnection.OpenSQL(sql); setlength(dataKar, buffer.recordcount);
  if buffer.RecordCount > 0 then begin
    for i := 0 to buffer.recordcount-1 do begin
      dataKar[i].seq := BufferToInteger(buffer.Fields[0].Value);
      dataKar[i].kode := BufferToString(buffer.Fields[1].Value);
      dataKar[i].nama := BufferToString(buffer.Fields[2].Value);
      dataKar[i].tglMasuk := BufferToDateTime(buffer.Fields[3].Value);
      dataKar[i].gaji := BufferToFloat(buffer.Fields[4].Value);
      dataKar[i].jumAnak := BufferToInteger(buffer.Fields[5].Value);
      buffer.movenext;
    end;
  end else dataKar[0].seq := 0;
  buffer.close;
end;

end.
