unit Unit3;

interface

uses   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls;

type
  tr_data = record
    seq : integer;
    kode, nama : string;
    tglMasuk : tdate;
    gaji : real;
    jumAnak : integer;
  end;

  arData = array of tr_data;
  
implementation

end.
