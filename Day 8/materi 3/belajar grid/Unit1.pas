unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls;

type
  TfrmGrid = class(TForm)
    asgData: TAdvStringGrid;
    btnProses: TButton;
    asgTampilData: TAdvStringGrid;
    procedure FormShow(Sender: TObject);
    procedure asgDataCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgDataGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgDataGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgDataCellValidate(Sender: TObject; Col, Row: Integer;
      var Value: String; var Valid: Boolean);
    procedure btnProsesClick(Sender: TObject);
    procedure asgTampilDataGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
  private
    { Private declarations }
    procedure setgrid (idx : integer = 0);
    procedure Arrangecolsize (idx : integer = 0);
  public
    { Public declarations }
    procedure execute;
  end;

var
  frmGrid: TfrmGrid;

implementation

uses unit3;

const
  colNo = 0;
  colKode = 1;
  colNama = 2;
  colTglMasuk = 3;
  colGaji  = 4;
  colJumAnak = 5;

{$R *.dfm}

procedure TfrmGrid.Arrangecolsize;
begin
  if (idx = 0) or (idx = 1) then begin
    asgData.AutoNumberCol(colNo);
    asgData.AutoSizeColumns(true, 4);
  end;

  if (idx = 0) or (idx = 2) then begin
    asgTampilData.cells[coltglmasuk, asgTampilData.RowCount-1] := 'Total';
    asgTampilData.AutoNumberCol(colNo);
    asgTampilData.FloatingFooter.ColumnCalc[colgaji] := acsum;
    asgTampilData.FloatingFooter.ColumnCalc[coljumAnak] := acsum;
    asgTampilData.AutoSizeColumns(true, 4);
  end;
end;

procedure TfrmGrid.execute;
begin
  setgrid;
end;

procedure TfrmGrid.setgrid (idx : integer = 0);
begin
  if (idx = 0) or (idx = 1) then begin
    asgData.RowCount := 2;
    asgData.ColCount := 7;
    asgData.FixedCols := 1;
    asgData.FixedRows := 1;
    asgData.ClearNormalCells;
  end;

  if (idx = 0) or (idx = 2) then begin
    asgTampilData.RowCount := 4;
    asgTampilData.ColCount := 7;
    asgTampilData.FixedCols := 1;
    asgTampilData.FixedRows := 2;
    asgTampilData.ClearNormalCells;

    asgTampilData.MergeCells(colNo,0,1,2);
    asgTampilData.Cells[colNo, 0] := 'No.';
    asgTampilData.MergeCells(colTglMasuk,0,1,2);
    asgTampilData.Cells[coltglmasuk, 0] := 'Tanggal Masuk';
    asgTampilData.MergeCells(colGaji,0,1,2);
    asgTampilData.Cells[colGaji, 0] := 'Gaji';
    asgTampilData.MergeCells(colJumAnak,0,1,2);
    asgTampilData.Cells[colJumAnak, 0] := 'Jum Anak';

    asgTampilData.MergeCells(colKode, 0, 2, 1);
    asgTampilData.Cells[colKode, 0] := 'Pegawai';
    asgTampilData.Cells[colKode, 1] := 'Kode';
    asgTampilData.Cells[colNama, 1] := 'Nama';

    asgTampilData.MergeCells(colJumAnak+1,0,1,2); 
  end;
  arrangecolsize (idx);
end;

procedure TfrmGrid.FormShow(Sender: TObject);
begin
  execute;
end;

procedure TfrmGrid.asgDataCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  if acol in [colkode, colnama, coltglmasuk, colgaji, coljumanak] then canedit := true
  else canedit := false;
end;

procedure TfrmGrid.asgDataGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then HAlign := taCenter
  else if acol in [colNo, colGaji, coljumAnak] then halign := taRightJustify;
  valign := vtacenter;
end;

procedure TfrmGrid.asgDataGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case acol of
    coltglmasuk : aeditor := edDateEdit;
    colGaji : AEditor := edFloat;
    colJumAnak : aeditor := edPositiveNumeric;
  end;
end;

procedure TfrmGrid.asgDataCellValidate(Sender: TObject; Col, Row: Integer;
  var Value: String; var Valid: Boolean);
begin
  case col of
    colJumAnak : begin
      if asgData.Ints[Col, row] > 2 then
        asgData.Floats[colgaji, row] := 6000000
      else
        asgData.Floats[colgaji, row] := 3500000
    end;
  end;
  arrangecolsize;
end;

procedure TfrmGrid.btnProsesClick(Sender: TObject);
var arDataKaryawan : arData;
    i, row : integer;
begin
  SetLength(ardatakaryawan, asgData.RowCount-1);
  for i := 1 to asgData.RowCount-1 do begin  //isi dari grid
    arDataKaryawan[i-1].kode := asgData.Cells[colkode, i];
    arDataKaryawan[i-1].nama := asgData.Cells[colnama, i];
    arDataKaryawan[i-1].tglMasuk := asgData.dates[coltglmasuk, i];
    arDataKaryawan[i-1].gaji := asgData.floats[colgaji, i];
    arDataKaryawan[i-1].jumAnak := asgData.ints[coljumAnak, i];
  end;

  for i := 0 to length(arDataKaryawan)-1 do begin
    if i > 0 then asgTampilData.addrow;
    row := asgtampildata.rowcount-2;
    asgTampilData.Cells[colkode, row] := arDataKaryawan[i].kode;
    asgTampilData.Cells[colnama, row] := arDataKaryawan[i].nama;
    asgTampilData.dates[coltglmasuk, row] := arDataKaryawan[i].tglMasuk;
    asgTampilData.floats[colgaji, row] := arDataKaryawan[i].gaji;
    asgTampilData.ints[coljumanak, row] := arDataKaryawan[i].jumAnak;
  end;
   arrangecolsize(2);
end;

procedure TfrmGrid.asgTampilDataGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow <= 1 then HAlign := taCenter
  else if acol in [colNo, colGaji, coljumAnak] then halign := taRightJustify;
  valign := vtacenter;
end;

end.
