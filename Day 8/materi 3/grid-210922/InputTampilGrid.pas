unit InputTampilGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, ExtCtrls;

type
  tr_data = record
    kode : string[5];
    nama : string[50];
    tglMasuk : tdate;
    gaji : real;
    jumAnak: integer;
    tunjangan : string[1];
    jk : string[1];
  end;

  arData = array of tr_data;

  TfrmInputTampilGrid = class(TForm)
    MainPanel: TPanel;
    gbxInputData: TGroupBox;
    asgInputData: TAdvStringGrid;
    btnProses: TButton;
    gbxTampilData: TGroupBox;
    asgTampilData: TAdvStringGrid;
    btnTampil: TButton;
    btnNode: TButton;
    procedure asgInputDataGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure FormShow(Sender: TObject);
    procedure asgInputDataCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputDataGetEditorType(Sender: TObject; ACol,
      ARow: Integer; var AEditor: TEditorType);
    procedure asgInputDataCellValidate(Sender: TObject; Col, Row: Integer;
      var Value: String; var Valid: Boolean);
    procedure asgInputDataAutoAddRow(Sender: TObject; ARow: Integer);
    procedure btnProsesClick(Sender: TObject);
    procedure asgTampilDataGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnTampilClick(Sender: TObject);
    procedure btnNodeClick(Sender: TObject);
  private
    { Private declarations }
    arKaryawan, arKaryCeklis : arData;
    procedure initform;
    procedure setgrid (idx : integer = 0);
    procedure arrangeColsize (idx : integer = 0);
  public
    { Public declarations }
  end;

var
  frmInputTampilGrid: TfrmInputTampilGrid;

implementation

{$R *.dfm}

const
  colNo       = 0;
  colKode     = 1;
  colNama     = 2;
  colTglMasuk = 3;
  colGaji     = 4;
  colJumAnak  = 5;
  colTunjangan = 6;
  ColJK        = 7;

  colTotGaji = 6;

{ TfrmInputTampilGrid }

procedure TfrmInputTampilGrid.arrangeColsize(idx : integer = 0);
begin
  if (idx = 0) or (idx = 1) then begin
    asgInputData.AutoNumberCol(colNo);
    asgInputData.AutoSizeColumns(true,4);
  end;

  if (idx = 0) or (idx = 2) then begin
    asgTampilData.FloatingFooter.ColumnCalc[colgaji] := acsum;
    asgTampilData.FloatingFooter.ColumnCalc[colJumAnak] := acsum;
    asgTampilData.FloatingFooter.ColumnCalc[colTotGaji] := acsum;
    asgTampilData.cells[colTglMasuk, asgTampilData.RowCount-1] := 'Total';
    asgTampilData.AutoNumberCol(colNo);
    asgTampilData.AutoSizeColumns(true,4);
  end;
end;

procedure TfrmInputTampilGrid.setgrid (idx : integer = 0);
begin
  if (idx = 0) or (idx = 1) then begin
    asgInputData.ClearNormalCells;
    asgInputData.ColCount := 9;//jumlah kolom yg dibutuhkan + 1
    asgInputData.RowCount := 2;//header + 1 baris kosong
    asgInputData.FixedCols := 1;
    asgInputData.FixedRows := 1;
    asgInputData.AddCheckBox(colTunjangan, 1, false, false);
  end;

  if (idx = 0) or (idx = 2) then begin
    asgTampilData.clearnormalcells;
    asgTampilData.ColCount := 9;//jumlah kolom yg dibutuhkan + 1
    asgTampilData.RowCount := 4;//2header + 1 baris kosong + floatingfooter
    asgTampilData.FixedCols := 1;
    asgTampilData.FixedRows := 2;

    asgTampilData.MergeCells(colNo, 0, 1, 2);
    asgTampilData.cells[colNo, 0] := 'No.';
    asgTampilData.MergeCells(colKode, 0, 2, 1);
    asgTampilData.cells[colKode, 0] := 'Pegawai';
    asgTampilData.cells[colKode, 1] := 'Kode';
    asgTampilData.cells[colNama, 1] := 'Nama';
    asgTampilData.MergeCells(colTglMasuk, 0, 1, 2);
    asgTampilData.cells[colTglMasuk, 0] := 'Tgl Masuk';
    asgTampilData.MergeCells(colGaji, 0, 1, 2);
    asgTampilData.cells[colGaji, 0] := 'Gaji';
    asgTampilData.MergeCells(colJumAnak, 0, 1, 2);
    asgTampilData.cells[colJumAnak, 0] := 'Jum. Anak';
    asgTampilData.MergeCells(colTotGaji, 0, 1, 2);
    asgTampilData.cells[colTotGaji, 0] := 'Total Gaji';
    asgTampilData.MergeCells(colJK, 0, 1, 2);
    asgTampilData.cells[colJK, 0] := 'Gender';
    asgTampilData.MergeCells(ColJK+1, 0, 1, 2);
  end;

  arrangecolsize(idx);
end;

procedure TfrmInputTampilGrid.asgInputDataGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (acol = colTunjangan) or (arow = 0) then HAlign := taCenter
  else if acol in [colNo, colJumAnak, colGaji] then halign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInputTampilGrid.initform;
begin
  setgrid;
end;

procedure TfrmInputTampilGrid.FormShow(Sender: TObject);
begin
  initform;
end;

procedure TfrmInputTampilGrid.asgInputDataCanEditCell(Sender: TObject;
  ARow, ACol: Integer; var CanEdit: Boolean);
begin
  if acol in [colkode..coljk] then canedit := true
  else canedit := false;
end;

procedure TfrmInputTampilGrid.asgInputDataGetEditorType(Sender: TObject;
  ACol, ARow: Integer; var AEditor: TEditorType);
begin
  case acol of
    colKode : aeditor := eduppercase;
    colNama : AEditor := edMixedCase;
    colTglMasuk : AEditor := edDateEdit;
    colGaji : AEditor := edFloat;
    colJumAnak : AEditor := edPositiveNumeric;
    colTunjangan : AEditor := edCheckBox;
    ColJK : begin
      AEditor := edComboList;
      asgInputData.Combobox.Items.Clear;
      asgInputData.Combobox.Items.Add('L');
      asgInputData.Combobox.Items.Add('P');
    end;
  end;
end;

procedure TfrmInputTampilGrid.asgInputDataCellValidate(Sender: TObject;
  Col, Row: Integer; var Value: String; var Valid: Boolean);
begin
  if col = colJumAnak then begin
    if asgInputData.Ints[Col, row] > 0 then
      asgInputData.SetCheckBoxState(colTunjangan, row, true)
    else
      asgInputData.SetCheckBoxState(colTunjangan, row, false)
  end;

  arrangeColsize;
end;

procedure TfrmInputTampilGrid.asgInputDataAutoAddRow(Sender: TObject;
  ARow: Integer);
begin
  asgInputData.AddCheckBox(colTunjangan, arow, false, false);
end;

procedure TfrmInputTampilGrid.btnProsesClick(Sender: TObject);
var i, idx : integer; status : boolean; hasil : string;
begin
  setlength(arKaryawan, 0);
  setlength(arKaryCeklis, 0);
  idx := 0;
  //proses isi array dari grid
  for i := 1 to asgInputData.RowCount-1 do begin
    setlength(arKaryawan, i);
    arKaryawan[i-1].kode := asgInputData.cells[colKode, i];
    arKaryawan[i-1].Nama := asgInputData.cells[colNama, i];
    arKaryawan[i-1].TglMasuk := asgInputData.dates[colTglMasuk, i];
    arKaryawan[i-1].Gaji := asgInputData.floats[colGaji, i];
    arKaryawan[i-1].JumAnak := asgInputData.ints[colJumanak, i];
    arKaryawan[i-1].JK := asgInputData.cells[colJK, i];

    asgInputData.GetCheckBoxState(colTunjangan, i, status);
    if status = true then
      arKaryawan[i-1].Tunjangan := 'T'
    else
      arKaryawan[i-1].Tunjangan := 'F';

    if status = true then begin
      setlength(arKaryCeklis, idx+1);
      arKaryCeklis[idx].kode := asgInputData.cells[colKode, i];
      arKaryCeklis[idx].Nama := asgInputData.cells[colNama, i];
      arKaryCeklis[idx].TglMasuk := asgInputData.dates[colTglMasuk, i];
      arKaryCeklis[idx].Gaji := asgInputData.floats[colGaji, i];
      arKaryCeklis[idx].JumAnak := asgInputData.ints[colJumanak, i];
      arKaryCeklis[idx].JK := asgInputData.cells[colJK, i];
      arKaryCeklis[idx].Tunjangan := 'T';

      hasil :=  hasil +#13+arKaryCeklis[idx].kode+' '+arKaryCeklis[idx].Nama+' '+datetostr(arKaryCeklis[idx].TglMasuk)+' '+
                    floattostr(arKaryCeklis[idx].Gaji)+' '+inttostr(arKaryCeklis[idx].JumAnak)+' '+arKaryCeklis[idx].Tunjangan+' '+
                    arKaryCeklis[idx].JK;
      idx := idx + 1;
    end;
  end;
  ShowMessage(hasil);
end;

procedure TfrmInputTampilGrid.asgTampilDataGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (arow < 2) then HAlign := taCenter
  else if acol in [colNo, colJumAnak, colGaji, coltotGaji] then halign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInputTampilGrid.btnTampilClick(Sender: TObject);
var i, row : integer;
begin
  if length(arKaryawan) = 0 then begin
    showmessage('Belum ada data.');
    exit;
  end;

  setgrid(2);
  for i := 0 to length(arKaryawan)-1 do begin
    if i > 0 then asgTampilData.AddRow;
    row := asgTampilData.rowcount-2;
    asgTampilData.Cells[colKode, row] := arKaryawan[i].kode;
    asgTampilData.Cells[colNama, row] := arKaryawan[i].nama;
    asgTampilData.Dates[colTglMasuk, row] := arKaryawan[i].tglMasuk;
    asgTampilData.Floats[colGaji, row] := arKaryawan[i].gaji;
    asgTampilData.Ints[colJumAnak, row] := arKaryawan[i].jumAnak;

    if (arKaryawan[i].jumAnak <= 2) and (arKaryawan[i].jumAnak <> 0) then
      asgTampilData.Floats[colTotGaji, row] := arKaryawan[i].gaji * arKaryawan[i].jumAnak
    else if arKaryawan[i].jumAnak > 2 then
      asgTampilData.Floats[colTotGaji, row] := arKaryawan[i].gaji * 4
    else
      asgTampilData.Floats[colTotGaji, row] := arKaryawan[i].gaji;

    if arKaryawan[i].jk = 'L' then
      asgTampilData.Cells[colJK, row] := 'Laki-laki'
    else
      asgTampilData.Cells[colJK, row] := 'Perempuan';
  end;
  arrangecolsize(2);
end;

procedure TfrmInputTampilGrid.btnNodeClick(Sender: TObject);
var i, row, row2, j : integer;
begin
  if length(arKaryawan) = 0 then begin
    showmessage('Belum ada data.');
    exit;
  end;

  setgrid(2);  asgTampilData.FixedCols := 0;
  for i := 0 to length(arKaryawan)-1 do begin
    if i > 0 then asgTampilData.AddRow;
    row := asgTampilData.rowcount-2;
    asgTampilData.Cells[colKode, row] := arKaryawan[i].kode;
    asgTampilData.Cells[colNama, row] := arKaryawan[i].nama;
    asgTampilData.Dates[colTglMasuk, row] := arKaryawan[i].tglMasuk;
    asgTampilData.Floats[colGaji, row] := arKaryawan[i].gaji;
    asgTampilData.Ints[colJumAnak, row] := arKaryawan[i].jumAnak;

    if (arKaryawan[i].jumAnak <= 2) and (arKaryawan[i].jumAnak <> 0) then
      asgTampilData.Floats[colTotGaji, row] := arKaryawan[i].gaji * arKaryawan[i].jumAnak
    else if arKaryawan[i].jumAnak > 2 then
      asgTampilData.Floats[colTotGaji, row] := arKaryawan[i].gaji * 4
    else
      asgTampilData.Floats[colTotGaji, row] := arKaryawan[i].gaji;

    if arKaryawan[i].jk = 'L' then
      asgTampilData.Cells[colJK, row] := 'Laki-laki'
    else
      asgTampilData.Cells[colJK, row] := 'Perempuan';

    if arKaryawan[i].jumAnak > 0 then begin
      for j := 1 to arKaryawan[i].jumAnak do begin
        asgTampilData.AddRow;
        row2 := asgTampilData.rowcount-2;
        asgTampilData.Cells[colNama, row2] := 'data anak ke - '+inttostr(j);
      end;
      row2 := asgTampilData.rowcount-2;
      if row <> row2 then asgTampilData.AddNode(row, row2-row+1);
    end;
  end;

  asgTampilData.FloatingFooter.ColumnCalc[colgaji] := acsum;
  asgTampilData.FloatingFooter.ColumnCalc[colJumAnak] := acsum;
  asgTampilData.FloatingFooter.ColumnCalc[colTotGaji] := acsum;
  asgTampilData.cells[colTglMasuk, asgTampilData.RowCount-1] := 'Total';
//  asgTampilData.ContractAll;
  asgTampilData.AutoSizeColumns(true,4);
end;

end.
