unit Tampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmTampil = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    sttNama: TStaticText;
    sttAlamat: TStaticText;
    sttNoTelp: TStaticText;
    sttTglLahir: TStaticText;
    btnTutup: TButton;
    mmKet: TMemo;
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTampil: TfrmTampil;

implementation

uses Input;

{$R *.dfm}

{ TfrmTampil }

procedure TfrmTampil.Execute;
begin
  InitForm;
  //isi dari RECORD
  sttNama.Caption := frmInput.data.nama;
  sttAlamat.Caption := frmInput.data.alamat;
  sttNoTelp.Caption := frmInput.data.no_telp;
  sttTglLahir.Caption := DateToStr(frmInput.data.tgl_lahir);
//  sttTglLahir.Caption := FormatDateTime('dd MMM yyyy', frmInput.data.tgl_lahir);
  mmKet.Lines.Text := frmInput.data.keterangan;
  ShowModal;
end;

procedure TfrmTampil.InitForm;
begin
  sttNama.Caption := '';
  sttAlamat.Caption := '';
  sttNoTelp.Caption := '';
  sttTglLahir.Caption := '';
  mmKet.Lines.Text := '';
end;

procedure TfrmTampil.btnTutupClick(Sender: TObject);
begin
  Close;
end;

end.
