unit InputData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmInputData = class(TForm)
    PanelInputData: TPanel;
    lblKode: TLabel;
    lblNama: TLabel;
    lblHargaBeli: TLabel;
    lblHargaJual: TLabel;
    Label5: TLabel;
    lblKategori: TLabel;
    txtKode: TAdvEdit;
    txtNama: TAdvEdit;
    txtHargaBeli: TAdvEdit;
    txtHargaJual: TAdvEdit;
    mmKeterangan: TMemo;
    cmbKategori: TComboBox;
    btnSimpan: TButton;
    btnBatal: TButton;
    procedure btnSimpanClick(Sender: TObject);
    procedure btnBatalClick(Sender: TObject);
  private
    { Private declarations }
    idx : String;
    EditMode, DetailMode : Boolean;
    procedure InitForm;
    procedure LoadData;
  public
    { Public declarations }
    //ArrData : tr_data;
    procedure Execute(kode : string; isEdit, isDetail : Boolean);
  end;

var
  frmInputData: TfrmInputData;

implementation
uses UEngine, URecord, PostgresConnection, PengolahanData;

{$R *.dfm}



{ TfrmInputData }

procedure TfrmInputData.Execute(kode: string; isEdit, isDetail: Boolean);
begin
  idx := kode;
  EditMode := isEdit;
  DetailMode := isDetail;
  if EditMode or DetailMode then LoadData;
  //if EditMode then frmInputData.Caption := 'Edit Data'
  //else if DetailMode then frmInputData.Caption := 'Detail Data'
  //else frmPengolahanbarang.Caption := 'Input Data';
  //frmInputDetail.Position := poDesktopCenter;
  ShowModal;
end;

procedure TfrmInputData.InitForm;
begin
  txtKode.Text := '';
  txtNama.Text := '';
  txtHargaBeli.FloatValue := 0;
  txtHargaJual.FloatValue := 0;
  mmKeterangan.Lines.Text := '';
  cmbKategori.ItemIndex := -1;

end;

procedure TfrmInputData.btnSimpanClick(Sender: TObject);
 var Data : TR_Data;
begin
 if DetailMode and not EditMode then Close
  else begin //isi ke array data
    if txtKode.Text = '' then begin
      ShowMessage('Kode tidak boleh kosong.');
      txtKode.SetFocus;
      exit; //supaya tidak keluar dari form input dan tidak masuk ke array
     end else if txtNama.Text = '' then begin
      ShowMessage('Nama tidak boleh kosong.');
      txtNama.SetFocus;
      exit;
    end else if txtHargaBeli.FloatValue = 0 then begin
      ShowMessage('Harga beli tidak boleh kosong.');
      txtHargaBeli.SetFocus;
      exit;
    end else if txtHargaJual.FloatValue = 0 then begin
      ShowMessage('Harga Jual tidak boleh kosong .');
      txtHargaJual.SetFocus;
      exit;
    end;
     //save ke database
    myConnection.BeginSQL;
    try
      Data.kode := idx;
      Data.nama := txtNama.Text;
      Data.harga_beli := txtHargaBeli.FloatValue;
      Data.harga_jual := txtHargaJual.FloatValue;
      Data.keterangan := mmKeterangan.Lines.Text;
      //Data.kategori := cmbKategori.ItemIndex;
      if not EditMode then Insert_Data(Data)
      else Update_Data(Data);
      myConnection.EndSQL;
    except
      myConnection.UndoSQL;
      ShowMessage('SAVE GAGAL.');
    end;

   close;
  end;
end;

procedure TfrmInputData.LoadData;
var Data : TR_Data;
begin
  Select_Data(idx, Data);
  txtNama.Text := Data.nama;
  txtHargaBeli.FloatValue := Data.Harga_Beli;
  txtHargaJual.FloatValue := Data.Harga_Jual;
  if Data.kategori = 'Bahan Baku' then cmbkategori.ItemIndex := 0
  else if Data.kategori = 'Bahan 1/2 Jadi' then cmbKategori.ItemIndex := 1
  else if Data.kategori = 'Bahan Jadi' then cmbKategori.ItemIndex := 2;

end;

procedure TfrmInputData.btnBatalClick(Sender: TObject);
begin
close;
frmPengolahanBarang.execute;
end;

end.
