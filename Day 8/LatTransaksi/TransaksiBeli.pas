unit TransaksiBeli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, ExtCtrls;

type
  TfrmTransaksiBeli = class(TForm)
    PanelTransaksiBeli: TPanel;
    grbxFilterBarang: TGroupBox;
    lblKode: TLabel;
    lblNama: TLabel;
    cmbKode: TComboBox;
    cmbNama: TComboBox;
    btnLoadData: TButton;
    btnReset: TButton;
    btnTambah: TButton;
    btnEdit: TButton;
    btnHapus: TButton;
    btnDetail: TButton;
    asgTransaksiBeli: TAdvStringGrid;
    procedure btnTambahClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmTransaksiBeli: TfrmTransaksiBeli;

implementation
uses PengolahanData

{$R *.dfm}

procedure TfrmTransaksiBeli.InitForm;
begin
 cmbKode.ItemIndex := 0;
 cmdNama.ItemIndex := 0;
end;

procedure TfrmTransaksiBeli.btnTambahClick(Sender: TObject);
begin
frmTambahTransaksiBeli.execute;
end;

end.
