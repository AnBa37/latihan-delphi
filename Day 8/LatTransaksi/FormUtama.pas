unit FormUtama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmFormUtama = class(TForm)
    MainPanel: TPanel;
    btnMasterBarang: TButton;
    btnTransaksiBeli: TButton;
    btnLaporanPembelian: TButton;
    btnTutup: TButton;
    procedure btnLaporanPembelianClick(Sender: TObject);
    procedure btnTransaksiBeliClick(Sender: TObject);
    procedure btnMasterBarangClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmFormUtama: TfrmFormUtama;

implementation
uses PengolahanData, TransaksiBeli;

{$R *.dfm}

procedure btnMasterBarangClick(Sender: TObject)
begin
 frmPengolahanbarang.Execute;
end;

end.
