unit PengolahanData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, ExtCtrls;

type
  Tfrmpengolahanbarang = class(TForm)
    PanelPenolaanBar: TPanel;
    btnTambah: TButton;
    btnEdit: TButton;
    btnHapus: TButton;
    btnTutup: TButton;
    asgPengolahanBarang: TAdvStringGrid;
    procedure btnTambahClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure asgPengolahanBarangGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgPengolahanBarangExpandNode(Sender: TObject; ARow,
      ARowreal: Integer);
    procedure asgPengolahanBarangContractNode(Sender: TObject; ARow,
      ARowreal: Integer);
  private
    { Private declarations }
    ArrIsi : array of string;
    procedure InitForm;
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
    procedure LoadNoArrDept;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmpengolahanbarang: Tfrmpengolahanbarang;

implementation
uses FormUtama, InputData;

{$R *.dfm}

Const
  ColNode = 0;
  ColKode = 1;
  ColNama = 2;
  ColHarjaBeli = 3;
  ColHargaJual = 4;
  ColKeterangan = 5;

procedure TfrmPengolahanbarang.ArrangeColSize;
begin
  asgPengolahanbarang.AutoSizeColumns(True, 2);
end;

procedure TfrmPengolahanbarang.SetGrid;
begin
  asgPengolahanBarang.ClearNormalCells;
  asgPengolahanBarang.RowCount := 5;
  asgPengolahanBarang.ColCount := 5;
  asgPengolahanBarang.FixedCols := 0;
  asgPengolahanBarang.FixedRows := 1;
  ArrangeColSize;
end;

procedure Tfrmpengolahanbarang.btnTutupClick(Sender: TObject);
begin
 Close;
 frmFormUtama.show;
end;

procedure btnTambahClick(Sender: TObject);
begin
 frmInputData.Execute;
end;

procedure Tfrmpengolahanbarang.asgPengolahanBarangGetAlignment(
  Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment;
  var VAlign: TVAlignment);
begin
   if ARow = 0 then HAlign := taCenter;
  //else if ACol in [ColId, ColGaji] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure Tfrmpengolahanbarang.asgPengolahanBarangExpandNode(
  Sender: TObject; ARow, ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure Tfrmpengolahanbarang.asgPengolahanBarangContractNode(
  Sender: TObject; ARow, ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure Tfrmpengolahanbarang.Execute;
begin

end;

procedure Tfrmpengolahanbarang.InitForm;
begin

end;

procedure Tfrmpengolahanbarang.LoadData;
begin

end;

procedure Tfrmpengolahanbarang.LoadNoArrDept;
begin

end;

end.
