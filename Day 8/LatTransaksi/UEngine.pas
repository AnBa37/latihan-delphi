unit UEngine;

interface
uses ADOInt, URecord, StrUtils, DateUtils, Math, PostgresConnection;

procedure Insert_Data(Data : TR_Data);
procedure Update_Data(Data : TR_Data);
procedure Delete_Data(kode : string);
procedure Select_Data(kode : string; var Data : TR_Data);
function Load_Data_Base : Arr_Data;
//function Get_Data(kode : string) : TR_Data;

implementation
var sqL : string;

procedure Insert_Data(Data : TR_Data);
begin
  sqL := 'INSERT INTO data (kode, nama, harga_beli, harga_jual, Keterangan, Kategori) '+
         'VALUES ('+FormatSQLString(Data.kode)+', '+
                    FormatSQLString(Data.nama)+', '+
                    FormatSQLNumber(Data.harga_beli)+', '+
                    FormatSQLNumber(Data.harga_jual)+','+
                    FormatSQLString(Data.Keterangan)+','+
                    FormatSQLString(Data.Kategori)+')';
  myConnection.ExecSQL(sqL);
end;

procedure Update_Data(Data : TR_Data);
begin
  sqL := 'UPDATE data SET '+
            'nama = '+FormatSQLString(Data.nama)+', '+
            'Harga Beli = '+FormatSQLNumber(Data.harga_beli)+', '+
            'Harga Jual = '+FormatSQLNumber(Data.harga_jual)+' '+
            'Keterangan = '+FormatSQLString(Data.Keterangan)+' '+
            'Kategori = '+FormatSQLString(Data.Kategori)+' '+
            'WHERE kode = '+FormatSQLString(Data.Kode);
  myConnection.ExecSQL(sqL);
end;

procedure Delete_Data(kode : string);
begin
  sqL := 'DELETE FROM data WHERE kode = '+FormatSQLString(kode);
  myConnection.ExecSQL(sqL);
end;

procedure Select_Data(kode : string; var Data : TR_Data);
var buffer : _Recordset;
begin

  sqL := 'SELECT kode, nama, harga_beli, harga_jual, Keterangan, Kategori FROM data WHERE id = '+FormatSQLString(kode);
  buffer := myConnection.OpenSQL(sqL);
  Data.kode := BufferToString(buffer.Fields[0].Value);
  Data.nama := BufferToString(buffer.Fields[1].Value);
  Data.harga_beli := BufferToFloat(buffer.Fields[2].Value);
  Data.harga_jual := BufferToFloat(buffer.Fields[3].Value);
  Data.keterangan := BufferToString(buffer.Fields[4].Value);
  Data.kategori := BufferToString(buffer.Fields[5].Value);
  buffer.Close;
end;

function Load_Data_Base : Arr_Data;
var buffer : _Recordset;
    i : integer;
begin

  sqL := 'SELECT kode, nama, harga_beli, harga_jual, Keterangan, Kategori FROM data ORDER BY nama';
  buffer := myConnection.OpenSQL(sqL);
  setlength(Result, buffer.RecordCount);

  for i := 0 to buffer.RecordCount-1 do begin
    Result[i].kode := BufferToString(buffer.Fields[0].Value);
    Result[i].nama := BufferToString(buffer.Fields[1].Value);
    Result[i].harga_beli := BufferToFloat(buffer.Fields[2].Value);
    Result[i].harga_jual := BufferToFloat(buffer.Fields[3].Value);
    Result[i].keterangan := BufferToString(buffer.Fields[4].Value);
    Result[i].kategori := BufferToString(buffer.Fields[5].Value);
    buffer.MoveNext;

  end;
  buffer.Close;
end;

end.
