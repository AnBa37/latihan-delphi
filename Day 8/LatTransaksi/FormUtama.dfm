object frmFormUtama: TfrmFormUtama
  Left = 292
  Top = 291
  Width = 256
  Height = 245
  Caption = 'Form Utama'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 240
    Height = 206
    Align = alClient
    TabOrder = 0
    object btnMasterBarang: TButton
      Left = 16
      Top = 32
      Width = 209
      Height = 25
      Caption = 'Master Barang'
      TabOrder = 0
      OnClick = btnMasterBarangClick
    end
    object btnTransaksiBeli: TButton
      Left = 16
      Top = 72
      Width = 209
      Height = 25
      Caption = 'Transaksi Beli'
      TabOrder = 1
      OnClick = btnTransaksiBeliClick
    end
    object btnLaporanPembelian: TButton
      Left = 16
      Top = 112
      Width = 209
      Height = 25
      Caption = 'Laporan Pembelian'
      TabOrder = 2
      OnClick = btnLaporanPembelianClick
    end
    object btnTutup: TButton
      Left = 16
      Top = 152
      Width = 209
      Height = 25
      Caption = 'Tutup'
      TabOrder = 3
    end
  end
end
