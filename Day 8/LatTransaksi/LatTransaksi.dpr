program LatTransaksi;

uses
  Forms,
  FormUtama in 'FormUtama.pas' {frmFormUtama},
  PengolahanData in 'PengolahanData.pas' {frmpengolahanbarang},
  InputData in 'InputData.pas' {frmInputData},
  TransaksiBeli in 'TransaksiBeli.pas' {frmTransaksiBeli},
  PostgresConnection in 'PostgresConnection.pas' {myConnection: TDataModule},
  UEngine in 'UEngine.pas',
  TambahTransaksiBeli in 'TambahTransaksiBeli.pas' {frmTambahTransBeli},
  URecord in 'URecord.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmFormUtama, frmFormUtama);
  Application.CreateForm(Tfrmpengolahanbarang, frmpengolahanbarang);
  Application.CreateForm(TfrmInputData, frmInputData);
  Application.CreateForm(TfrmTransaksiBeli, frmTransaksiBeli);
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(TfrmTambahTransBeli, frmTambahTransBeli);
  Application.Run;
end.
