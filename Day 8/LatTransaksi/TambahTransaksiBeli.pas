unit TambahTransaksiBeli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, AdvEdit, ComCtrls;

type
  TfrmTambahTransBeli = class(TForm)
    dtpTanggal: TDateTimePicker;
    txtNoBon: TAdvEdit;
    lblTanggal: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnHapusBon: TButton;
    btnSimpan: TButton;
    BtnBatal: TButton;
    mmKeteranganTambah: TMemo;
    asgTambahTransBeli: TAdvStringGrid;
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmTambahTransBeli: TfrmTambahTransBeli;

implementation

{$R *.dfm}

procedure TfrmTambahTransBeli.InitForm;
begin
 dtpTanggal.Date := now;
 txtNoBon.Text := '';
 mmKeteranganTambah := '';
end;

end.
