unit InputData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, URecord, UEngine, PostgresConnection,
  AdvObj;

type
  TfrmInput = class(TForm)
    asgDaftar: TAdvStringGrid;
    btnPindah: TButton;
    btnPindahUrut: TButton;
    btnReport: TButton;
    btnSave: TButton;
    procedure asgDaftarCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgDaftarGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgDaftarAutoAddRow(Sender: TObject; ARow: Integer);
    procedure asgDaftarGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgDaftarCellValidate(Sender: TObject; Col, Row: Integer;
      var Value: String; var Valid: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnPindahClick(Sender: TObject);
    procedure btnPindahUrutClick(Sender: TObject);
    procedure btnReportClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetGrid;
    procedure ArrangeColSize;
  public
    { Public declarations }
    Arr : Arr_Data;
    procedure Execute;
  end;

var
  frmInput: TfrmInput;

implementation

uses PindahData, ReportData;

{$R *.dfm}
Const
  ColNo = 0;
  ColKode = 1;
  ColNama = 2;
  ColTgl = 3;
  ColHarga = 4;
  ColStokSkr = 5;
  ColTotal = 6;
  ColSetan = 7;
  ColStok = 8;

{ TfrmInput }

procedure TfrmInput.ArrangeColSize;
begin
  asgDaftar.AutoNumberCol(ColNo);
  asgDaftar.ClearRows(asgDaftar.RowCount-1, 1);
  asgDaftar.Cells[ColHarga, asgDaftar.RowCount-1] := 'Total : ';
  asgDaftar.FloatingFooter.ColumnCalc[ColStokSkr] := acSUM;
  asgDaftar.FloatingFooter.ColumnCalc[ColTotal] := acSUM;
  asgDaftar.AutoSizeColumns(True, 2);
  asgDaftar.ColWidths[ColSetan] := 0;
end;

procedure TfrmInput.Execute;
begin
  setlength(Arr, 0);
  SetGrid;
end;

procedure TfrmInput.SetGrid;
begin
  asgDaftar.ClearNormalCells;
  asgDaftar.ClearRows(asgDaftar.RowCount-1, 1);
  asgDaftar.ColCount := 10;
  asgDaftar.RowCount := 3;
  asgDaftar.FixedCols := 1;
  asgDaftar.FixedRows := 1;
  asgDaftar.AddCheckBox(ColStok, 1, False, False);
  ArrangeColSize;
end;

procedure TfrmInput.asgDaftarCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  CanEdit := ACol in [ColKode, ColNama, ColTgl, ColHarga, ColStokSkr, ColStok];
end;

procedure TfrmInput.asgDaftarGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case ACol of
    ColKode : AEditor := edUpperCase;
    ColTgl : AEditor := edDateEdit;
    ColHarga : AEditor := edFloat;
    ColStokSkr : AEditor := edPositiveNumeric;
    ColStok : AEditor := edCheckBox;
  end;
end;

procedure TfrmInput.asgDaftarAutoAddRow(Sender: TObject; ARow: Integer);
begin
  asgDaftar.AddCheckBox(ColStok, ARow, False, False);
end;

procedure TfrmInput.asgDaftarGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow = 0) or (ACol = ColStok) then HAlign := taCenter
  else if ACol in [ColNo, ColHarga, ColStokSkr, ColTotal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInput.asgDaftarCellValidate(Sender: TObject; Col,
  Row: Integer; var Value: String; var Valid: Boolean);
begin
  Valid := Value <> '';
  case Col of
    ColHarga : begin
      if Valid and (asgDaftar.Cells[ColStokSkr, Row] <> '') then
        asgDaftar.Floats[ColTotal, Row] := asgDaftar.Floats[Col, Row] * asgDaftar.Floats[ColStokSkr, Row];
    end;

    ColStokSkr : begin
      if Valid and (asgDaftar.Cells[ColHarga, Row] <> '') then
        asgDaftar.Floats[ColTotal, Row] := asgDaftar.Floats[Col, Row] * asgDaftar.Floats[ColHarga, Row];
    end;
  end;
  ArrangeColSize;
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  Execute;
end;

procedure TfrmInput.btnPindahClick(Sender: TObject);
var i, idx : integer;
    stat : Boolean;
    Hasil : TR_Data;
begin
  setlength(Arr, 0);
  for i := 1 to asgDaftar.RowCount-2 do begin
    asgDaftar.GetCheckBoxState(ColStok, i, stat);
    if stat = True then begin
      setlength(Arr, length(Arr)+1);
      idx := length(Arr)-1;
      Hasil := UEngine.Get_Data(asgDaftar.Cells[ColKode, i]);
      Arr[idx].Kode := Hasil.Kode;
      Arr[idx].Nama := Hasil.Nama;
      Arr[idx].tanggal := Hasil.tanggal;
      Arr[idx].harga := Hasil.harga;
      Arr[idx].stok := Hasil.stok;
      Arr[idx].total := Hasil.total;

{      Arr[idx].Kode := asgDaftar.Cells[ColKode, i];
      Arr[idx].Nama := asgDaftar.Cells[ColNama, i];
      Arr[idx].tanggal := asgDaftar.Dates[ColTgl, i];
      Arr[idx].harga := asgDaftar.Floats[ColHarga, i];
      Arr[idx].stok := asgDaftar.Ints[ColStokSkr, i];
      Arr[idx].total := asgDaftar.Floats[ColTotal, i];}
    end;
  end;
  frmPindah.Execute(False);
end;

procedure TfrmInput.btnPindahUrutClick(Sender: TObject);
var i, idx : integer;
    stat : Boolean;
begin
  setlength(Arr, 0);
  for i := 1 to asgDaftar.RowCount-2 do begin
    asgDaftar.GetCheckBoxState(ColStok, i, stat);
    if stat = True then begin
      setlength(Arr, length(Arr)+1);
      idx := length(Arr)-1;
      Arr[idx].Kode := asgDaftar.Cells[ColKode, i];
      Arr[idx].Nama := asgDaftar.Cells[ColNama, i];
      Arr[idx].tanggal := asgDaftar.Dates[ColTgl, i];
      Arr[idx].harga := asgDaftar.Floats[ColHarga, i];
      Arr[idx].stok := asgDaftar.Ints[ColStokSkr, i];
      Arr[idx].total := asgDaftar.Floats[ColTotal, i];
    end;
  end;
  frmPindah.Execute(True);
end;

procedure TfrmInput.btnReportClick(Sender: TObject);
var i, idx, j : integer;
    stat : Boolean;
    tgl_data : TDate;
begin
  setlength(Arr, 0);
  for i := 1 to asgDaftar.RowCount-2 do begin
    setlength(Arr, length(Arr)+1);
    idx := length(Arr)-1;
    Arr[idx].Kode := asgDaftar.Cells[ColKode, i];
    Arr[idx].Nama := asgDaftar.Cells[ColNama, i];
    Arr[idx].tanggal := asgDaftar.Dates[ColTgl, i];
    Arr[idx].harga := asgDaftar.Floats[ColHarga, i];
    Arr[idx].stok := asgDaftar.Ints[ColStokSkr, i];
    Arr[idx].total := asgDaftar.Floats[ColTotal, i];
  end;

  //siapin isi arr tanggal
  setlength(frmReport.ArrTgl, 0);
  for i := 1 to asgDaftar.RowCount-2 do begin
    tgl_data := asgDaftar.Dates[ColTgl, i];
    stat := False; //anggep data tidak ada
    for j := 0 to length(frmReport.ArrTgl)-1 do begin
      if tgl_data = frmReport.ArrTgl[j] then begin
        stat := True; break;
      end;
    end;
    if not stat then begin
      setlength(frmReport.ArrTgl, length(frmReport.ArrTgl)+1);
      idx := length(frmReport.ArrTgl)-1;
      frmReport.ArrTgl[idx] := tgl_data;
    end;
  end;

  frmReport.Execute;
end;

procedure TfrmInput.btnSaveClick(Sender: TObject);
var i : integer;
    Input : TR_Data;
begin
  myConnection.BeginSQL;
  try
    for i := 1 to asgDaftar.RowCount-2 do begin
      //proses save ke database
      Input.Kode := asgDaftar.Cells[ColKode, i];
      Input.Nama := asgDaftar.Cells[ColNama, i];
      Input.tanggal := asgDaftar.Dates[ColTgl, i];
      Input.harga := asgDaftar.Floats[ColHarga, i];
      Input.stok := asgDaftar.Ints[ColStokSkr, i];
      Input.total := asgDaftar.Floats[ColTotal, i];
      UEngine.Insert_Data(Input);
    end;

    myConnection.EndSQL; //commit
    ShowMessage('Simpan BERHASIL');
  except
    myConnection.UndoSQL; //rollback
    ShowMessage('Simpan GAGAL');
  end;
end;

end.
