unit UEngine;

interface

uses ADOInt, URecord, StrUtils, DateUtils, Math, PostgresConnection;

procedure Insert_Data(Data : TR_Data);
procedure Update_Data(Data : TR_Data);
procedure Delete_Data(kode : string);
function Load_Data : Arr_Data;
function Get_Data(kode : string) : TR_Data;

implementation
var sqL : string;

procedure Insert_Data(Data : TR_Data);
begin
  sqL := 'INSERT INTO data (kode, nama, tanggal, harga, stok, total) '+
         'VALUES ('+FormatSQLString(Data.Kode)+','+
                    FormatSQLString(Data.Nama)+','+
                    FormatSQLDate(Data.tanggal)+','+
                    FormatSQLNumber(Data.harga)+','+
                    FormatSQLNumber(Data.stok)+','+
                    FormatSQLNumber(Data.total)+')';
  myConnection.ExecSQL(sqL);
end;

procedure Update_Data(Data : TR_Data);
begin
  sqL := 'UPDATE data SET '+
         'nama = '+FormatSQLString(Data.Nama)+', '+
         'tanggal = '+FormatSQLDate(Data.tanggal)+', '+
         'harga = '+FormatSQLNumber(Data.harga)+', '+
         'stok = '+FormatSQLNumber(Data.stok)+', '+
         'total = '+FormatSQLNumber(Data.Total)+' '+
         'WHERE kode = '+FormatSQLString(Data.Kode);
  myConnection.ExecSQL(sqL);
end;

procedure Delete_Data(kode : string);
begin
  sqL := 'DELETE FROM data WHERE kode = '+FormatSQLString(kode);
  myConnection.ExecSQL(sqL);
end;

function Load_Data : Arr_Data;
var buffer : _Recordset;
    i : integer;
begin
              //  0     1      2        3     4      5
  sqL := 'SELECT kode, nama, tanggal, harga, stok, total FROM data ORDER BY nama';
  buffer := myConnection.OpenSQL(sqL);
  setlength(Result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount-1 do begin
    Result[i].Kode := BufferToString(buffer.Fields[0].Value);
    Result[i].Nama := BufferToString(buffer.Fields[1].Value);
    Result[i].tanggal := BufferToDateTime(buffer.Fields[2].Value);
    Result[i].harga := BufferToFloat(buffer.Fields[3].Value);
    Result[i].stok := BufferToInteger(buffer.Fields[4].Value);
    Result[i].total := BufferToFloat(buffer.Fields[5].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Get_Data(kode : string) : TR_Data;
var buffer : _Recordset;
begin
  sqL := 'SELECT kode, nama, tanggal, harga, stok, total FROM data WHERE kode = '+FormatSQLString(kode);
  buffer := myConnection.OpenSQL(sqL);
  Result.Kode := BufferToString(buffer.Fields[0].Value);
  Result.Nama := BufferToString(buffer.Fields[1].Value);
  Result.tanggal := BufferToDateTime(buffer.Fields[2].Value);
  Result.harga := BufferToFloat(buffer.Fields[3].Value);
  Result.stok := BufferToInteger(buffer.Fields[4].Value);
  Result.total := BufferToFloat(buffer.Fields[5].Value);
  buffer.Close;
end;

end.
