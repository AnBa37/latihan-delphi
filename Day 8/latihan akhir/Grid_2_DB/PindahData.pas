unit PindahData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, AdvObj;

type
  TfrmPindah = class(TForm)
    asgDaftar: TAdvStringGrid;
    procedure asgDaftarGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
  private
    { Private declarations }
    IsUrut : Boolean;
    procedure SetGrid;
    procedure ArrangeCOlSize;
    procedure LoadData;
  public
    { Public declarations }
    procedure Execute(UrutHarga : Boolean);
  end;

var
  frmPindah: TfrmPindah;

implementation

uses InputData;

{$R *.dfm}
Const
  ColNo = 0;
  ColKode = 1;
  ColNama = 2;
  ColTgl = 3;
  ColHarga = 4;
  ColStokSkr = 5;
  ColTotal = 6;

procedure TfrmPindah.ArrangeCOlSize;
begin
  asgDaftar.AutoNumberCol(ColNo);
  asgDaftar.ClearRows(asgDaftar.RowCount-1, 1);
  asgDaftar.Cells[ColHarga, asgDaftar.RowCount-1] := 'Total : ';
  asgDaftar.FloatingFooter.ColumnCalc[ColStokSkr] := acSUM;
  asgDaftar.FloatingFooter.ColumnCalc[ColTotal] := acSUM;
  asgDaftar.AutoSizeColumns(True, 2);
end;

procedure TfrmPindah.Execute(UrutHarga : Boolean);
begin
  IsUrut := UrutHarga;
  SetGrid;
  LoadData;
  ShowModal;
end;

procedure TfrmPindah.LoadData;
var i, vRow : integer;
begin
  asgDaftar.ClearNormalCells;
  asgDaftar.ClearRows(asgDaftar.RowCount-1, 1);
  asgDaftar.RowCount := 3;
  for i := 0 to length(frmInput.Arr)-1 do begin
    if i > 0 then asgDaftar.AddRow;
    vRow := asgDaftar.RowCount-2;
    asgDaftar.Cells[ColKode, vRow] := frmInput.Arr[i].Kode;
    asgDaftar.Cells[ColNama, vRow] := frmInput.Arr[i].Nama;
    asgDaftar.Dates[ColTgl, vRow] := frmInput.Arr[i].tanggal;
    asgDaftar.Floats[ColHarga, vRow] := frmInput.Arr[i].harga;
    asgDaftar.Floats[ColStokSkr, vRow] := frmInput.Arr[i].stok;
    asgDaftar.Floats[ColTotal, vRow] := frmInput.Arr[i].total;
  end;
  if IsUrut then begin
    asgDaftar.SortSettings.Direction := sdAscending;
    asgDaftar.SortByColumn(ColHarga);
  end;
  ArrangeCOlSize;
end;

procedure TfrmPindah.SetGrid;
begin
  asgDaftar.ClearNormalCells;
  asgDaftar.ClearRows(asgDaftar.RowCount-1, 1);
  asgDaftar.ColCount := 8;
  asgDaftar.RowCount := 3;
  asgDaftar.FixedCols := 1;
  asgDaftar.FixedRows := 1;
  ArrangeColSize;
end;

procedure TfrmPindah.asgDaftarGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow = 0) then HAlign := taCenter
  else if ACol in [ColNo, ColHarga, ColStokSkr, ColTotal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

end.
