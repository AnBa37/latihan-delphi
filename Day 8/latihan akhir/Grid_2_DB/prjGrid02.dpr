program prjGrid02;

uses
  Forms,
  InputData in 'InputData.pas' {frmInput},
  PindahData in 'PindahData.pas' {frmPindah},
  ReportData in 'ReportData.pas' {frmReport},
  PostgresConnection in 'PostgresConnection.pas' {myConnection: TDataModule},
  URecord in 'URecord.pas',
  UEngine in 'UEngine.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(TfrmPindah, frmPindah);
  Application.CreateForm(TfrmReport, frmReport);
  Application.Run;
end.
