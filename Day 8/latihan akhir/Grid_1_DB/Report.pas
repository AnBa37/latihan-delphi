unit Report;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, BaseGrid, AdvGrid, StdCtrls, PostgresConnection, UEngine,
  AdvObj;

type
  TfrmReport = class(TForm)
    asgReport: TAdvStringGrid;
    btnTutup: TButton;
    procedure asgReportGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgReportExpandNode(Sender: TObject; ARow,
      ARowreal: Integer);
    procedure asgReportContractNode(Sender: TObject; ARow,
      ARowreal: Integer);
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    ArrDept : array of string;
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
    procedure LoadNoArrDept;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmReport: TfrmReport;

implementation

uses InputDetail;

{$R *.dfm}
Const
  ColNode = 0;
  ColId = 1;
  ColNama = 2;
  ColGaji = 3;

procedure TfrmReport.ArrangeColSize;
begin
  asgReport.AutoSizeColumns(True, 2);
end;

procedure TfrmReport.asgReportGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter;
  //else if ACol in [ColId, ColGaji] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmReport.Execute;
begin
  setlength(ArrDept, 3);
  ArrDept[0] := 'Pembelian';
  ArrDept[1] := 'Marketing';
  ArrDept[2] := 'Finance';
  SetGrid;
  LoadData;
  ShowModal;
end;

procedure TfrmReport.LoadData;
var i, j, R1, R2 : integer;
begin
  //load dari database
  frmInputDetail.ArrData := Load_Data_Base;
  asgReport.ClearNormalCells;
  asgReport.RowCount := 2;
  //isi bapanya berdasarkan ArrDept
  for i := 0 to length(ArrDept)-1 do begin
    if i > 0 then asgReport.AddRow;
    R1 := asgReport.RowCount-1;
    asgReport.MergeCells(ColId, R1, 3, 1);
    asgReport.Cells[ColId, R1] := ArrDept[i];
    asgReport.FontStyles[ColId, R1] := [fsBold];
    asgReport.FontColors[ColId, R1] := clTeal;
    //isi anak dari frmDetail.ArrData
    for j := 0 to length(frmInputDetail.ArrData)-1 do begin
      if ArrDept[i] = frmInputDetail.ArrData[j].dept then begin
        asgReport.AddRow;
        R2 := asgReport.RowCount-1;
        asgReport.Alignments[ColId, R2] := taRightJustify;
        asgReport.Ints[ColId, R2] := frmInputDetail.ArrData[j].id;
        asgReport.Cells[ColNama, R2] := frmInputDetail.ArrData[j].nama;
        asgReport.Alignments[ColGaji, R2] := taRightJustify;
        asgReport.Floats[ColGaji, R2] := frmInputDetail.ArrData[j].gaji;
        if frmInputDetail.ArrData[j].gaji > 1500000 then begin
          asgReport.FontColors[ColId, R2] := clNavy;
          asgReport.FontColors[ColNama, R2] := clNavy;
          asgReport.FontColors[ColGaji, R2] := clNavy;
        end;
      end;
    end;
    R2 := asgReport.RowCount-1;
    if R1 <> R2 then asgReport.AddNode(R1, R2-R1+1);
  end;
  asgReport.ContractAll;
  ArrangeColSize;
end;

procedure TfrmReport.SetGrid;
begin
  asgReport.ClearNormalCells;
  asgReport.RowCount := 2;
  asgReport.ColCount := 5;
  asgReport.FixedCols := 0;
  asgReport.FixedRows := 1;
  ArrangeColSize;
end;

procedure TfrmReport.asgReportExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmReport.asgReportContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmReport.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmReport.LoadNoArrDept;
var j, R1, R2 : integer;
begin
  asgReport.ClearNormalCells;
  asgReport.RowCount := 2;
  //isi bapanya = Pembelian
  R1 := asgReport.RowCount-1;
  asgReport.MergeCells(ColId, R1, 3, 1);
  asgReport.Cells[ColId, R1] := 'Pembelian';
  asgReport.FontStyles[ColId, R1] := [fsBold];
  asgReport.FontColors[ColId, R1] := clTeal;
  //isi anak dari frmDetail.ArrData
  for j := 0 to length(frmInputDetail.ArrData)-1 do begin
    if 'Pembelian' = frmInputDetail.ArrData[j].dept then begin
      asgReport.AddRow;
      R2 := asgReport.RowCount-1;
      asgReport.Alignments[ColId, R2] := taRightJustify;
      asgReport.Ints[ColId, R2] := frmInputDetail.ArrData[j].id;
      asgReport.Cells[ColNama, R2] := frmInputDetail.ArrData[j].nama;
      asgReport.Alignments[ColGaji, R2] := taRightJustify;
      asgReport.Floats[ColGaji, R2] := frmInputDetail.ArrData[j].gaji;
      if frmInputDetail.ArrData[j].gaji > 1500000 then begin
        asgReport.FontColors[ColId, R2] := clNavy;
        asgReport.FontColors[ColNama, R2] := clNavy;
        asgReport.FontColors[ColGaji, R2] := clNavy;
      end;
    end;
  end;
  R2 := asgReport.RowCount-1;
  if R1 <> R2 then asgReport.AddNode(R1, R2-R1+1);

  asgReport.AddRow;
  R1 := asgReport.RowCount-1;
  asgReport.MergeCells(ColId, R1, 3, 1);
  asgReport.Cells[ColId, R1] := 'Marketing';
  asgReport.FontStyles[ColId, R1] := [fsBold];
  asgReport.FontColors[ColId, R1] := clTeal;
  //isi anak dari frmDetail.ArrData
  for j := 0 to length(frmInputDetail.ArrData)-1 do begin
    if 'Marketing' = frmInputDetail.ArrData[j].dept then begin
      asgReport.AddRow;
      R2 := asgReport.RowCount-1;
      asgReport.Alignments[ColId, R2] := taRightJustify;
      asgReport.Ints[ColId, R2] := frmInputDetail.ArrData[j].id;
      asgReport.Cells[ColNama, R2] := frmInputDetail.ArrData[j].nama;
      asgReport.Alignments[ColGaji, R2] := taRightJustify;
      asgReport.Floats[ColGaji, R2] := frmInputDetail.ArrData[j].gaji;
      if frmInputDetail.ArrData[j].gaji > 1500000 then begin
        asgReport.FontColors[ColId, R2] := clNavy;
        asgReport.FontColors[ColNama, R2] := clNavy;
        asgReport.FontColors[ColGaji, R2] := clNavy;
      end;
    end;
  end;
  R2 := asgReport.RowCount-1;
  if R1 <> R2 then asgReport.AddNode(R1, R2-R1+1);

  asgReport.AddRow;
  R1 := asgReport.RowCount-1;
  asgReport.MergeCells(ColId, R1, 3, 1);
  asgReport.Cells[ColId, R1] := 'Finance';
  asgReport.FontStyles[ColId, R1] := [fsBold];
  asgReport.FontColors[ColId, R1] := clTeal;
  //isi anak dari frmDetail.ArrData
  for j := 0 to length(frmInputDetail.ArrData)-1 do begin
    if 'Finance' = frmInputDetail.ArrData[j].dept then begin
      asgReport.AddRow;
      R2 := asgReport.RowCount-1;
      asgReport.Alignments[ColId, R2] := taRightJustify;
      asgReport.Ints[ColId, R2] := frmInputDetail.ArrData[j].id;
      asgReport.Cells[ColNama, R2] := frmInputDetail.ArrData[j].nama;
      asgReport.Alignments[ColGaji, R2] := taRightJustify;
      asgReport.Floats[ColGaji, R2] := frmInputDetail.ArrData[j].gaji;
      if frmInputDetail.ArrData[j].gaji > 1500000 then begin
        asgReport.FontColors[ColId, R2] := clNavy;
        asgReport.FontColors[ColNama, R2] := clNavy;
        asgReport.FontColors[ColGaji, R2] := clNavy;
      end;
    end;
  end;
  R2 := asgReport.RowCount-1;
  if R1 <> R2 then asgReport.AddNode(R1, R2-R1+1);

  asgReport.ContractAll;
  ArrangeColSize;
end;

end.
