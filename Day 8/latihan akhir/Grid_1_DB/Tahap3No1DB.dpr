program Tahap3No1DB;

uses
  Forms,
  Input in 'Input.pas' {frmInput},
  InputDetail in 'InputDetail.pas' {frmInputDetail},
  Tampil in 'Tampil.pas' {frmTampil},
  Report in 'Report.pas' {frmReport},
  PostgresConnection in 'PostgresConnection.pas' {myConnection: TDataModule},
  UEngine in 'UEngine.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(TfrmInputDetail, frmInputDetail);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.CreateForm(TfrmReport, frmReport);
  Application.Run;
end.
