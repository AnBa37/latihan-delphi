unit UEngine;

interface

uses InputDetail, PostgresConnection, ADOInt;

procedure Insert_Data(Data : TR_Data);
procedure Update_Data(Data : TR_Data);
procedure Delete_Data(id : integer);
procedure Select_Data(Id : integer; var Data : TR_Data);
function Load_Data_Base : AR_Data;

implementation
var sqL : string;

procedure Insert_Data(Data : TR_Data);
begin
  sqL := 'INSERT INTO data (id, nama, dept, gaji) '+
         'VALUES ('+FormatSQLNumber(Data.id)+', '+
                    FormatSQLString(Data.nama)+', '+
                    FormatSQLString(Data.dept)+', '+
                    FormatSQLNumber(Data.Gaji)+')';
  myConnection.ExecSQL(sqL);
end;

procedure Update_Data(Data : TR_Data);
begin
  sqL := 'UPDATE data SET '+
            'nama = '+FormatSQLString(Data.nama)+', '+
            'dept = '+FormatSQLString(Data.dept)+', '+
            'gaji = '+FormatSQLNumber(Data.Gaji)+' '+
            'WHERE id = '+FormatSQLNumber(Data.id);
  myConnection.ExecSQL(sqL);
end;

procedure Delete_Data(id : integer);
begin
  sqL := 'DELETE FROM data WHERE id = '+FormatSQLNumber(id);
  myConnection.ExecSQL(sqL);
end;

procedure Select_Data(Id : integer; var Data : TR_Data);
var buffer : _Recordset;
begin
              //  0    1    2     3
  sqL := 'SELECT id, nama, dept, gaji FROM data WHERE id = '+FormatSQLNumber(Id);
  buffer := myConnection.OpenSQL(sqL);
  Data.id := BufferToInteger(buffer.Fields[0].Value);
  Data.nama := BufferToString(buffer.Fields[1].Value);
  Data.dept := BufferToString(buffer.Fields[2].Value);
  Data.gaji := BufferToFloat(buffer.Fields[3].Value);
  buffer.Close;
end;

function Load_Data_Base : AR_Data;
var buffer : _Recordset;
    i : integer;
begin
              //  0    1    2     3
  sqL := 'SELECT id, nama, dept, gaji FROM data ORDER BY nama';
  buffer := myConnection.OpenSQL(sqL);
  setlength(Result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount-1 do begin
    Result[i].id := BufferToInteger(buffer.Fields[0].Value);
    Result[i].nama := BufferToString(buffer.Fields[1].Value);
    Result[i].dept := BufferToString(buffer.Fields[2].Value);
    Result[i].gaji := BufferToFloat(buffer.Fields[3].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

end.
