object frmInputDetail: TfrmInputDetail
  Left = 203
  Top = 180
  BorderStyle = bsDialog
  Caption = 'Input Detail'
  ClientHeight = 150
  ClientWidth = 201
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 9
    Height = 13
    Caption = 'Id'
  end
  object Label2: TLabel
    Left = 8
    Top = 44
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label3: TLabel
    Left = 8
    Top = 68
    Width = 23
    Height = 13
    Caption = 'Dept'
  end
  object Label4: TLabel
    Left = 8
    Top = 92
    Width = 18
    Height = 13
    Caption = 'Gaji'
  end
  object sttId: TStaticText
    Left = 48
    Top = 16
    Width = 24
    Height = 17
    BorderStyle = sbsSingle
    Caption = 'sttId'
    TabOrder = 4
  end
  object txtNama: TAdvEdit
    Left = 48
    Top = 40
    Width = 121
    Height = 21
    AutoFocus = False
    EditAlign = eaLeft
    EditType = etString
    ErrorColor = clRed
    ErrorFontColor = clWhite
    ExcelStyleDecimalSeparator = False
    Flat = False
    FlatLineColor = clBlack
    FlatParentColor = True
    FocusAlign = eaDefault
    FocusBorder = False
    FocusColor = clWindow
    FocusFontColor = clWindowText
    FocusLabel = False
    FocusWidthInc = 0
    ModifiedColor = clHighlight
    DisabledColor = clSilver
    URLColor = clBlue
    ReturnIsTab = True
    LengthLimit = 0
    TabOnFullLength = False
    Precision = 0
    LabelPosition = lpLeftTop
    LabelMargin = 4
    LabelTransparent = False
    LabelAlwaysEnabled = False
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Persistence.Enable = False
    Persistence.Location = plInifile
    Color = clWindow
    Enabled = True
    HintShowLargeText = False
    MaxLength = 50
    OleDropTarget = False
    OleDropSource = False
    Signed = False
    TabOrder = 0
    Text = 'txtNama'
    Transparent = False
    Visible = True
  end
  object txtGaji: TAdvEdit
    Left = 48
    Top = 88
    Width = 121
    Height = 21
    AutoFocus = False
    EditAlign = eaRight
    EditType = etMoney
    ErrorColor = clRed
    ErrorFontColor = clWhite
    ExcelStyleDecimalSeparator = True
    Flat = False
    FlatLineColor = clBlack
    FlatParentColor = True
    FocusAlign = eaDefault
    FocusBorder = False
    FocusColor = clWindow
    FocusFontColor = clWindowText
    FocusLabel = False
    FocusWidthInc = 0
    ModifiedColor = clHighlight
    DisabledColor = clSilver
    URLColor = clBlue
    ReturnIsTab = True
    LengthLimit = 0
    TabOnFullLength = False
    Precision = 2
    LabelPosition = lpLeftTop
    LabelMargin = 4
    LabelTransparent = False
    LabelAlwaysEnabled = False
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Persistence.Enable = False
    Persistence.Location = plInifile
    Color = clWindow
    Enabled = True
    HintShowLargeText = False
    OleDropTarget = False
    OleDropSource = False
    Signed = False
    TabOrder = 2
    Text = '0,00'
    Transparent = False
    Visible = True
  end
  object cmbDept: TComboBox
    Left = 48
    Top = 64
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnKeyPress = cmbDeptKeyPress
    Items.Strings = (
      'Pembelian'
      'Marketing'
      'Finance')
  end
  object btnOk: TButton
    Left = 120
    Top = 120
    Width = 75
    Height = 25
    Caption = '&Ok'
    TabOrder = 3
    OnClick = btnOkClick
  end
end
