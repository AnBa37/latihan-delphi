unit InputDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, PostgresConnection;

type
  TR_Data = Record
    id : integer;
    nama : string[50];
    dept : string[15];
    gaji : real;
  end;
  AR_Data = array of TR_Data;

{sintaks create table di PostGre
create table data (
	id integer,
	nama varchar(50),
	dept varchar(15),
	gaji float,
	primary key(id)
);}

  TfrmInputDetail = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    sttId: TStaticText;
    txtNama: TAdvEdit;
    txtGaji: TAdvEdit;
    cmbDept: TComboBox;
    btnOk: TButton;
    procedure cmbDeptKeyPress(Sender: TObject; var Key: Char);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
    idx : integer;
    EditMode, DetailMode : Boolean;
    procedure InitForm;
    procedure LoadData;
  public
    { Public declarations }
    ArrData, ArrTemp : AR_Data;
    procedure Execute(Id : integer; isEdit, isDetail : Boolean);
  end;

var
  frmInputDetail: TfrmInputDetail;

implementation

uses UEngine;

{$R *.dfm}

procedure TfrmInputDetail.cmbDeptKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then txtGaji.SetFocus;
end;

procedure TfrmInputDetail.Execute(Id: integer; isEdit, isDetail: Boolean);
begin
  idx := id;
  EditMode := isEdit;
  DetailMode := isDetail;
  InitForm; sttId.Caption := IntToStr(Id);
  if EditMode or DetailMode then LoadData;
  if EditMode then frmInputDetail.Caption := 'Edit Data'
  else if DetailMode then frmInputDetail.Caption := 'Detail Data'
  else frmInputDetail.Caption := 'Input Data';
  frmInputDetail.Position := poDesktopCenter;
  ShowModal;
end;

procedure TfrmInputDetail.InitForm;
begin
  sttId.Caption := '';
  txtNama.Text := '';
  cmbDept.ItemIndex := -1;
  txtGaji.FloatValue := 0;

  txtNama.ReadOnly := DetailMode;
  cmbDept.Enabled := not DetailMode;
  txtGaji.ReadOnly := DetailMode;
end;

procedure TfrmInputDetail.btnOkClick(Sender: TObject);
var Data : TR_Data;
begin
  if DetailMode and not EditMode then Close
  else begin //isi ke array data
    if trim(txtNama.Text) = '' then begin
      ShowMessage('Nama tidak boleh kosong.');
      txtNama.SetFocus;
      exit; //supaya tidak keluar dari form input dan tidak masuk ke array
    end else if cmbDept.ItemIndex = -1 then begin
      ShowMessage('Dept belum dipilih.');
      cmbDept.SetFocus;
      exit;
    end else if txtGaji.FloatValue = 0 then begin
      ShowMessage('Gaji tidak boleh 0.');
      txtGaji.SetFocus;
      exit;
    end;
    //save ke database
    myConnection.BeginSQL;
    try
      Data.id := idx;
      Data.nama := trim(txtNama.Text);
      Data.dept := cmbDept.Text;
      Data.gaji := txtGaji.FloatValue;
      if not EditMode then Insert_Data(Data)
      else Update_Data(Data);
      myConnection.EndSQL;
    except
      myConnection.UndoSQL;
      ShowMessage('SAVE GAGAL...');
    end;

{    ArrData[idx].id := idx;
    ArrData[idx].nama := trim(txtNama.Text);
    ArrData[idx].dept := cmbDept.Text;
    ArrData[idx].gaji := txtGaji.FloatValue;
}
    close;
  end;
end;

procedure TfrmInputDetail.LoadData;
var Data : TR_Data;
begin
  Select_Data(idx, Data);
  txtNama.Text := Data.nama;
  txtGaji.FloatValue := Data.gaji;
  if Data.dept = 'Pembelian' then cmbDept.ItemIndex := 0
  else if Data.dept = 'Marketing' then cmbDept.ItemIndex := 1
  else if Data.dept = 'Finance' then cmbDept.ItemIndex := 2;
{  txtNama.Text := ArrData[idx].nama;
  txtGaji.FloatValue := ArrData[idx].gaji;
  if ArrData[idx].dept = 'Pembelian' then cmbDept.ItemIndex := 0
  else if ArrData[idx].dept = 'Marketing' then cmbDept.ItemIndex := 1
  else if ArrData[idx].dept = 'Finance' then cmbDept.ItemIndex := 2;
}
end;

end.
