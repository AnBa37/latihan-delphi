unit PostgresConnection;
{$DEFINE DEBUG}
{$IFDEF DEBUG}
  {$DEFINE SQL_DEBUG}
{$ENDIF}

interface

uses
  ActiveX, ADOConst, ComObj, ADOInt, StrUtils,
  SysUtils, Classes, DB, ADODB, Variants, Controls,
  Windows, Messages, Graphics, Dialogs, Forms, DBTables,DateUtils,
  BaseGrid, AdvGrid, asgprev, AsgFindDialog;
  //EditLinks;

type
  TSQLOperator = (soGreaterThan, soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);
  TBooleanOperator = (boTrue,boFalse,boNone);

  //add by j@idol
  TCompareOperator = (coEquals, coInEquals, coInclude);

  arrString = array of string;
  arInteger = array of integer;
  arDate    = array of TDate;

  TmyConnection = class(TDataModule)
    ADOConnection: TADOConnection;
    ADOCommand: TADOCommand;
    ADODataSet: TADODataSet;
    SaveToExcell: TSaveDialog;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  protected
    { Private declarations }
    Level: integer;
    Log: array of TStringList;
  public
    { Public declarations }
    //ListKategori: TStringList;

    function StoredProc(AName: string): TParameters;
    procedure ExecStoredProc;
    function AddParamString(AName: string; AString: string; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamNull(AName: string): TParameter;
    function AddParamInteger(AName: string; AInteger: integer; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamFloat(AName: string; AFloat: real; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamDateTime(AName: string; ADateTime: TDateTime; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamDateTime2(AName: string; ADateTime: TDateTime; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamBoolean(AName: string; ABoolean: boolean; ADirection: TParameterDirection = pdInput): TParameter;
    function GetParamOutput(AName: string): variant;

    procedure DumpSQL(ASQL: string);
    function ExecSQL(ASQL: string): integer;
    function OpenSQL(ASQL: string): _RecordSet;

    function BeginSQL: integer;
    procedure EndSQL;
    procedure UndoSQL;

    class function OpenRecordSet(ASQL: string): _RecordSet;
    procedure InsertDumpSQL(ASQL: string);
  end;

var
  myConnection: TmyConnection;
  //DQLFile   : string = {$IFDEF SQL_DEBUG}'dql.sql'{$ELSE}''{$ENDIF};
  DMLFile   : string = {$IFDEF SQL_DEBUG}'dml.sql'{$ELSE}''{$ENDIF};
  ErrorFile : string = 'error.log';
  DumpFile  : string = 'dump.sql';
  DelFile   : string = 'delete.log';

const
  TRUE_STRING = 'T';
  FALSE_STRING = 'F';
  TRUE_VALUE = -1;
  FALSE_VALUE = 0;
  DBisORCL = FALSE;

  function ServerNow: TDateTime;
  function FormatSQLDateTimeNow: string;
  function FormatSQLDateNow: string;
  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  function getStringFromSQL(aSql:string):string;
  function getFloatFromSQL(aSql:string):double;
  function getIntegerFromSQL(aSql:string):integer;
  function getBooleanFromSQL(aSql:string):boolean;
  function getTDateFromSQL(aSql:string):TDate;
  function ExecuteSQL(aSql:string):boolean;

  function ExecTransaction(SQL: string; InsertMode: boolean=true): boolean;
  function ExecTransaction2(SQL: string; InsertMode: boolean=true): boolean;
  function ExecTransaction3(SQL: string): boolean;
  function ExecDelete(SQL: string): boolean;

  //alin tambah 051009 KHUSUS untuk PostGre
  function SelectTimePG(Field : String; is3 : Boolean = True) : String;
  function FormatSQLTimePG(ATime: TTime): string;
  function BufferToTimePG(ABuffer: variant): TTime;
  function SelectDateTimePG(Field : String; is3 : Boolean = True) : String;
  function FormatSQLDateTimePG(ADateTime: TDateTime): string;

  function FormatSQLBoolean(ABoolean: boolean): string;
  function FormatSQLDateTime(ADateTime: TDateTime): string;
  function FormatSQLDate(ADate: TDate): string;
  function FormatSQLTime(ATime: TTime): string;
  function FormatSQLDateTime2(ADateTime: TDateTime):string;
  function FormatSQLString(AString: string): string; overload;
  function FormatSQLString(AChar: char): string; overload;
  function FormatSQLNumber(AInteger: integer): string; overload;
  function FormatSQLNumber(AFloat: real): string; overload;
  function FormatSQLNULL: string;
  function FormatSQLNULL2: string;
  function FormatSQLOperator(operator:TSQLOperator):string;
  function FormatSQLBooleanOperator(operator:TBooleanOperator):string;
  function FormatSQLCompareOperator(operator:TCompareOperator):string;
  //TSetOperator = (soEquals, soInEquals, soInclude);

  function BufferToBooleanDef(ABuffer: variant; ADefault: boolean): boolean;
  function BufferToBoolean(ABuffer: variant): boolean;
  function BufferToStringDef(ABuffer: variant; ADefault: string): string;
  function BufferToString(ABuffer: variant): string;
  function BufferToIntegerDef(ABuffer: variant; ADefault: integer): integer;
  function BufferToInteger(ABuffer: variant): integer;
  function BufferToFloatDef(ABuffer: variant; ADefault: real): real;
  function BufferToFloat(ABuffer: variant): real;
  function BufferToDateTimeDef(ABuffer: variant; ADefault: TDateTime): TDateTime;
  function BufferToDateTime(ABuffer: variant): TDateTime;

  procedure SetingPrint(Asg: TAdvStringGrid);
  function CreateNewSeq(NamaSeq : String; Table : string) : integer;
  function GetCurrentSeq(NamaSeq, Table, ANomor : string) : integer;
  function ExecAktivasi(SQL: string; isAktif: boolean=true): boolean;

  function Get_NVL(Val1, Val2 : String) : String;
  function Get_Null(Field : String; Stat : Boolean) : String;
implementation

{$R *.dfm}

  function ServerNow: TDateTime;
  var sql: string;
    buffer: _RecordSet;
  begin
    if DBisORCL then
      sql := 'SELECT '+FormatSQLDateNow+' FROM DUAL'
    else
      sql := 'SELECT now()';
      //sql := 'SELECT current_timestamp';
    buffer := myConnection.ADOConnection.Execute(sql);
    Result := BufferToDateTime(buffer.Fields[0].Value);
    buffer.Close;
  end;

  procedure WriteLog(AFilename, AString: string);
  var FP: Text;
  begin
    if (AFileName='') then Exit;

    AssignFile(FP, AFilename);
    {$I-} Append(FP); {$I+}
    if (IOResult<>0) then Rewrite(FP);
    Writeln(FP, AString);
    CloseFile(FP);
  end;

{ ---------------------------------------------------------------------------- }

procedure TmyConnection.DataModuleCreate(Sender: TObject);
var path:string;
begin
  try
    path := 'FILE NAME='+GetCurrentDir+'\connection.udl';
    ADOConnection.ConnectionString := path;
    ADOConnection.Open;
    repeat Application.ProcessMessages; until ADOConnection.Connected;
  except
    Application.Terminate;
    repeat Application.ProcessMessages; until Application.Terminated;
    raise;
  end;

  SaveToExcell.InitialDir := GetCurrentDir;
  
  if (DMLFile<>'') then begin
    Level := 0;
    setLength(Log,0);
  end;

  //writelog(DQLFile,sLineBreak+'-- Started on '+DateTimeToStr(Now));
//  writelog(DMLFile,sLineBreak+'-- Started on '+DateTimeToStr(Now));
end;

procedure TmyConnection.DataModuleDestroy(Sender: TObject);
var i: integer;
begin
  //writelog(DQLFile,'-- Stopped on '+DateTimeToStr(Now));
//  writelog(DMLFile,'-- Stopped on '+DateTimeToStr(Now));

  if (DMLFile<>'') then
    for i:=0 to high(Log) do Log[i].Destroy;

  if (ADOConnection.Connected) then begin
    ADOConnection.Close;
    repeat Application.ProcessMessages; until not ADOConnection.Connected;
  end;
end;

function TmyConnection.BeginSQL: integer;
begin
  Level := ADOConnection.BeginTrans;
  Result := Level;

  if (DMLFile<>'') then begin

//    writeLog(DMLFile,'-- Begin on level '+IntToStr(Level));
    setLength(Log,Level);
    Log[high(Log)] := TStringList.Create;
  end;
end;

procedure TmyConnection.EndSQL;
begin
  if (Level=0) then Exit;
  dec(Level); ADOConnection.CommitTrans;

  if (DMLFile<>'') then begin
    {if (Level=0) then }writeLog(DMLFile,Log[high(Log)].Text);
    //else Log[Level].AddStrings(Log[high(Log)]);
//    writeLog(DMLFile,'-- Commit on level '+IntToStr(Level+1));
    Log[high(Log)].Destroy;
    setLength(Log,Level);
  end;
end;

procedure TmyConnection.UndoSQL;
begin
  if (Level=0) then Exit;
  dec(Level); ADOConnection.RollbackTrans;

  if (DMLFile<>'') then begin
//    writeLog(ErrorFile,'RollBack data dump:'+sLineBreak+Log[high(Log)].Text);
//    writeLog(DMLFile,'-- Rollback on level '+IntToStr(Level+1));
    Log[high(Log)].Destroy;
    setLength(Log,Level);
  end;
end;

function TmyConnection.ExecSQL(ASQL: string): integer;
var
  vTemp: String;
begin
  try
    ADOConnection.Execute(ASQL,Result);
    //InsertDumpSQL(ASQL);
    //handy 140906
    try
//      WriteLog(DumpFile, FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' : '+ASQL);
    except
    end;
    if (DMLFile<>'') then begin
      if (Level = 0) then
//        writeLog(DMLFile,ASQL+';')
       else
        Log[high(Log)].Append(ASQL+';');
    end;
    vTemp := Copy(ASQL, 1, 6);
    if UpperCase(vTemp) = 'DELETE' then
//      WriteLog(DelFile, FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' : '+ASQL);
  except
    on E: Exception do begin
//      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      //InsertDumpSQL(ASQL);
      raise;
    end;
  end;
end;

function TmyConnection.OpenSQL(ASQL: string): _RecordSet;
begin
  try
   Result := ADOConnection.Execute(ASQL);
        //writelog(DQLFile,ASQL);
  except
    on E: Exception do begin
//      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      raise;
    end;
  end;
end;

procedure TmyConnection.DumpSQL(ASQL: string);
begin
//  WriteLog(DumpFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+ ' - '+ASQL);
end;

procedure TmyConnection.ExecStoredProc;
var i: integer; ASQL, DirectionStr, ValueStr: string;
begin
  ASQL := '';
  for i:=0 to ADOCommand.Parameters.Count-1 do
    with ADOCommand.Parameters[i] do begin
      {case Direction of
        pdInput: DirectionStr:='<=';
        pdOutput: DirectionStr:='=>';
        pdInputOutput: DirectionStr:='<=>';
        pdReturnValue: DirectionStr:='=';
      end;}
      DirectionStr:=' => ';
      if (Direction in [pdInput,pdInputOutput]) then begin
        case DataType of
          ftDateTime: ValueStr := FormatSQLDate(Value);
          ftFloat, ftInteger: ValueStr := FormatSQLNumber(Value);
          ftString: ValueStr := FormatSQLString(Value);
          
        end;
        ASQL := ASQL + ', '+Name+DirectionStr+ValueStr+' '
      end{ else
        ASQL := ASQL + ', '+Name+DirectionStr+' '};
    end;
  if (length(ASQL)>0) then delete(ASQL,1,1);

  ASQL := 'EXEC '+ADOCommand.CommandText+'('+ASQL+')';

  try
    ADOCommand.Execute;
    if (DMLFile<>'') then begin
      if (Level = 0) then
//        writeLog(DMLFile,ASQL+';')
      else
        Log[high(Log)].Append(ASQL+';');
    end;
  except
    on E: Exception do begin
//      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      raise;
    end;
  end;
end;

function TmyConnection.StoredProc(AName: string): TParameters;
begin
  ADOCommand.CommandType := cmdStoredProc;
  ADOCommand.CommandText := AName;
  ADOCommand.Parameters.Clear;
  Result := ADOCommand.Parameters;
end;

function TmyConnection.AddParamDateTime(AName: string; ADateTime: TDateTime;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftDateTime, ADirection, 0, ADateTime);
end;

function TmyConnection.AddParamFloat(AName: string; AFloat: real;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftFloat, ADirection, 0, AFloat);
end;

function TmyConnection.AddParamInteger(AName: string; AInteger: integer;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftInteger, ADirection, 0, AInteger);
end;

function TmyConnection.AddParamString(AName: string; AString: string;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftString, ADirection, length(AString)+1, AString);
end;

function TmyConnection.AddParamNull(AName: string): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftUnknown, pdInput, 0, varNull);
end;

function TmyConnection.AddParamBoolean(AName: string; ABoolean: boolean;
  ADirection: TParameterDirection): TParameter;
begin
  if (ABoolean) then
    Result := AddParamInteger(AName,TRUE_VALUE,ADirection)
  else
    Result := AddParamInteger(AName,FALSE_VALUE,ADirection);
end;

function TmyConnection.GetParamOutput(AName: string): variant;
begin
  Result := ADOCommand.Parameters.ParamValues[AName];
end;

{ ---------------------------------------------------------------------------- }

  function FormatSQLDateTimeNow: string;
  begin
    Result := 'SYSDATE';
  end;

  function FormatSQLDateNow: string;
  begin
    Result := 'TRUNC(SYSDATE)';
  end;

  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  { I.S. : Input, Find, dan ReplaceWith terdefinisi
    F.S. : menghasilkan string Input di mana substring Find diganti dengan
           substring ReplaceWith }
  var i: integer;
      Tmp: string;
  begin
    while (Pos(Find,Input)>0) do begin
      i:=Pos(Find,Input);
      Delete(Input,i,Length(Find));
      Tmp:=Copy(Input,i,Length(Input)-i+2);
      Delete(Input,i,Length(Input)-i+2);
      Input:=Input+ReplaceWith+Tmp;
    end;

    Result:=Input;
  end;

  function getStringFromSQL(aSql:string):string;
  var buffer :_Recordset;
   {} begin
      buffer := myConnection.OpenSQL(aSql);
      Result := '';
      if buffer.RecordCount > 0 then
        Result := BufferToString(buffer.Fields[0].Value);
      buffer.Close;
    end;

  function getFloatFromSQL(aSql:string):double;
  var buffer :_Recordset;
    begin
      buffer := myConnection.OpenSQL(aSql);
      Result := 0;
     if buffer.RecordCount > 0 then
       Result := BufferToFloat(buffer.Fields[0].Value);
     buffer.Close;
    end;

  function getIntegerFromSQL(aSql:string):integer;
  var buffer :_Recordset;
    begin
      buffer := myConnection.OpenSQL(aSql);
      Result := 0;
      if buffer.RecordCount > 0 then
        Result := BufferToInteger(buffer.Fields[0].Value);
      buffer.Close;
    end;

  function getBooleanFromSQL(aSql:string):boolean;
  var buffer :_Recordset;
    begin
      buffer := myConnection.OpenSQL(aSql);
      Result := False;
     if buffer.RecordCount > 0 then
       Result := BufferToBoolean(buffer.Fields[0].Value);
     buffer.Close;
    end;

  function getTDateFromSQL(aSql:string):TDate;
  var buffer :_Recordset;
    begin
      buffer := myConnection.OpenSQL(aSql);
      Result := 0;
      if buffer.RecordCount > 0 then
        Result := BufferToDateTime(buffer.Fields[0].Value);
      buffer.Close;
    end;

  function ExecuteSQL(aSql:string):boolean;
   begin
      try myConnection.BeginSQL;
        myConnection.ExecSQL(aSql);
        myConnection.EndSQL;
        Result := True;
      except
        myConnection.UndoSQL;
        Result := False;
      end;
   end;

  function ExecTransaction(SQL: string;
    InsertMode: boolean): boolean;
  begin
    try
      myConnection.BeginSQL;
      myConnection.ExecSQL(SQL);
      myConnection.EndSQL;
//      showmessage(IfThen(InsertMode,MSG_SUCCESS_SAVING,MSG_SUCCESS_UPDATE));
      Result:= true;
      except
        myConnection.UndoSQL;
//        showmessage(IfThen(InsertMode,MSG_UNSUCCESS_SAVING,MSG_UNSUCCESS_UPDATE));
        Result:= false;
    end;
  end;

  function ExecTransaction2(SQL: string;
    InsertMode: boolean): boolean;
  begin
    try
      myConnection.BeginSQL;
      myConnection.ExecSQL(SQL);
      myConnection.EndSQL;
      Result:= true;
      except
        myConnection.UndoSQL;
        Result:= false;
    end;
  end;

  function ExecTransaction3(SQL: string): boolean;
  begin
    try
      myConnection.ExecSQL(SQL);
      Result:= true;
      except
        raise;
        Result:= false;
    end;
  end;

  function ExecAktivasi(SQL: string; isAktif: boolean): boolean;
  begin
    myConnection.BeginSQL;
    try
      myConnection.ExecSQL(SQL);
      myConnection.EndSQL;
//      showmessage(IfThen(isAktif, MSG_SUCCESS_AKTIVASI,MSG_SUCCESS_NOT_AKTIVASI));
      Result:= true;
    except
      myConnection.UndoSQL;
//      Alert(IfThen(isAktif, MSG_UNSUCCESS_AKTIVASI, MSG_UNSUCCESS_NOT_AKTIVASI));
      Result:= false;
    end;
  end;

  function ExecDelete(SQL: string): boolean;
  begin
    try
      myConnection.BeginSQL;
      myConnection.ExecSQL(SQL);
      myConnection.EndSQL;
     // showmessage(MSG_SUCCESS_DELETING);
      Result:= true;
      except
        myConnection.UndoSQL;
      //  showmessage(MSG_UNSUCCESS_DELETING);
        Result:= false;
    end;
  end;

  //alin tambah 051009 KHUSUS untuk PostGre
  function SelectTimePG(Field : String; is3 : Boolean = True) : String;
  begin
    Result := 'date_part('+FormatSQLString('hour')+', '+Field+')||'+FormatSQLString(':')+'||date_part('+FormatSQLString('minute')+', '+Field+')'+
              IfThen(is3, '||'+FormatSQLString(':')+'||date_part('+FormatSQLString('second')+', '+Field+')');
  end;

  function FormatSQLTimePG(ATime: TTime): string;
  begin
    Result := FormatDateTime('"TO_TIMESTAMP(''"hh:nn:ss"'', ''HH24:MI:SS'')"', ATime);
  end;

  function BufferToTimePG(ABuffer: variant): TTime;
  begin
    Result := StrToTime(BufferToStringDef(ABuffer,'0'));
  end;

  function SelectDateTimePG(Field : String; is3 : Boolean = True) : String;
  begin
    Result := 'date_part('+FormatSQLString('day')+', '+Field+')||'+FormatSQLString('/')+'||date_part('+FormatSQLString('month')+', '+Field+')'+
              '||'+FormatSQLString('/')+'||date_part('+FormatSQLString('year')+', '+Field+')||'+FormatSQLString(' ')+'||'+
              'date_part('+FormatSQLString('hour')+', '+Field+')||'+FormatSQLString(':')+'||date_part('+FormatSQLString('minute')+', '+Field+')'+
              IfThen(is3, '||'+FormatSQLString(':')+'||date_part('+FormatSQLString('second')+', '+Field+')');
  end;

  function FormatSQLDateTimePG(ADateTime: TDateTime): string;
  begin
    Result := FormatDateTime('"TO_TIMESTAMP(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"', ADateTime);
  end;

  function FormatSQLNULL: string;
  begin
    Result := '';
  end;

  function FormatSQLNULL2: string;
  begin
    Result := 'NULL';
  end;

  function FormatSQLOperator(operator:TSQLOperator):string;
  begin
    case operator of
      soGreaterThan : result := '> ';
      soGreaterThanEqualsTo : result:= '>= ';
      soEquals : result := '= ';
      soLessThan : result:= '< ';
      soLessThanEqualsTo : result:= '<=';
      else result := '= ';
    end;
  end;

  function FormatSQLBooleanOperator(operator:TBooleanOperator):string;
  begin
    case operator of
      boNone : result := '';
      boTrue : Result := 'True';
      boFalse: Result := 'False';
      else result := '= ';
    end;
  end;

  function FormatSQLNumber(AInteger: integer): string; overload;
  begin
    Result := IntToStr(AInteger);
  end;

  function FormatSQLNumber(AFloat: real): string; overload;
  begin
    if DecimalSeparator = '.' then
      Result := FloatToStr(AFloat)
    else
      Result := ReplaceSubStr(FloatToStr(AFloat),DecimalSeparator,'.');
  end;

  function FormatSQLNumber(ANumber: string): string; overload;
  begin
    Result := ReplaceSubStr(ReplaceSubStr(ANumber,ThousandSeparator,''),DecimalSeparator,'.');
  end;

  function FormatSQLString(AString: string): string;
  begin
    Result := QuotedStr(AString);
  end;

  function FormatSQLString(AChar: char): string; overload;
  begin
    Result := QuotedStr(AChar);
  end;

  function FormatSQLDate(ADate: TDate): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"dd/mm/yyyy"'', ''DD/MM/YYYY'')"', ADate);
  end;

  function FormatSQLTime(ATime: TTime): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"hh:nn:ss"'', ''HH24:MI:SS'')"', ATime);
  end;

  function FormatSQLDateTime(ADateTime: TDateTime): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"', ADateTime);
  end;

  function FormatSQLBoolean(ABoolean: boolean): string;
  begin
    if (ABoolean) then Result := FormatSQLNumber(TRUE_VALUE) else Result := FormatSQLNumber(FALSE_VALUE);
  end;
   // add by chan********
  function FormatSQLDateTime2(ADateTime: TDateTime):string;
  var y,m,d,hh,nn,ss,ms:word;
  begin
    DecodeTime(Now,hh,nn,ss,ms);
    DecodeDate(ADateTime,y,m,d);
    result :=  FormatDateTime('"TO_DATE(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"',EncodeDateTime(y,m,d,hh,nn,ss,ms));
  end;
{ ---------------------------------------------------------------------------- }

  function BufferToBoolean(ABuffer: variant): boolean;
  begin
    Result := BufferToBooleanDef(ABuffer,false);
  end;

  function BufferToBooleanDef(ABuffer: variant; ADefault: boolean): boolean;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := (BufferToInteger(ABuffer)=TRUE_VALUE)
    else
      Result := ADefault;
  end;

  function BufferToString(ABuffer: variant): string;
  begin
    Result := BufferToStringDef(ABuffer,'');
  end;

  function BufferToStringDef(ABuffer: variant; ADefault: string): string;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToInteger(ABuffer: variant): integer;
  begin
    Result := BufferToIntegerDef(ABuffer,0);
  end;

  function BufferToIntegerDef(ABuffer: variant; ADefault: integer): integer;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToFloat(ABuffer: variant): real;
  begin
    Result := BufferToFloatDef(ABuffer,0);
  end;

  function BufferToFloatDef(ABuffer: variant; ADefault: real): real;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToDateTime(ABuffer: variant): TDateTime;
  begin
    Result := BufferToDateTimeDef(ABuffer,0);
  end;

  function BufferToDateTimeDef(ABuffer: variant; ADefault:TDateTime): TDateTime;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function CreateADOObject(const ClassID: TGUID): IUnknown;
  var
    Status: HResult;
    FPUControlWord: Word;
  begin
    asm
      FNSTCW  FPUControlWord
    end;
    Status := CoCreateInstance(ClassID, nil, CLSCTX_INPROC_SERVER or
      CLSCTX_LOCAL_SERVER, IUnknown, Result);
    asm
      FNCLEX
      FLDCW FPUControlWord
    end;
    if (Status = REGDB_E_CLASSNOTREG) then
      raise Exception.CreateRes(@SADOCreateError) else
      OleCheck(Status);
  end;

class function TmyConnection.OpenRecordSet(ASQL: string): _RecordSet;
begin
  Result := CreateADOObject(CLASS_Recordset) as _Recordset;
  Result.Open(ASQL,myConnection.ADOConnection.ConnectionObject,
    adOpenForwardOnly,adLockPessimistic,adCmdText);
end;

function TmyConnection.AddParamDateTime2(AName: string;
  ADateTime: TDateTime; ADirection: TParameterDirection): TParameter;
var y,m,d,hh,nn,ss,ms:word;
begin
  DecodeTime(Now,hh,nn,ss,ms);
  DecodeDate(ADateTime,y,m,d);
  ADateTime := EncodeDateTime(y,m,d,hh,nn,ss,ms);
  Result := ADOCommand.Parameters.CreateParameter(AName, ftDateTime, ADirection, 0, ADateTime);
end;


function FormatSQLCompareOperator(operator:TCompareOperator):string;
begin
  case operator of
    coEquals  : Result := '=';
    coInEquals: Result := '<>';
    coInclude : Result := ' in ';
    else result := '= ';
  end;
end;

procedure SetingPrint(Asg: TAdvStringGrid);
begin
  with Asg.PrintSettings do begin
    Borders           := pbSingle;
    BorderStyle       := psSolid;
    FitToPage         := fpNever;
    Centered          := True;
    HeaderFont.Name   := 'Arial';
    HeaderFont.Size   := 10;
    HeaderFont.Style  := [fsBold];
    Font.Name         := 'Tahoma';
    Font.Size         := 8;

    FooterFont.Color  := clWhite;
    Date              := ppTopRight;
    DateFormat        := 'dd/mmm/yyyy';
    Title             := ppTopLeft;
    PagePrefix        := 'Halaman : ';
    HeaderSize        := 200;
    FooterSize        := 200;
    LeftSize          := 75;
    RightSize         := 75;
    ColumnSpacing     := 5;
    RowSpacing        := 5;
    TitleSpacing      := 5;
    //Orientation       :=
    //fo
    //NoAutoSize        := True;
    RepeatFixedRows   := True;
    //FitToPage         := fpAlways;
    Centered          := True;
  end;

end;



procedure TmyConnection.InsertDumpSQL(ASQL: string);
//var
 // vSql: String;
begin
  //vSql:= 'insert into dml_dump(dml_date, dml_sql)'+
  //       'values ('+FormatSQLDateTimeNow+','+
  //                  FormatSQLString(ASQL)+')';
  //myConnection.ADOConnection.Execute(vSql);
end;

function CreateNewSeq(NamaSeq : String; Table : string) : integer;
var sql : string;
begin
  sql := 'SELECT MAX('+NamaSeq+') FROM ' + Table;
  Result := getIntegerFromSQL(sql)+1;
end;

function GetCurrentSeq(NamaSeq, Table, ANomor : string) : integer;
var sql : string;
begin
  sql := 'SELECT MAX('+NamaSeq+') FROM ' + Table ;//+ ' WHERE nomor = '+FormatSQLString(ANomor);
  Result := getIntegerFromSQL(sql);
end;

function Get_NVL(Val1, Val2 : String) : String;
begin
  if DBisORCL then Result := ' NVL('+Val1+', '+Val2+') '
  else Result := ' coalesce('+Val1+', '+Val2+') ';
end;

function Get_Null(Field : String; Stat : Boolean) : String;
var yy, mm, dd : word;
    def_tgl : TDate;
begin
  yy := 1899; mm := 12; dd := 30;
  def_tgl := EncodeDate(yy, mm, dd);
  if DBisORCL then Result := ' '+Field+' IS '+IfThen(not Stat, 'NOT ')+'NULL '
  else Result := ' ('+Field+' IS '+IfThen(not Stat, 'NOT ')+'NULL '+IfThen(Stat, 'or ', 'and ')+Field + IfThen(Stat, ' = ', ' <> ')+FormatSQLDate(def_tgl)+') ';
end;

end.
