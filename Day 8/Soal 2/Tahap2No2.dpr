program Tahap2No2;

uses
  Forms,
  Input in 'Input.pas' {frmInput},
  InputDetail in 'InputDetail.pas' {frmInputDetail},
  Tampil in 'Tampil.pas' {frmTampil};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInput, frmInput);
  Application.CreateForm(TfrmInputDetail, frmInputDetail);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.Run;
end.
