unit InputDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TR_Data = Record
    nama : string;
    nilai : integer;
  end;
  ArrData = array of TR_Data; //memberi ALIAS / Nama Lain

  TfrmInputDetail = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    txtNama: TAdvEdit;
    txtNilai: TAdvEdit;
    btnOk: TButton;
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    tampung : TR_Data;
    Arr : ArrData;
    procedure Execute(idx : integer);
  end;

var
  frmInputDetail: TfrmInputDetail;

implementation

{$R *.dfm}

{ TfrmInputDetail }

procedure TfrmInputDetail.Execute(idx: integer);
begin
  InitForm;
  ShowModal;
  //isi ke array
  Arr[idx].nama := trim(txtNama.Text);
  Arr[idx].nilai := txtNilai.IntValue;
end;

procedure TfrmInputDetail.InitForm;
begin
  txtNama.Text := '';
  txtNilai.IntValue := 0;
end;

procedure TfrmInputDetail.btnOkClick(Sender: TObject);
begin
  if Trim(txtNama.Text) = '' then begin
    ShowMessage('Nama tidak boleh kosong.');
    txtNama.SetFocus;
  end else if txtNilai.IntValue = 0 then begin
    ShowMessage('Nilai tidak boleh 0.');
    txtNilai.SetFocus;
  end else Close;
end;

end.
