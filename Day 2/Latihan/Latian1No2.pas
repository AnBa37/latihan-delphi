unit Latian1No2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmInput = class(TForm)
    MainPanel: TPanel;
    txtIsiText1: TAdvEdit;
    txtIsiText2: TAdvEdit;
    btnProses: TButton;
    btnCancel: TButton;
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
    function CekInteger(AString: string): boolean; //dalam bentuk FUNGSI
    procedure CekInteger2 (AString : string; var Hasil : boolean;)
  public
    { Public declarations }

  begin
    try
      StrToInt(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  end;

var
  frmInput: TfrmInput;

implementation

{$R *.dfm}

procedure TfrmInput.btnCancelClick(Sender: TObject);
begin
close;
end;

end.
