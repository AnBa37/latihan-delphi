unit FormTampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmTampil = class(TForm)
    PanelTampil: TPanel;
    txtIsiText: TAdvEdit;
    btnIsiText: TButton;
    btnProses: TButton;
    btnKembali: TButton;
    procedure btnIsiTextClick(Sender: TObject);
    procedure btnKembaliClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTampil: TfrmTampil;

implementation

uses LatihanNo1;

{$R *.dfm}

procedure TfrmTampil.btnIsiTextClick(Sender: TObject);
begin
 if txtIsiText.Text = '' then
 txtIsiText.Text := 'Bank Mega';
end;

procedure TfrmTampil.btnKembaliClick(Sender: TObject);
begin
 close;
 frmFormUtama.show;
 
end;

end.
