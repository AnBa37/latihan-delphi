object Form1: TForm1
  Left = 534
  Top = 324
  Width = 384
  Height = 107
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object txtIsi: TAdvEdit
    Left = 40
    Top = 8
    Width = 289
    Height = 21
    AutoFocus = False
    EditAlign = eaLeft
    EditType = etString
    ErrorColor = clRed
    ErrorFontColor = clWhite
    ExcelStyleDecimalSeparator = False
    Flat = False
    FlatLineColor = clBlack
    FlatParentColor = True
    FocusAlign = eaDefault
    FocusBorder = False
    FocusColor = clWindow
    FocusFontColor = clWindowText
    FocusLabel = False
    FocusWidthInc = 0
    ModifiedColor = clHighlight
    DisabledColor = clSilver
    URLColor = clBlue
    ReturnIsTab = False
    LengthLimit = 0
    TabOnFullLength = False
    Precision = 0
    LabelPosition = lpLeftTop
    LabelMargin = 4
    LabelTransparent = False
    LabelAlwaysEnabled = False
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Persistence.Enable = False
    Persistence.Location = plInifile
    Color = clWindow
    Enabled = True
    HintShowLargeText = False
    OleDropTarget = False
    OleDropSource = False
    Signed = False
    TabOrder = 0
    Transparent = False
    Visible = True
  end
  object btnIsiText: TButton
    Left = 40
    Top = 40
    Width = 75
    Height = 25
    Caption = '&Isi Text'
    TabOrder = 1
  end
  object btnProcessTampil: TButton
    Left = 152
    Top = 40
    Width = 75
    Height = 25
    Caption = '&Process'
    TabOrder = 2
  end
  object btnTutupTampil: TButton
    Left = 256
    Top = 40
    Width = 75
    Height = 25
    Caption = '&Tutup'
    TabOrder = 3
  end
end
