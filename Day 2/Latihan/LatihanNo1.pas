unit LatihanNo1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TfrmFormUtama = class(TForm)
    MainPanel: TPanel;
    txtSelamat_Datang: TLabel;
    btnProcess: TButton;
    btnTutup: TButton;
    procedure btnProcessClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;


  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmFormUtama: TfrmFormUtama;

implementation

uses Unit2;




{$R *.dfm}

procedure TfrmFormUtama.btnProcessClick(Sender: TObject);
begin
  frmTampil.show;
end;

procedure TfrmFormUtama.btnTutupClick(Sender: TObject);
begin
close;
end;

end.
