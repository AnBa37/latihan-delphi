object frmFormUtama: TfrmFormUtama
  Left = 451
  Top = 225
  Width = 362
  Height = 116
  Caption = 'Form Utama'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 346
    Height = 77
    Align = alClient
    TabOrder = 0
    object txtSelamat_Datang: TLabel
      Left = 56
      Top = 8
      Width = 253
      Height = 33
      Caption = 'SELAMAT DATANG'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -29
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object btnProcess: TButton
      Left = 80
      Top = 40
      Width = 75
      Height = 25
      Caption = '&Process'
      TabOrder = 0
      OnClick = btnProcessClick
    end
    object btnTutup: TButton
      Left = 192
      Top = 40
      Width = 75
      Height = 25
      Caption = '&Tutup'
      TabOrder = 1
      OnClick = btnTutupClick
    end
  end
end
