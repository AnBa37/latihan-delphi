unit InputHitungLuas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmInputHitungLuas = class(TForm)
    mainPanel: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    txtPanjang: TAdvEdit;
    txtLebar: TAdvEdit;
    txtTotal: TAdvEdit;
    btnProses: TButton;
    btnArray: TButton;
    btnArrayStatis: TButton;
    cmb1: TComboBox;
    cmb2: TComboBox;
    btnCombo: TButton;
    procedure btnProsesClick(Sender: TObject);
    procedure btnArrayClick(Sender: TObject);
    procedure btnArrayStatisClick(Sender: TObject);
  private
    { Private declarations }
    arrUtkCombo: array of String;
    function hitungLuas(pjg, lbr : integer) : integer;
    procedure hitung_Luas (var luas : integer; pjg, lbr : integer);
  public
    { Public declarations }
  end;

var
  frmInputHitungLuas: TfrmInputHitungLuas;

implementation

{$R *.dfm}

{ TfrmInputHitungLuas }

function TfrmInputHitungLuas.hitungLuas(pjg, lbr: integer): integer;
begin
  result := pjg * lbr;
end;

procedure TfrmInputHitungLuas.btnProsesClick(Sender: TObject);
var aluas : integer;
begin
  txtTotal.IntValue := hitungLuas(txtPanjang.IntValue, txtLebar.IntValue);
  showmessage('contoh fungsi = '+inttostr(txtTotal.IntValue));

  hitung_Luas(aLuas, txtPanjang.IntValue, txtLebar.IntValue);
  txtTotal.IntValue := aluas;
  showmessage('contoh procedure = '+inttostr(aluas));
end;



procedure TfrmInputHitungLuas.hitung_Luas(var luas : integer; pjg, lbr: integer);
begin
  luas := pjg - lbr;
end;

procedure TfrmInputHitungLuas.btnArrayClick(Sender: TObject);
var arrData : array of String; i : integer; data: String;
begin
 SetLength(arrData, txtPanjang.IntValue); data := 'contoh array dinamis: ' +#13;
 SetLength(arrUtkCombo, txtPanjang.IntValue);
 for i := 0 to length(arrData)-1 do begin
   arrData[i] := inttostr((i + 1) * (i + 1));
   //arrUtkCombo[i] :=
   ShowMessage(arrData[i]);
   data := data + (arrData[i]) + #13;
 end;
 ShowMessage(data);
end;

procedure TfrmInputHitungLuas.btnArrayStatisClick(Sender: TObject);
begin
 var arrAngka : array of Integer; i : integer; data: string;
begin
 SetLength(arrAngka, 5); data := 'Contoh array statis: ' +#13;
 for i := 0 to length(arrangka)-1 do begin
   arrAngka[i] := (i + 1) * (i + 1);
   ShowMessage(inttostr(arrAngka[i]));
   data := data + (inttostr(arrAngka[i])) + #13;
 end;
 ShowMessage(data);
end;

procedure TfrmInputHitungLuas.initform;
begin
txtPanjang.IntValue := 0;
txtLebar.IntValue :=0;
txtTotal.IntValue := 0;
cmb1.ItemIndex := 0;
cmb2.ItemIndex := -1;

end.
