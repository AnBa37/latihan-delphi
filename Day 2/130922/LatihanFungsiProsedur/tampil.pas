unit tampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmTampil = class(TForm)
    mainPanel: TPanel;
    cmbContoh: TComboBox;
    cmbTampilData: TComboBox;
    btnProses: TButton;
    procedure btnProsesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTampil: TfrmTampil;

implementation

uses LatihanFungsiProsedur;

{$R *.dfm}

procedure TfrmTampil.btnProsesClick(Sender: TObject);
var i, pjgArray : integer;
begin
  pjgArray := length(frmLatihanFungsiProsedur.arDataBaru);

  if pjgArray = 0 then begin
    showmessage('Data masih kosong.');
    cmbTampilData.SetFocus;
    exit;
  end;

  cmbTampilData.Items.clear;
  for i := 0 to pjgArray-1 do begin
    cmbTampilData.Items.Add(inttostr(frmLatihanFungsiProsedur.arDataBaru[i]));
  end;   
end;

procedure TfrmTampil.FormShow(Sender: TObject);
begin
  cmbContoh.ItemIndex := 0;
  cmbTampilData.ItemIndex := -1;
end;

end.
