unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    txtInput: TAdvEdit;
    btnIsiData: TButton;
    btnTampil: TButton;
    btnMin: TButton;
    btnMax: TButton;
    btnTutup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure btnIsiDataClick(Sender: TObject);
    procedure txtInputExit(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
    procedure btnMinClick(Sender: TObject);
    procedure btnMaxClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

uses InputDetail, Tampil;

{$R *.dfm}

{ TfrmInput }

procedure TfrmInput.InitForm;
begin
  txtInput.IntValue := 0;
  setlength(frmInputDetail.Arr, 0);
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInput.btnIsiDataClick(Sender: TObject);
var i : integer;
begin
  if (txtInput.IntValue >= 2) and (txtInput.IntValue <= 20) then begin
    setlength(frmInputDetail.Arr, txtInput.IntValue); //set panjang array
    //tampilkan form input sebanyak data input user
    for i := 0 to txtInput.IntValue-1 do frmInputDetail.Execute(i);
  end else ShowMessage('Input antara 2 - 20.');  
end;

procedure TfrmInput.txtInputExit(Sender: TObject);
begin
  setlength(frmInputDetail.Arr, 0);
end;

procedure TfrmInput.btnTampilClick(Sender: TObject);
begin
  if length(frmInputDetail.Arr) > 0 then //pastikan arraynya ada
    frmTampil.Execute
  else ShowMessage('Panjang array masih 0.');
end;

procedure TfrmInput.btnMinClick(Sender: TObject);
var min, i : integer;
    nama : string;             
begin
  if length(frmInputDetail.Arr) > 0 then begin //pastikan arraynya ada
    min := frmInputDetail.Arr[0].nilai;
    nama := frmInputDetail.Arr[0].nama;
    for i := 1 to length(frmInputDetail.Arr)-1 do begin
      if frmInputDetail.Arr[i].nilai < min then begin
        min := frmInputDetail.Arr[i].nilai;
        nama := frmInputDetail.Arr[i].nama;
      end;
    end;
    ShowMessage('Min : ' + nama + ' --> ' +IntToStr(min));
  end else ShowMessage('Panjang array masih 0.');
end;

procedure TfrmInput.btnMaxClick(Sender: TObject);
var max, i : integer;
    nama : string;
begin
  if length(frmInputDetail.Arr) > 0 then begin //pastikan arraynya ada
    max := frmInputDetail.Arr[0].nilai;
    nama := frmInputDetail.Arr[0].nama;
    for i := 1 to length(frmInputDetail.Arr)-1 do begin
      if frmInputDetail.Arr[i].nilai > max then begin
        max := frmInputDetail.Arr[i].nilai;
        nama := frmInputDetail.Arr[i].nama;
      end;
    end;
    ShowMessage('Max : ' + nama + ' --> ' +IntToStr(max));
  end else ShowMessage('Panjang array masih 0.');
end;

end.
