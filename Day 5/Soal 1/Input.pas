unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit;

type
//bikin record
  TR_Data = Record
    nama : string[30];
    alamat : string[150];
    no_telp : string[20];
    tgl_lahir : TDate;
    keterangan : string[250];
  end;

  TfrmInput = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtNama: TAdvEdit;
    txtAlamat: TAdvEdit;
    txtNoTelp: TAdvEdit;
    dtpTglLahir: TDateTimePicker;
    mmKet: TMemo;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    data : TR_Data;
  end;

var
  frmInput: TfrmInput;

implementation

uses Tampil;

{$R *.dfm}

{ TfrmInput }

procedure TfrmInput.InitForm;
begin
  txtNama.Text := '';
  txtAlamat.Text := '';
  txtNoTelp.Text := '';
  dtpTglLahir.Date := Now;
  mmKet.Lines.Text := '';
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInput.btnTampilClick(Sender: TObject);
begin
  //isi dulu recordnya
  data.nama := Trim(txtNama.Text);
  data.alamat := Trim(txtAlamat.Text);
  data.no_telp := Trim(txtNoTelp.Text);
  data.tgl_lahir := dtpTglLahir.Date;
  data.keterangan := Trim(mmKet.Lines.Text);
  frmTampil.Execute;
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

end.
