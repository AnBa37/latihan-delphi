unit InputAwal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type

  TfrmInputData = class(TForm)
    MainPanel: TPanel;
    txtArray1: TAdvEdit;
    txtArray2: TAdvEdit;
    btnIsi1: TButton;
    btnIsi2: TButton;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnIsi1Click(Sender: TObject);
    procedure btnTampilClick(Sender: TObject);
    procedure btnIsi2Click(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    procedure Initform;
  public
    { Public declarations }
  end;

var
  frmInputData: TfrmInputData;

implementation

uses Tampil , IsiDataDetail;

{$R *.dfm}

{ TForm1 }

procedure TfrmInputData.Initform;
begin
 txtArray1.IntValue := 0;
 txtArray2.IntValue := 0;


end;

procedure TfrmInputData.FormShow(Sender: TObject);
begin
 InitForm;
end;

procedure TfrmInputData.btnIsi1Click(Sender: TObject);
var i : integer;
begin

 if(txtArray1.IntValue >= 2) and (txtArray1.IntValue <= 5) then begin
   SetLength(frmIsiDataDetail.Arr , txtArray1.IntValue);

  for i := 0 to txtArray1.IntValue-1 do frmIsiDataDetail.Execute(i);
 end else ShowMessage('Input antara 2-5');


end;

procedure TfrmInputData.btnTampilClick(Sender: TObject);
begin
 frmTampil.show;
end;

procedure TfrmInputData.btnIsi2Click(Sender: TObject);
var i : integer;
begin
  if(txtArray2.IntValue >= 2) and (txtArray2.IntValue <= 5) then begin
   SetLength(frmIsiDataDetail.Arr2 , txtArray2.IntValue);

  for i := 0 to txtArray2.IntValue-1 do frmIsiDataDetail.Execute(i);
 end else ShowMessage('Input antara 2-5');
end;

procedure TfrmInputData.btnTutupClick(Sender: TObject);
begin
 close;
end;

end.
