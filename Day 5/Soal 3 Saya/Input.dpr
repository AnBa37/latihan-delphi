program Input;

uses
  Forms,
  InputAwal in 'InputAwal.pas' {frmInputData},
  IsiDataDetail in 'IsiDataDetail.pas' {frmIsiDataDetail},
  Tampil in 'Tampil.pas' {frmTampil};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInputData, frmInputData);
  Application.CreateForm(TfrmIsiDataDetail, frmIsiDataDetail);
  Application.CreateForm(TfrmTampil, frmTampil);
  Application.Run;
end.
