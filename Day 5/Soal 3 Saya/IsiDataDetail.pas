unit IsiDataDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit;

type
  tr_data = record
  nama : string;
  nilai : integer;
  tanggal : TDate;
  end;

  ArrData = array of tr_data;
  //ArrData2 = array of tr_data;

  TfrmIsiDataDetail = class(TForm)
    lblNama: TLabel;
    lblNilai: TLabel;
    lblTanggal: TLabel;
    txtNama: TAdvEdit;
    txtNilai: TAdvEdit;
    dtpTanggal: TDateTimePicker;
    btnOke: TButton;
    procedure btnOkeClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    data : tr_data;
    Arr : ArrData;
    Arr2: ArrData;
    procedure Execute(idx : integer);
  end;

var
  frmIsiDataDetail: TfrmIsiDataDetail;

implementation

{$R *.dfm}

{ TfrmIsiDataDetail }

procedure TfrmIsiDataDetail.Execute(idx : integer);
begin
 InitForm;
 ShowModal;

 //untuk array
 Arr[idx].nama := trim(txtNama.Text);
 Arr[idx].nilai := txtNilai.IntValue;
 Arr[idx].tanggal := dtpTanggal.Date;
end;

procedure TfrmIsiDataDetail.InitForm;
begin
 txtNama.Text := '';
 txtNilai.IntValue := 0;
 dtpTanggal.Date := now;
end;

procedure TfrmIsiDataDetail.btnOkeClick(Sender: TObject);
begin
 if trim(txtNama.Text) = '' then begin
 ShowMessage('Nama Tidak Boleh Kosong.');
 txtNama.SetFocus;
 end else if txtNilai.IntValue  = 0 then begin
 ShowMessage('Nilai Tidak Boleh 0.');
 txtNilai.SetFocus;
 end else close;
end;

end.
