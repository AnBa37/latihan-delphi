unit Tampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmTampil = class(TForm)
    PanelTampil: TPanel;
    btnTampilSemuaData: TButton;
    btnTampikanNama: TButton;
    btnTampilkanNilai: TButton;
    btnTampilkanTanggal: TButton;
    procedure btnTampilSemuaDataClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTampil: TfrmTampil;

implementation

uses IsiDataDetail;

{$R *.dfm}

{ TfrmTampil }

procedure TfrmTampil.Execute;
begin
 InitForm;
 ShowModal;
end;

procedure TfrmTampil.InitForm;
begin

end;

procedure TfrmTampil.btnTampilSemuaDataClick(Sender: TObject);
var i : integer;
    hasil : string;
    //j : integer;
    //hasil2 : string;
begin
  hasil := '';
  for i := 0 to length(frmIsiDataDetail.Arr)-1 do begin
  hasil := hasil + frmIsiDataDetail.Arr[i].nama + '  ' + inttostr(frmIsiDataDetail.Arr[i].nilai)+ '  ' + datetostr(frmIsiDataDetail.Arr[i].tanggal)+#13;
  //for j := 0 to length(frmIsiDataDetail.Arr2)-1 do begin
  //hasil2 := hasil2 + frmIsiDataDetail.Arr2[i].nama + '  ' + inttostr(frmIsiDataDetail.Arr2[i].nilai)+ '  ' + datetostr(frmIsiDataDetail.Arr2[i].tanggal)+#13;
  end;
  ShowMessage(hasil);
  //ShowMessage(hasil2);
end;

end.
