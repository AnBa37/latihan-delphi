object frmTampil: TfrmTampil
  Left = 192
  Top = 125
  Width = 292
  Height = 197
  Caption = 'Tampil'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTampil: TPanel
    Left = 0
    Top = 0
    Width = 276
    Height = 158
    Align = alClient
    TabOrder = 0
    object btnTampilSemuaData: TButton
      Left = 16
      Top = 16
      Width = 233
      Height = 25
      Caption = 'Tampikan SEMUA DATA'
      TabOrder = 0
      OnClick = btnTampilSemuaDataClick
    end
    object btnTampikanNama: TButton
      Left = 16
      Top = 48
      Width = 233
      Height = 25
      Caption = 'Tampilkan Berdasarkan Nama'
      TabOrder = 1
    end
    object btnTampilkanNilai: TButton
      Left = 16
      Top = 80
      Width = 233
      Height = 25
      Caption = 'Tampikan Berdasarkan Nilai'
      TabOrder = 2
    end
    object btnTampilkanTanggal: TButton
      Left = 16
      Top = 112
      Width = 233
      Height = 25
      Caption = 'Tampilkan Berdasarkan Tanggal'
      TabOrder = 3
    end
  end
end
