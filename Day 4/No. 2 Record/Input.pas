unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
 
  TfrmInputData = class(TForm)
    MainPanel: TPanel;
    txtUtkArr: TAdvEdit;
    btnIsiData: TButton;
    btnTampil: TButton;
    btnMin: TButton;
    btnMax: TButton;
    btnTutup: TButton;
    procedure btnIsiDataClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure txtUtkArrKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInputData: TfrmInputData;

implementation

uses InputDetail;

{$R *.dfm}

procedure TfrmInputData.btnIsiDataClick(Sender: TObject);
begin
 frmInputDetail.show;
end;

procedure TfrmInputData.btnTutupClick(Sender: TObject);
begin
 close;
end;

procedure TfrmInputData.txtUtkArrKeyPress(Sender: TObject; var Key: Char);
begin
 if not (key in['0'..'9',#8,#13]) then key = #0;
end;

end.
