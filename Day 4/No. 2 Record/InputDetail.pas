unit InputDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
 tr_detail = record
 nama : string[100];
 nilai : string[10];
 end;
 ardetail = array of tr_detail;

  TfrmInputDetail = class(TForm)
    lblNama: TLabel;
    lblNilai: TLabel;
    txtNama: TAdvEdit;
    TxtNilai: TAdvEdit;
    btnOk: TButton;
    procedure TxtNilaiKeyPress(Sender: TObject; var Key: Char);
    procedure txtNamaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    //data : tr_detail;
    procedure initform;
  public
    { Public declarations }
  end;

var
  frmInputDetail: TfrmInputDetail;

implementation

{$R *.dfm}

{ TfrmInputDetail }

procedure TfrmInputDetail.initform;
begin
 initform;
 txtNama.Text := '';
 TxtNilai.Text := '';
end;

procedure TfrmInputDetail.TxtNilaiKeyPress(Sender: TObject; var Key: Char);
begin
   if not(key in['0'..'9',#8,#13]) then key := #0;
end;

procedure TfrmInputDetail.txtNamaKeyPress(Sender: TObject; var Key: Char);
begin
 if not(key in['a'..'z',#8,#13]) then key := #0;
end;

end.
