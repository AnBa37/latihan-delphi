program InputMaxMin;

uses
  Forms,
  Input in 'Input.pas' {frmInputData},
  InputDetail in 'InputDetail.pas' {frmInputDetail};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInputData, frmInputData);
  Application.CreateForm(TfrmInputDetail, frmInputDetail);
  Application.Run;
end.
