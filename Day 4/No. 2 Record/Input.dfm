object frmInputData: TfrmInputData
  Left = 656
  Top = 230
  BorderStyle = bsDialog
  Caption = 'Input'
  ClientHeight = 225
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 330
    Height = 225
    Align = alClient
    TabOrder = 0
    object txtUtkArr: TAdvEdit
      Left = 32
      Top = 24
      Width = 273
      Height = 21
      AutoFocus = False
      EditAlign = eaLeft
      EditType = etString
      ErrorColor = clRed
      ErrorFontColor = clWhite
      ExcelStyleDecimalSeparator = False
      Flat = False
      FlatLineColor = clBlack
      FlatParentColor = True
      FocusAlign = eaDefault
      FocusBorder = False
      FocusColor = clWindow
      FocusFontColor = clWindowText
      FocusLabel = False
      FocusWidthInc = 0
      ModifiedColor = clHighlight
      DisabledColor = clSilver
      URLColor = clBlue
      ReturnIsTab = False
      LengthLimit = 0
      TabOnFullLength = False
      Precision = 0
      LabelPosition = lpLeftTop
      LabelMargin = 4
      LabelTransparent = False
      LabelAlwaysEnabled = False
      LabelFont.Charset = DEFAULT_CHARSET
      LabelFont.Color = clWindowText
      LabelFont.Height = -11
      LabelFont.Name = 'MS Sans Serif'
      LabelFont.Style = []
      Persistence.Enable = False
      Persistence.Location = plInifile
      Color = clWindow
      Enabled = True
      HintShowLargeText = False
      OleDropTarget = False
      OleDropSource = False
      Signed = False
      TabOrder = 0
      Transparent = False
      Visible = True
      OnKeyPress = txtUtkArrKeyPress
    end
    object btnIsiData: TButton
      Left = 40
      Top = 72
      Width = 75
      Height = 25
      Caption = '&Isi Data'
      TabOrder = 1
      OnClick = btnIsiDataClick
    end
    object btnTampil: TButton
      Left = 200
      Top = 72
      Width = 75
      Height = 25
      Caption = '&Tampil'
      TabOrder = 2
    end
    object btnMin: TButton
      Left = 40
      Top = 112
      Width = 75
      Height = 25
      Caption = 'Min'
      TabOrder = 3
    end
    object btnMax: TButton
      Left = 200
      Top = 112
      Width = 75
      Height = 25
      Caption = 'Max'
      TabOrder = 4
    end
    object btnTutup: TButton
      Left = 120
      Top = 160
      Width = 75
      Height = 25
      Caption = 'Tutup'
      TabOrder = 5
      OnClick = btnTutupClick
    end
  end
end
