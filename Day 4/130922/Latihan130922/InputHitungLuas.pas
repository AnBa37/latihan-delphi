unit InputHitungLuas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit, ExtCtrls;

type
  TfrmInputHitungLuas = class(TForm)
    mainPanel: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    txtPanjang: TAdvEdit;
    txtLebar: TAdvEdit;
    txtTotal: TAdvEdit;
    btnProses: TButton;
    procedure btnProsesClick(Sender: TObject);
  private
    { Private declarations }
    function hitungLuas(pjg, lbr : integer) : integer;
    procedure hitung_Luas (var luas : integer; pjg, lbr : integer);
  public
    { Public declarations }
  end;

var
  frmInputHitungLuas: TfrmInputHitungLuas;

implementation

{$R *.dfm}

{ TfrmInputHitungLuas }

function TfrmInputHitungLuas.hitungLuas(pjg, lbr: integer): integer;
begin
  result := pjg * lbr;
end;

procedure TfrmInputHitungLuas.btnProsesClick(Sender: TObject);
var aluas : integer;
begin
  txtTotal.IntValue := hitungLuas(txtPanjang.IntValue, txtLebar.IntValue);
  showmessage('contoh fungsi = '+inttostr(txtTotal.IntValue));

  hitung_Luas(aLuas, txtPanjang.IntValue, txtLebar.IntValue);
  txtTotal.IntValue := aluas;
  showmessage('contoh procedure = '+inttostr(aluas));
end;



procedure TfrmInputHitungLuas.hitung_Luas(var luas : integer; pjg, lbr: integer);
begin
  luas := pjg - lbr;
end;

end.
