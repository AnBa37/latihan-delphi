object frmTampil: TfrmTampil
  Left = 175
  Top = 125
  Caption = 'Tampil'
  ClientHeight = 441
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mainPanel: TPanel
    Left = 0
    Top = 0
    Width = 680
    Height = 441
    Align = alClient
    TabOrder = 0
    object cmbContoh: TComboBox
      Left = 120
      Top = 48
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Orion'
        'IT'
        'Solution')
    end
    object cmbTampilData: TComboBox
      Left = 120
      Top = 96
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object btnProses: TButton
      Left = 136
      Top = 168
      Width = 75
      Height = 25
      Caption = '&Proses'
      TabOrder = 2
      OnClick = btnProsesClick
    end
  end
end
