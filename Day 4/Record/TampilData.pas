unit TampilData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmTampilData = class(TForm)
    lblnama: TLabel;
    lblalamat: TLabel;
    lblnotelp: TLabel;
    lbltgl_lahir: TLabel;
    lblketerangan: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    bntTutup: TButton;
    procedure bntTutupClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTampilData: TfrmTampilData;

implementation

uses InputData;

{$R *.dfm}

procedure TfrmTampilData.bntTutupClick(Sender: TObject);
begin
 Close;
 frmInputData.Show;
end;

end.
