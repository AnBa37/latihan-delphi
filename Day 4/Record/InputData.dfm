�
 TFRMINPUTDATA 0�  TPF0TfrmInputDatafrmInputDataLeft�Top� BorderStylebsDialogCaption
Input DataClientHeightMClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnShowFormShowPixelsPerInch`
TextHeight TPanel	MainPanelLeft Top WidthHeightMAlignalClientTabOrder  TLabel	cptAlamatLeftTopDWidth HeightCaptionAlamat  TLabel	cptNoTelpLeftToplWidth)HeightCaptionNo. Telp  TLabelcptTgl_LahirLeftTop� Width)HeightCaption	Tgl Lahir  TLabelcptKeteranganLeftTop� Width7HeightCaption
Keterangan  TLabelcptNamaLeftTop$WidthHeightCaptionNama  TAdvEdit	txtAlamatLeftHTop@WidthyHeight	AutoFocus	EditAligneaLeftEditTypeetString
ErrorColorclRedErrorFontColorclWhiteExcelStyleDecimalSeparatorFlatFlatLineColorclBlackFlatParentColor	
FocusAlign	eaDefaultFocusBorder
FocusColorclWindowFocusFontColorclWindowText
FocusLabelFocusWidthInc ModifiedColorclHighlightDisabledColorclSilverURLColorclBlueReturnIsTabLengthLimit TabOnFullLength	Precision LabelPosition	lpLeftTopLabelMarginLabelTransparentLabelAlwaysEnabledLabelFont.CharsetDEFAULT_CHARSETLabelFont.ColorclWindowTextLabelFont.Height�LabelFont.NameMS Sans SerifLabelFont.Style Persistence.EnablePersistence.Location	plInifileColorclWindowEnabled	HintShowLargeTextOleDropTargetOleDropSourceSignedTabOrder TransparentVisible	  TAdvEdit	txtNoTelpLeftHTophWidthyHeight	AutoFocus	EditAligneaLeftEditTypeetString
ErrorColorclRedErrorFontColorclWhiteExcelStyleDecimalSeparatorFlatFlatLineColorclBlackFlatParentColor	
FocusAlign	eaDefaultFocusBorder
FocusColorclWindowFocusFontColorclWindowText
FocusLabelFocusWidthInc ModifiedColorclHighlightDisabledColorclSilverURLColorclBlueReturnIsTabLengthLimit TabOnFullLength	Precision LabelPosition	lpLeftTopLabelMarginLabelTransparentLabelAlwaysEnabledLabelFont.CharsetDEFAULT_CHARSETLabelFont.ColorclWindowTextLabelFont.Height�LabelFont.NameMS Sans SerifLabelFont.Style Persistence.EnablePersistence.Location	plInifileColorclWindowEnabled	HintShowLargeTextOleDropTargetOleDropSourceSignedTabOrderTransparentVisible	
OnKeyPresstxtNoTelpKeyPress  TDateTimePickerdtpTgl_LahirLeftHTop� Width� HeightCalAlignmentdtaLeftDate (F�r�@Time (F�r�@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder  TMemommKeteranganLeftHTop� Width� HeightYLines.StringsmmKeterangan 
ScrollBarsssBothTabOrder  TAdvEdittxtNamaLeftHTop WidthyHeight	AutoFocus	EditAligneaLeftEditTypeetString
ErrorColorclRedErrorFontColorclWhiteExcelStyleDecimalSeparatorFlatFlatLineColorclBlackFlatParentColor	
FocusAlign	eaDefaultFocusBorder
FocusColorclWindowFocusFontColorclWindowText
FocusLabelFocusWidthInc ModifiedColorclHighlightDisabledColorclSilverURLColorclBlueReturnIsTab	LengthLimit TabOnFullLength	Precision LabelPosition	lpLeftTopLabelMarginLabelTransparentLabelAlwaysEnabledLabelFont.CharsetDEFAULT_CHARSETLabelFont.ColorclWindowTextLabelFont.Height�LabelFont.NameMS Sans SerifLabelFont.Style Persistence.EnablePersistence.Location	plInifileColorclWindowEnabled	HintShowLargeTextOleDropTargetOleDropSourceSignedTabOrderTransparentVisible	  TButton	btnTampilLeft(Top WidthKHeightCaption&TampilTabOrderOnClickbtnTampilClick  TButtonbtnTutupLeft� Top WidthKHeightCaption&TutupTabOrderOnClickbtnTutupClick    