program LatihanRecord;

uses
  Forms,
  InputData in 'InputData.pas' {frmInputData},
  TampilData in 'TampilData.pas' {frmTampilData};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmInputData, frmInputData);
  Application.CreateForm(TfrmTampilData, frmTampilData);
  Application.Run;
end.
