unit InputData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit, ExtCtrls;

type
  tr_identitas = record
    nama : string[30];
    alamat : string[150];
    notelp : string[20];
    tgl_lahir : tdate;
    keterangan : string[120];
    end;

  TfrmInputData = class(TForm)
    MainPanel: TPanel;
    cptAlamat: TLabel;
    cptNoTelp: TLabel;
    cptTgl_Lahir: TLabel;
    cptKeterangan: TLabel;
    txtAlamat: TAdvEdit;
    txtNoTelp: TAdvEdit;
    dtpTgl_Lahir: TDateTimePicker;
    mmKeterangan: TMemo;
    cptNama: TLabel;
    txtNama: TAdvEdit;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnTampilClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure txtNoTelpKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    data : tr_identitas;
    procedure initform;
    //procedure isi_record;
    //procedure isi_array_record;
  public
    { Public declarations }
    procedure execute;

  end;

var
  frmInputData: TfrmInputData;

implementation

uses TampilData;

{$R *.dfm}

procedure TfrmInputData.initform;
begin
 initform;
 txtNama.Text := '';
 txtAlamat.Text := '';
 txtNoTelp.Text := '';
 dtpTgl_Lahir.Date := now;
 mmKeterangan.Lines.Text := '';

end;

procedure TfrmInputData.btnTampilClick(Sender: TObject);
begin

 if txtNama.Text = '' then begin
 ShowMessage('Nama Tolong Diisi.');
 txtNama.SetFocus;
 exit;
 end;

 if txtAlamat.Text = '' then begin
 ShowMessage('Alamat Tolong Diisi.');
 txtAlamat.SetFocus;
 exit;
 end;
 
 data.nama := txtNama.Text;
 data.alamat := txtAlamat.Text;
 data.notelp := txtNoTelp.Text;
 data.tgl_lahir:= dtpTgl_Lahir.date;
 data.keterangan := mmKeterangan.Lines.Text;

 //if txtNama.Text = '' and txtAlamat.Text = '' then begin
 //ShowMessage('Nama Dan Alamat Tolong Diisi');
 //end;

 frmTampilData.lblnama.caption := data.nama ;
 frmTampilData.lblalamat.caption := data.alamat;
 frmTampilData.lblnotelp.caption := data.notelp;
 frmTampilData.lbltgl_lahir.caption:= datetostr(dtpTgl_Lahir.DateTime);
 frmTampilData.lblketerangan.caption := mmKeterangan.Lines.Text ;
   frmTampilData.Show;
end;

procedure TfrmInputData.execute;
begin

end;


//procedure TfrmInputData.isi_record;
//begin
//end;

procedure TfrmInputData.FormShow(Sender: TObject);
begin
 execute;
end;

procedure TfrmInputData.btnTutupClick(Sender: TObject);
begin
 close;
end;

procedure TfrmInputData.txtNoTelpKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in['0'..'9',#8,#13]) then key := #0;
end;

end.
