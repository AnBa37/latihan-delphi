object frmTampilData: TfrmTampilData
  Left = 457
  Top = 111
  BorderStyle = bsDialog
  Caption = 'Tampil Data'
  ClientHeight = 190
  ClientWidth = 188
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblnama: TLabel
    Left = 100
    Top = 32
    Width = 36
    Height = 13
    Caption = 'lblnama'
  end
  object lblalamat: TLabel
    Left = 99
    Top = 56
    Width = 41
    Height = 13
    Caption = 'lblalamat'
  end
  object lblnotelp: TLabel
    Left = 101
    Top = 80
    Width = 39
    Height = 13
    Caption = 'lblnotelp'
  end
  object lbltgl_lahir: TLabel
    Left = 94
    Top = 104
    Width = 46
    Height = 13
    Caption = 'lbltgl_lahir'
  end
  object lblketerangan: TLabel
    Left = 84
    Top = 128
    Width = 64
    Height = 13
    Caption = 'lblketerangan'
  end
  object Label1: TLabel
    Left = 34
    Top = 32
    Width = 28
    Height = 13
    Caption = 'Nama'
  end
  object Label2: TLabel
    Left = 32
    Top = 56
    Width = 32
    Height = 13
    Caption = 'Alamat'
  end
  object Label3: TLabel
    Left = 29
    Top = 80
    Width = 38
    Height = 13
    Caption = 'No.Telp'
  end
  object Label4: TLabel
    Left = 28
    Top = 104
    Width = 41
    Height = 13
    Caption = 'Tgl Lahir'
  end
  object Label5: TLabel
    Left = 21
    Top = 128
    Width = 55
    Height = 13
    Caption = 'Keterangan'
  end
  object bntTutup: TButton
    Left = 40
    Top = 152
    Width = 75
    Height = 25
    Caption = '&Tutup'
    TabOrder = 0
    OnClick = bntTutupClick
  end
end
