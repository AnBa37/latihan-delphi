unit latRecord;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, AdvEdit;

type
  tr_barang = record
    id : integer;
    kode : string[3];
    nama : string[50];
    stok : integer;
    harga : real;
    tgl_beli : tdate;
    keterangan : string[255];
  end;

  arBarang = array of tr_barang;

  TfrmLatRecord = class(TForm)
    mainPanel: TPanel;
    txtKode: TAdvEdit;
    txtNama: TAdvEdit;
    txtStok: TAdvEdit;
    txtHarga: TAdvEdit;
    dtpTglBeli: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    mmKeterangan: TMemo;
    btnProses: TButton;
    lbl_id: TLabel;
    procedure btnProsesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    data : tr_barang;
    arData : arBarang;
    procedure initform;
    procedure isi_tampil_record;
    procedure isi_array_record;
  public
    { Public declarations }
    procedure execute;
  end;

var
  frmLatRecord: TfrmLatRecord;

implementation

{$R *.dfm}

{ TfrmLatRecord }

procedure TfrmLatRecord.btnProsesClick(Sender: TObject);
var i : integer; isiArray : string;
begin
  if trim(txtKOde.Text) = '' then begin
    ShowMessage('Kode belum diisi.');
    txtKode.SetFocus;
    exit;
  end;

  if txtStok.IntValue = 0 then begin
    ShowMessage('Stok belum diisi.');
    txtStok.SetFocus;
    exit;
  end;

  isi_tampil_record;
  isi_array_record;
  //menampilkan array dari record
  isiArray := 'Isi Data2 Barang : '+#13;
  for i := 0 to Length(arData)-1 do begin
    isiArray := isiArray +'Id : '+inttostr(arData[i].id)+#13+
                'Kode : '+arData[i].kode+#13+
                'Nama : '+arData[i].nama+#13+
                'Stok : '+inttostr(arData[i].stok)+#13+
                'Harga : '+FloatToStr(arData[i].harga)+#13+
                'Tgl Beli : '+datetostr(arData[i].tgl_beli)+#13+
                'Keterangan : '+arData[i].keterangan+#13+#13;
    //ShowMessage(isiArray);
  end;

  ShowMessage(isiArray);
end;

procedure TfrmLatRecord.execute;
begin
  initform;
end;

procedure TfrmLatRecord.FormShow(Sender: TObject);
begin
  execute;
end;

procedure TfrmLatRecord.initform;
begin
  lbl_id.Caption := '';
  txtKode.Text := '';
  txtNama.Text := '';
  txtStok.IntValue := 0;
  txtHarga.FloatValue := 0;
  dtpTglBeli.Date := now;
  mmKeterangan.Lines.Text := '';
end;

procedure TfrmLatRecord.isi_array_record;
var i : integer;
begin
  SetLength(arData, txtStok.IntValue);
  for i := 0 to Length(arData)-1 do begin
    arData[i].id := i+1;
    arData[i].kode := 'kode '+inttostr(i+1);
    arData[i].nama := 'barang '+inttostr(i+1);
    arData[i].stok := 10 * (i+1);
    arData[i].harga := 1000 * (i+1);
    arData[i].tgl_beli := now + (i+1);
    arData[i].keterangan := 'test '+inttostr(i+1);
  end;
end;

procedure TfrmLatRecord.isi_tampil_record;
begin
  data.id := 1;
  data.kode := trim(txtKode.Text);
  data.nama := trim(txtNama.Text);
  data.stok := txtStok.IntValue;
  data.harga := txtHarga.FloatValue;
  data.tgl_beli := dtpTglBeli.date;
  data.keterangan := trim(mmKeterangan.Lines.Text);

  ShowMessage('Isi Record adalah : '+#13+
              'Id : '+inttostr(data.id)+#13+
              'Kode : '+data.kode+#13+
              'Nama : '+data.nama+#13+
              'Stok : '+inttostr(data.stok)+#13+
              'Harga : '+FloatToStr(data.harga)+#13+
              'Tgl Beli : '+datetostr(data.tgl_beli)+#13+
              'Keterangan : '+data.keterangan);
end;

end.
