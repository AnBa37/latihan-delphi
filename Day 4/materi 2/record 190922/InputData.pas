unit InputData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit, ExtCtrls;

type
  tr_barang = record
    kode : string[3];
    nama : string[20];
    stok : integer;
    harga : real;
    tgl_beli : tdate;
    keterangan : string [255];
  end;

  arrBarang = array of tr_barang;
  
  TfrmInputData = class(TForm)
    MainPanel: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    txtKode: TAdvEdit;
    txtNama: TAdvEdit;
    txtStok: TAdvEdit;
    txtHarga: TAdvEdit;
    dtpTglBeli: TDateTimePicker;
    mmKeterangan: TMemo;
    btnProses: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
  private
    { Private declarations }
    dataBrg : tr_barang;
    procedure initform;
    procedure isiRecord;
    procedure isiArrayRecord;
  public
    { Public declarations }
  end;

var
  frmInputData: TfrmInputData;

implementation

{$R *.dfm}

{ TfrmInputData }

procedure TfrmInputData.initform;
begin
  txtKode.Text := '';
  txtNama.Text := '';
  txtStok.IntValue := 0;
  txtHarga.FloatValue := 0;
  dtpTglBeli.Date := now;
  mmKeterangan.Lines.Text := '';
end;

procedure TfrmInputData.FormShow(Sender: TObject);
begin
  initform;
end;

procedure TfrmInputData.btnProsesClick(Sender: TObject);
begin
  isiRecord;
  showmessage('Data barang : '+#13+'kode : '+dataBrg.kode+#13+'nama : '+dataBrg.nama+#13+
              'stok : '+inttostr(dataBrg.stok)+#13+'Harga : '+floattostr(dataBrg.Harga)+#13+
              'tgl beli : '+datetostr(dataBrg.tgl_beli)+#13+
              'keterangan : '+dataBrg.keterangan);
  isiArrayRecord;
end;

procedure TfrmInputData.isiRecord;
begin
  dataBrg.kode := trim(txtKode.Text);
  dataBrg.nama := trim(txtNama.Text);
  dataBrg.stok := txtStok.IntValue;
  dataBrg.harga := txtHarga.FloatValue;
  dataBrg.tgl_beli := dtpTglBeli.Date;
  dataBrg.keterangan := trim(mmKeterangan.Lines.Text);
end;

procedure TfrmInputData.isiArrayRecord;
var arDataBrg : arrBarang; k : integer; dataBarang : string;
begin
  setlength(arDataBrg, 3);  dataBarang := '';
  for k := 0 to length(arDataBrg)-1 do begin
    arDataBrg[k].kode := '00'+inttostr(k+1);
    arDataBrg[k].nama := 'Barang '+inttostr(k+1);
    arDataBrg[k].stok := k * 2;
    arDataBrg[k].harga := arDataBrg[k].stok * 1000;
    arDataBrg[k].tgl_beli := now + k;
    arDataBrg[k].keterangan := 'keterangan '+inttostr(k+1);
    dataBarang := dataBarang + 'kode : '+arDataBrg[k].kode+#13+
                  'nama : '+arDataBrg[k].nama+#13+
                  'stok : '+inttostr(arDataBrg[k].stok)+#13+
                  'harga : '+floattostr(arDataBrg[k].harga)+#13+
                  'tgl Beli : '+datetostr(arDataBrg[k].tgl_beli)+#13+
                  'keteraangan : '+arDataBrg[k].keterangan+#13+#13;
  end;
  showmessage('Data-data barang :'+#13+#13+dataBarang);
end;

end.
