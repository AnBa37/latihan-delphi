unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    txt1: TAdvEdit;
    txt2: TAdvEdit;
    btnProses: TButton;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
    function CekInteger(AString: string): boolean; //dalam bentuk FUNGSI
    procedure CekInteger2(AString : string; var Hasil : Boolean); //dalam bentuk PROSEDUR
    function Bravo(input1, input2 : string) : string; //dalam bentuk FUNGSI
    procedure Tampilin(var Hasil : string; input1, input2 : string); //dalam bentuk PROSEDUR
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

{$R *.dfm}

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInput.InitForm;
begin
  txt1.Text := ''; txt2.Text := '';
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

function TfrmInput.CekInteger(AString: string): boolean;
begin
  try
    StrToInt(AString);
    Result := true;
  except
    on EConvertError do Result := false;
  end;
end;

function TfrmInput.Bravo(input1, input2: string): string;
var cek1, cek2 : Boolean;
begin
  cek1 := CekInteger(input1);
  CekInteger2(input2, cek2);
//  cek2 := CekInteger(input2);
  if (cek1 and not cek2) or (not cek1 and cek2) then //22nya beda tipe
    Result := 'Tipe TIDAK SAMA.'
  else if cek1 and cek2 then //22nya angka
    Result := input1 + ' x ' + input2 + ' = ' + IntToStr(StrToInt(input1) * (StrToInt(input2)))
  else if not cek1 and not cek2 then //22nya huruf
    Result := input1 + '-' + input2;
end;

procedure TfrmInput.btnProsesClick(Sender: TObject);
var hsl : string;
begin
  ShowMessage('Hasil FUNGSI : ' + Bravo(Trim(txt1.Text), Trim(txt2.Text)));
  hsl := ''; Tampilin(hsl, Trim(txt1.Text), Trim(txt2.Text));
  ShowMessage('Hasil PROSEDUR : ' + hsl);
end;

procedure TfrmInput.CekInteger2(AString: string; var Hasil: Boolean);
begin
  try
    StrToInt(AString);
    Hasil := true;
  except
    on EConvertError do Hasil := false;
  end;
end;

procedure TfrmInput.Tampilin(var Hasil: string; input1, input2: string);
var cek1, cek2 : Boolean;
begin
  cek1 := CekInteger(input1);
  CekInteger2(input2, cek2);
  if (cek1 and not cek2) or (not cek1 and cek2) then //22nya beda tipe
    Hasil := 'Tipe TIDAK SAMA.'
  else if cek1 and cek2 then //22nya angka
    Hasil := input1 + ' x ' + input2 + ' = ' + IntToStr(StrToInt(input1) * (StrToInt(input2)))
  else if not cek1 and not cek2 then //22nya huruf
    Hasil := input1 + '-' + input2;
end;

end.
