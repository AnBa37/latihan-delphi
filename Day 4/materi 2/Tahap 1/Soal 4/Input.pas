unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    txt1: TAdvEdit;
    txt2: TAdvEdit;
    cmbOutput: TComboBox;
    btnProses: TButton;
    btnTutup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
    function CekInteger(AString: string): boolean;
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

{$R *.dfm}

{ TfrmInput }

procedure TfrmInput.InitForm;
begin
  txt1.Text := '';
  txt2.Text := '';
  cmbOutput.Items.Clear;
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

function TfrmInput.CekInteger(AString: string): boolean;
begin
  try
    StrToInt(AString);
    Result:=true
  except
    on EConvertError do
      Result:=false;
  end;
end;

procedure TfrmInput.btnProsesClick(Sender: TObject);
var cek1, cek2 : Boolean;
    selisih, i : integer;
begin
  cmbOutput.Items.Clear; //pastikan combo KOSONG
  cek1 := CekInteger(txt1.Text);
  cek2 := CekInteger(txt2.Text);
  if (cek1 and not cek2) or (not cek1 and cek2) then
    ShowMessage('Input harus TIPE SAMA.')
  else if (not cek1) and (not cek2) then begin //22nya huruf
    cmbOutput.Items.Clear;
    cmbOutput.Items.Append(Trim(txt1.Text));
    cmbOutput.Items.Append(Trim(txt2.Text));
  end else if cek1 and cek2 then begin //22nya angka
    if (StrToInt(txt1.Text) > StrToInt(txt2.Text)) then begin //isi txt1 > isi txt2
      selisih := StrToInt(txt1.Text) - StrToInt(txt2.Text);
      if selisih > 20 then ShowMessage('Selisih terlalu besar')
      else begin
        cmbOutput.Items.Clear;
        for i := StrToInt(txt1.Text) downto StrToInt(txt2.Text) do
          cmbOutput.Items.Add(IntToStr(i));
      end;
    end else if (StrToInt(txt2.Text) > StrToInt(txt1.Text)) then begin //isi txt1 < isi txt 2
      selisih := StrToInt(txt2.Text) - StrToInt(txt1.Text);
      if selisih > 20 then ShowMessage('Selisih terlalu besar')
      else begin
        cmbOutput.Items.Clear;
        for i := StrToInt(txt1.Text) to StrToInt(txt2.Text) do
          cmbOutput.Items.Add(IntToStr(i));
      end;
    end;
  end;
end;

end.
