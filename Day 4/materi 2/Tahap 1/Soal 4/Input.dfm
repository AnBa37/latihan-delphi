object frmInput: TfrmInput
  Left = 490
  Top = 311
  BorderStyle = bsDialog
  Caption = 'Input'
  ClientHeight = 136
  ClientWidth = 170
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object txt1: TAdvEdit
    Left = 8
    Top = 8
    Width = 153
    Height = 21
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 0
    Text = 'txt1'
    Visible = True
    Version = '2.9.2.1'
  end
  object txt2: TAdvEdit
    Left = 8
    Top = 40
    Width = 153
    Height = 21
    FocusColor = clWindow
    ReturnIsTab = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 1
    Text = 'txt2'
    Visible = True
    Version = '2.9.2.1'
  end
  object cmbOutput: TComboBox
    Left = 8
    Top = 72
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object btnProses: TButton
    Left = 8
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Proses'
    TabOrder = 3
    OnClick = btnProsesClick
  end
  object btnTutup: TButton
    Left = 88
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 4
    OnClick = btnTutupClick
  end
end
