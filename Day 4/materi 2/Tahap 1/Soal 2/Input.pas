unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmInput = class(TForm)
    txt1: TAdvEdit;
    txt2: TAdvEdit;
    txt3: TAdvEdit;
    txt4: TAdvEdit;
    txt5: TAdvEdit;
    btnShowAll: TButton;
    btnProses: TButton;
    btnTutup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure btnShowAllClick(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
    procedure txt2KeyPress(Sender: TObject; var Key: Char);
    procedure txt4KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    Arr : array of string;
  end;

var
  frmInput: TfrmInput;

implementation

uses Proses;

{$R *.dfm}

{ TfrmInput }

procedure TfrmInput.InitForm;
begin
  txt1.IntValue := 0;
  txt2.Text := '';
  txt3.IntValue := 0;
  txt4.Text := '';
  txt5.IntValue := 0;
  setlength(Arr, 0);
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  InitForm;
end;

procedure TfrmInput.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInput.btnShowAllClick(Sender: TObject);
var i : integer;
begin
  //memastikan Array terisi data KARENA saat awal setlength = 0
  setlength(Arr, 5);
  Arr[0] := Trim(txt1.Text);
  Arr[1] := Trim(txt2.Text);
  Arr[2] := Trim(txt3.Text);
  Arr[3] := Trim(txt4.Text);
  Arr[4] := Trim(txt5.Text);

  for i := length(Arr)-1 downto 0 do ShowMessage(Arr[i]);
end;

procedure TfrmInput.btnProsesClick(Sender: TObject);
begin
  //memastikan Array terisi data KARENA saat awal setlength = 0
  setlength(Arr, 5);
  Arr[0] := Trim(txt1.Text);
  Arr[1] := Trim(txt2.Text);
  Arr[2] := Trim(txt3.Text);
  Arr[3] := Trim(txt4.Text);
  Arr[4] := Trim(txt5.Text);
  frmProses.Execute;
end;

procedure TfrmInput.txt2KeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key := #0;
end;

procedure TfrmInput.txt4KeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['a'..'z',#8,#13]) then key := #0; //tidak bisa ketik angka/charakter selain huruf a- z & enter dan #8 (cri di gugle kode ascii)
end;

end.
