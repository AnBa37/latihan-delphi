unit Proses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmProses = class(TForm)
    txtInput: TAdvEdit;
    btnTampil: TButton;
    btnTutup: TButton;
    procedure btnTampilClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmProses: TfrmProses;

implementation

uses Input;

{$R *.dfm}

{ TfrmProses }

procedure TfrmProses.Execute;
begin
  InitForm;
  ShowModal;
end;

procedure TfrmProses.InitForm;
begin
  txtInput.IntValue := 0;
end;

procedure TfrmProses.btnTampilClick(Sender: TObject);
begin
{ cara 1 : brute force dicek 1 1.. mending cuman 5, kalo lebih?
  if txtInput.IntValue = 1 then ShowMessage(frmInput.Arr[0])
  else if txtInput.IntValue = 2 then ShowMessage(frmInput.Arr[1])
  else if txtInput.IntValue = 3 then ShowMessage(frmInput.Arr[2])
  else if txtInput.IntValue = 4 then ShowMessage(frmInput.Arr[3])
  else if txtInput.IntValue = 5 then ShowMessage(frmInput.Arr[(txtInput.IntValue-1)])
  else ShowMessage('INPUT antara 1 - 5.');
}
  //cara 2 : lebih simple
  if (txtInput.IntValue >= 1) and (txtInput.IntValue <= 5) then
    ShowMessage('Isi text ke-'+txtInput.Text+' : '+frmInput.Arr[(txtInput.IntValue-1)])
  else ShowMessage('INPUT antara 1 - 5.');
end;

procedure TfrmProses.btnTutupClick(Sender: TObject);
begin
  Close;
end;

end.
