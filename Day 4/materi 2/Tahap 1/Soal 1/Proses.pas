unit Proses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmProses = class(TForm)
    lblHasil: TLabel;
    btnTutup: TButton;
    procedure btnTutupClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmProses: TfrmProses;

implementation

uses Tampil;

{$R *.dfm}

{ TfrmProses }

procedure TfrmProses.Execute;
begin
  lblHasil.Caption := frmTampil.txtInput.Text;
  ShowModal;
end;

procedure TfrmProses.btnTutupClick(Sender: TObject);
begin
  Close;
end;

end.
