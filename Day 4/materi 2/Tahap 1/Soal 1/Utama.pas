unit Utama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmUtama = class(TForm)
    lblJudul: TLabel;
    btnProses: TButton;
    btnTutup: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
  end;

var
  frmUtama: TfrmUtama;

implementation

uses Tampil;

{$R *.dfm}

{ TfrmUtama }

procedure TfrmUtama.InitForm;
begin
  lblJudul.Caption := 'Selamat Datang';
end;

procedure TfrmUtama.FormShow(Sender: TObject);
begin
  InitForm;
end;

procedure TfrmUtama.btnTutupClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmUtama.btnProsesClick(Sender: TObject);
begin
  frmTampil.Execute;
end;

end.
