unit Tampil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit;

type
  TfrmTampil = class(TForm)
    txtInput: TAdvEdit;
    btnIsiText: TButton;
    btnProses: TButton;
    btnTutup: TButton;
    procedure btnIsiTextClick(Sender: TObject);
    procedure btnTutupClick(Sender: TObject);
    procedure btnProsesClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitForm;
  public
    { Public declarations }
    procedure Execute;
  end;

var
  frmTampil: TfrmTampil;

implementation

uses Utama, Proses;

{$R *.dfm}

{ TfrmTampil }

procedure TfrmTampil.Execute;
begin
  InitForm;
  ShowModal;
end;

procedure TfrmTampil.InitForm;
begin
  txtInput.Text := '';
end;

procedure TfrmTampil.btnIsiTextClick(Sender: TObject);
begin
  txtInput.Text := 'Bank Danamon';
end;

procedure TfrmTampil.btnTutupClick(Sender: TObject);
begin
  Close;
  frmUtama.lblJudul.Caption := 'Selamat Datang Kembali';
end;

procedure TfrmTampil.btnProsesClick(Sender: TObject);
begin
  frmProses.Execute;
  frmUtama.lblJudul.Caption := 'Selamat Datang Kembali';
  Close;
end;

end.
